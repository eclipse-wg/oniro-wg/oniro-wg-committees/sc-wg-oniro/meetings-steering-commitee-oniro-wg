# Oniro Working Group Steering Committee Meeting 2025wk2

* Date: 2025wk2 2025-1-9
* Time: from 13:30 to 14:30 UTC
    * Oniro WG SC Members Meeting
    * Open to All Members
* Chair: Jaroslaw Marek 
* Minutes taker:  Juan Rico
* Join via Zoom
    * Link: https://eclipse.zoom.us/j/85049355252
     * Meeting ID: 85049355252

All times in UTC time.

Meeting starts at 13:35, as quorum is reached

# Agenda

- Approval of the MoM 2024wk50- all
- Technical update
- Marketing Update
- AOB

# Attendees

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

## Steering Committee

- Carlo Piana (Array representative)
- Adrian O'Sullivvan (Huawei Alternate)
- Jaroslaw Marek (Huawei representative)
- Alberto Pianon (Array Alternate)

## Eclipse Foundation 
- Juan Rico

## Committers
- Francesco Pham (Huawei)

## Other Members



## Regrets:
- Sharon Corbett  
- Gael Blondelle
- Thabang Mashologu

# Minutes

## Discussion

### Approval of the meeting minutes from the 2024wk50 2024-12-12 Oniro WG SC Meeting
- MoM approved at commit 253b78a11eac5b083e7cdca243622bf48be8684e

### Technical Update
- New Appliations 14 in total 
- WIP to support Volla and mobile devices to make it working with the GPU.
- Oniro based onn OpenHarmony 5.0 - build working fine, engaging with IP compliance.
- Start the integration of a boot animation 

### Marketing Update
- The first version of the marketing plan was presented to be checked by the SC members before openning it to the community.
- it is available in the repository - https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/program-plan-budget-templates/2025/OniroMarketingPlan_2025V0.1.pdf

### AOB
- Next meeting on January 23rd 2025

## Resolutions
- MoM approved at commit 253b78a11eac5b083e7cdca243622bf48be8684e

## Links and references

### Links

This is the list of relevant links:

Repository where you will find the agenda and meeting minutes to be approved (private repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve

Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc

Oniro WG Program Plan: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf

Oniro WG Budget: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf


This is the list to reference and other important notes to understand this document:

AoB: Any Other Business
MoM: Minutes of Meeting
WG: Working Group
SC: Steering Committee
EF: Eclipse Foundation
OH: Open Harmony


Meeting closes at 13:37 UTC 
