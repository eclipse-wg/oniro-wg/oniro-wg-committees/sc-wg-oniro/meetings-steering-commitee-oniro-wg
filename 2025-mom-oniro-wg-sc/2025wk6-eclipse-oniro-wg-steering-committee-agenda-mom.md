# Oniro Working Group Steering Committee Meeting 2025wk6

* Date: 2025wk6 2025-2-6
* Time: from 13:30 to 14:30 UTC
    * Oniro WG SC Members Meeting
    * Open to All Members
* Chair: Jaroslaw Marek 
* Minutes taker:  Juan Rico
* Join via Zoom
    * Link: https://eclipse.zoom.us/j/85049355252
     * Meeting ID: 85049355252

All times in UTC time.

Meeting starts at 13:35, as quorum is reached

# Agenda

- Approval of the MoM 2025wk4 - all
- Technical update
- Marketing Update
- AOB

# Attendees

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

## Steering Committee

- Carlo Piana (Array representative)
- Jaroslaw Marek (Huawei representative)
- Adrian O'Sullivan (Huawei Alternate)
- Alberto Pianon (Array Alternate)

## Eclipse Foundation 
- Juan Rico

## Committers
- Francesco Pham (Huawei)

## Other Members
- Luca Miotto (NOI)

## Regrets:
- Sharon Corbett  
- Gael Blondelle
- Thabang Mashologu


# Minutes

## Discussion

### Approval of the meeting minutes from the 2025wk4 2025-1-23 Oniro WG SC Meeting
- MoM approved at commit eaf29f38f00f344d111ec676dfeaa6d8c477aaf4

### Technical Update
- FOSDEM - libhybris presentation well accepted and people was engaged
- Apps - GPT client ongoing, Spotify, Weather app improvements.
- IP toolchain - reconstruction of the output metadata of the OpenHarmony. 
    - Pending discussion with Kubi for a better understanding of the open points.

### Marketing Update
- Planning of the hackathon from a horizontal perspective:
    - Improve developer experience
    - Maximize the impact of the actions and results derived from the organization of a hackathon
- Working on onboarding process
    - Needs to involve all the parts of the WG including IP toolchain.

- MWC - OpenWallet Workshop
### AOB
- Program Manager 
- Next meeting on February 20th

## Resolutions
- MoM approved at commit eaf29f38f00f344d111ec676dfeaa6d8c477aaf4

## Links and references

### Links

This is the list of relevant links:

Repository where you will find the agenda and meeting minutes to be approved (private repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve

Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc

Oniro WG Program Plan: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf

Oniro WG Budget: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf


This is the list to reference and other important notes to understand this document:

AoB: Any Other Business
MoM: Minutes of Meeting
WG: Working Group
SC: Steering Committee
EF: Eclipse Foundation
OH: Open Harmony


Meeting closes at 13:37 UTC 
