# Oniro Working Group Steering Committee Meeting 2025wk4

* Date: 2025wk4 2025-1-23
* Time: from 13:30 to 14:30 UTC
    * Oniro WG SC Members Meeting
    * Open to All Members
* Chair: Jaroslaw Marek 
* Minutes taker:  Juan Rico
* Join via Zoom
    * Link: https://eclipse.zoom.us/j/85049355252
     * Meeting ID: 85049355252

All times in UTC time.

Meeting starts at 13:35, as quorum is reached

# Agenda

- Approval of the MoM 2025wk2 - all
- OpenHarmony ad Open Invention Network
- Technical update
- Marketing Update
- AOB

# Attendees

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

## Steering Committee

- Carlo Piana (Array representative)
- Adrian O'Sullivvan (Huawei Alternate)
- Jaroslaw Marek (Huawei representative)
- Alberto Pianon (Array Alternate)

## Eclipse Foundation 
- Juan Rico

## Committers
- Francesco Pham (Huawei)

## Other Members
- Luca Miotto (NOI)


## Regrets:
- Sharon Corbett  
- Gael Blondelle
- Thabang Mashologu

# Minutes

## Discussion

### Approval of the meeting minutes from the 2025wk2 2025-1-9 Oniro WG SC Meeting
- MoM approved at commit 5c101facd232223a19c90993868a3e273e488929

### OpenHarmony
- No updates in the specification process.
- Share the agenda for the meeting with OH to give them enough time to get prepared. 

### Open Invention Network
- Array member of the initiative
- Cross patent pull
- Looks like out of the scope of Oniro today

### Technical Update
-  App building 
    - Weather forecast and many others
    - CI pipeline to streamline processes
- Oniro based on OH5.0 working well
- Libsys work in progress - will be presented in FOSDEM
- Superdevice concept demo using soundcloud using a raspberry pi and the phone in progress.

### Marketing Update
- Focusing first on phones and Apps
- Focus on make our message heard, keep working on awareness
- Events:
    - FOSDEM - Alberto and Francesco presenting 
- Rust 10 years potential cooperation - TBC

### AOB
- Next meeting on February 6th

## Resolutions
- MoM approved at commit 5c101facd232223a19c90993868a3e273e488929

## Links and references

### Links

This is the list of relevant links:

Repository where you will find the agenda and meeting minutes to be approved (private repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve

Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc

Oniro WG Program Plan: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf

Oniro WG Budget: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf


This is the list to reference and other important notes to understand this document:

AoB: Any Other Business
MoM: Minutes of Meeting
WG: Working Group
SC: Steering Committee
EF: Eclipse Foundation
OH: Open Harmony


Meeting closes at 13:37 UTC 
