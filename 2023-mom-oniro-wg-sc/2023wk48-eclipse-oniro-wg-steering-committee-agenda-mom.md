# Oniro Working Group Steering Committee Meeting 2023wk48

* Date: 2023wk46 2023-11-30
* Time: from 14:00 to 15:00 UTC
    * Oniro WG SC Members Meeting
    * Open to All Members
* Chair: Suhail Khan
* Minutes taker:  Carlo Piana
* Join via Zoom
    * Link: https://eclipse.zoom.us/j/85049355252
     * Meeting ID: 85049355252

All times in UTC time.

Meeting is started at 14:04, quorum is reached

# Agenda

* AApproval of the meeting minutes from the 2023-11-16 2023wk46 Oniro WG SC Meeting (All) 5 minutes
* Presentation of the impact of the Eclipse Foundation - Open Atom Agreement - Juan 25 minutes
* Oniro Pipeline - Juan 5 minutes
* Resolutions - 3 min
* Next Meeting - Dec 14 - 2 min
* AOB - 5 minutes

# Attendees

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

## Steering Committee

- Suhail Khan (Huawei primary) 
- Carlo Piana   (Array, Silver Members representative)
- Adrian O'Sullivan (Huawei alternate)
- Alberto Pianon (Array Alternate)

## Eclipse Foundation 

- Juan Rico
- Biil Fletcher


## Committers

- Stefan Schmidt (Huawei)

## Other Members

- Luca Miotto (NOI)

## Regrets:

- Sharon Corbett  
- Gael Blondelle
- Thabang Mashologu

# Minutes

## Discussion

### Approval of the meeting minutes from the 2023-06-01 2023wk46 Oniro WG SC Meeting

Minutes at commit 785d8dd3 are approved without objections

## Other items in the Agenda

## Eclipse Foundation - Open Atom agreement

Juan presents the last 

## Oniro Pipeline

Quick review by Juan

## Resolutions

- to approve the minutes at commit 785d8dd3

## AOB

No other business

## Links and references

### Links

This is the list of relevant links:

Repository where you will find the agenda and meeting minutes to be approved (private repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve

Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc

Oniro WG Program Plan: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf

Oniro WG Budget: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf


This is the list to reference and other important notes to understand this document:

AoB: Any Other Business
MoM: Minutes of Meeting
WG: Working Group
SC: Steering Committee
EF: Eclipse Foundation
OH: Open Harmony


Meeting closes at 14:58 UTC 