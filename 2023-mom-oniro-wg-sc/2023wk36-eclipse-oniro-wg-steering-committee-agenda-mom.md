# Oniro Working Group Steering Committee Meeting 2023wk36

* Date: 2023wk36 2023-09-07
* Time: from 13:00 to 14:00 UTC
    * Oniro WG SC Members Meeting
    * Open to All Members
* Chair: Suhail Khan
* Minutes taker:  Carlo Piana
* Join via Zoom
    * Link: https://eclipse.zoom.us/j/86710449041?pwd=dmk1OW56emQvU1YvSjBUY1AyOHIzUT09
    * Meeting ID: 867 1044 9041 


All times in UTC time.

Meeting is started at 13:05, quorum is reached at 13:10 UTC

# Agenda

* Approval of the meeting minutes from the 2023-06-01 2023wk30 Oniro WG SC Meeting (All)
* OpenAtom Agreement Update - Gael B./Juan R. 10 minutes
* Development Activity Update - Suhail K. 10 minutes
* Renaming of Projects Update - Alberto P. 10 Minutes
* EclipseCon Update - Suhail K./Juan R. - 10 Minutes
* Next Meeting - October 5 - (All) 2.5 minutes 
* Resolutions - 2.5 minutes
* AoB - 2.5 minutes

# Attendees

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

## Steering Committee

- Suhail Khan (Huawei primary) 
- Alberto Pianon (Array Alternate)
- Carlo Piana   (Array, Silver Members representative)

## Eclipse Foundation 

- Juan Rico

## Committers

- Stefan Schmidt (Huawei)

## Other Members

- Luca Miotto (NOI)
- Patrick Ohnewein (NOI)

## Regrets:

- Adrian O'Sullivan (Huawei alternate)
- Sharon Corbett  
- Gael Blondelle
- Thabang Mashologu

# Minutes

## Discussion

### Approval of the meeting minutes from the 2023-06-01 2023wk30 Oniro WG SC Meeting

2023wk30 Oniro WG SC meeting's MoM needs to be approved.

MoM #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/agenda-minutes-to-approve/2023wk30-eclipse-oniro-wg-steering-committee-agenda-mom.md
   
Approved at commit  109147e4

### Othher items in the Agenda

- Suhail gives the status update on the progess of the project

- Alberto reports on the progress of including other potential members inthe IP Compliance Toolchain and the possible creation of a dedicated SIG

- We discuss the activities of the upcoming EclipseCon, especially the Community Day.

- The next meeting will be held regularly, the new invite will be circulated with sufficient time ahead.

## Resolutions

- To approve minutes for 2023wk30 at commit 109147e4


## AOB

None.

## Links and references

### Links

This is the list of relevant links:

Repository where you will find the agenda and meeting minutes to be approved (private repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve

Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc

Oniro WG Program Plan: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf

Oniro WG Budget: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf


This is the list to reference and other important notes to understand this document:

AoB: Any Other Business
MoM: Minutes of Meeting
WG: Working Group
SC: Steering Committee
EF: Eclipse Foundation
OH: Open Harmony


Meeting closes at 13:36 UTC 
