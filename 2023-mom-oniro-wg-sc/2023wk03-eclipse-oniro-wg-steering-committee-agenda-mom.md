# Oniro Working Group Steering Committee Meeting 2023wk03 

These minutes are only informative, since no quorum was achieved

* Date: 2023wk03 2023-01-19
* Time: from 14:00 to 15:00 UTC
   * Oniro WG SC All Members meeting: from 14:00 to 14:30 UTC
	* Oniro WG SC All Members meeting: from 14:31 to 15:00 UTC
* Chair: N/A 
* Minutes taker: Carlo Piana
* Join via Zoom
   * Link: <https://eclipse.zoom.us/j/86710449041?pwd=dmk1OW56emQvU1YvSjBUY1AyOHIzUT09>
   * Meeting ID: 867 1044 9041 Passcode: 293617

## Agenda

Oniro WG SC meeting

* Approval of the meeting minutes from the 2022-11-24 2022wk47 Oniro WG SC Meeting - Davide R. 5 minutes
* Oniro offering value proposition - Agustin 15 minutes
* Resolutions - 5 minutes
* AoB - 5 minutes

Oniro WG SC All Members meeting

* Oniro offering value proposition - Agustin 15 min
* 2023 Events list - Agustin 5 min
* Oniro IT Migration report - Agustin 5 min
* AoB - 5 minutes


## Attendees

All Member representatives have been invited to the full meeting. No private session.

### Oniro WG SC Meeting

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

* Carlo Piana - Array. Silver Members representative
* Alberto Pianon - Array alternate. Silver Members

Steering Committee Regrets:

* Davide Ricci - Huawei
* Amit Kucheria - Commiters
* Adrian O'Sullivan - Huawei alternate

**No quorum.** The topics will be part of the agenda for the coming Oniro WG Sc meeting

Eclipse Foundation:

* Agustin Benito Bethencourt
* Paul Buck 

[comment]: # (* Sharon Corbbet)

### Oniro WG SC All Members Meeting

Beyond the above, the participants in this section were:

* Andrea Gallo - Linaro
* Luca Tavanti - Cubit
* Patrick Ohnewein - NOI
* Steeve Baudy - IoTex

[comment]: # (* Luca Miotto - NOI alternate)
[comment]: # (* Xinxin Fan - IoTex alternate)
[comment]: # (* Bill Fletcher - Linaro alternate)

Apologies from Paul Buck, had to leave before the public session

## Minutes

No Quorum, carry over the business to the next meeting.

### Oniro WG SC meeting minutes

#### Oniro offering value proposition


#### Resolutions

No resolution due to lack of quorum.

#### AoB  (Open Floor)

* Plans for the Oniro WG SC to assume temporarily the responsibilities of the Oniro WG Specification Committee, which will be disbanded until we have capacity to create a healthy specification project 

* Paul Buck summarised at very high level the proposal from the EF to the Oniro WG Specification Committee to be disbanded given that there has been no answer to the call for participation in the first specification project. The procedure to follow to achieve this will be presented an executed during the coming days. The overall idea is that the Oniro WG  Specification Committee asks the Oniro WG SC to assume the responsibilities related with specifications until there is capacity to initiate a specification project so the Oniro WG Spec. Committee is re-stated.

* Short conversation about who is attending to FOSDEM and to the EU policy meeting the Friday before. FOSDEM 2023 participants list:

Link: https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/Events/2023-events/FOSDEM'23#participants-list

Meeting adjourns at 14:20 UTC until the public session.

### Oniro WG SC All Members meeting

Opens at 14:32

Discussion on where does Oniro stand and what are the possible developments, after the reassessment of the capacity of the project (Steve askging).

Agustin provides a short report on the events from September to today, pipeline of new prospects stopped, redefining the value proposition and restart the pipeline of the prospect. At the moment efforts are directed to the cooperation with OpenHarmony. Carlo mentions that it is difficult to speak about value proposition in the absence of a clear view of what the technical and engineering capacity and what can be achieved in a finite time, and what we have today, albeit very good, is probably not sufficient to clearly set us apart from anything existing in the space.

More discussion on the OpenHarmony - Oniro integration, which should be mainly an interoperability project to make sure that the interaction is based on clearly defined specs (Agustin, on Patrick's question).



The reminder of the points are kept only for future reference:


#### Oniro offering value proposition

##### Description

At EclipseCon 2022 we agreed on going through a process to define the value proposition of the new offering we defined, which includes oniro-core and profiles, being the initial profile IoT&Edge for consumer's devices.

* Agustin propose to use as starting session to define this Value Proposition tomorrow's Oniro Prospects Pipeline Meeting.  
* The first exercise could be creating a mindmap of topics we would want to include in the discussion
* The second exercise would be to define the Value proposition for each one of the offering elements.
* The third exercise would be to concile those Value Propositions and extrat the main common points we can focus on from a communication perspective Prioritization.

##### Discussion

#### 2023 Events list

Not discussed

##### Description

PLease evaluate the following list of events. Add those you intend to participate or those you think that Oniro should have presence.
Link #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/Events#events-list-2023> 

Are these the 2023 key events:
* FOSDEM 2023 #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/Events/2023-events/FOSDEM'23> 
* Embedded World 2023
* Embedded Open Source Summit 2023
* EclipseCon 2023 
* SFSCon 2023

If not, which ones?

##### Discussion

#### Oniro IT Migration report

Not discussed

Important links and references:
* Oniro IT Migration Plan v 1.1.0 #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/coordination-it-services-oniro-wg/-/blob/main/oniro-it-infrastructure-and-services-implementation-and-migration-plan.md>
* MoMs of the Oniro IT meetings #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/coordination-it-services-oniro-wg/-/tree/main/mom-coordination-it-sservices-oniro-wg>
* Priority 1. Oniro Deployment Pipelines:
   * [Workflow board](https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/-/boards/2466)
   * [Priority board](https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/-/boards/1852)
* Priority 2. Chat-service:
   * [Workflow board](https://gitlab.eclipse.org/groups/eclipsefdn/it/releng/chat-service/-/boards/)
   * [Priority board](https://gitlab.eclipse.org/groups/eclipsefdn/it/releng/chat-service/-/boards/2687)
   
Status: overview.

The EF IT team has created the pipelines but now, in order to put them in production, effort from engineers with knowledge on the tooling and the creation of the distribution is needed. 
   * Review that the existing configurations and implementations are the ones desired.
   * Test the current pipelines to make sure results are like the ones obtained with the existing one, given that the environment is now different.
   * PLan to put the deployment pipelines in production and decomision the existing ones.
   
This activities requires a sustained effort from engineers and a gradual transition into production to avoid impact.

In the meantime, the chat service, priority 2, is gaining traction. This service is in alpha testing phase.
* A staging instance of a matrix server has been implemented, including a web client.
* EF staff is testing together with EF IT such instance to detect the most obvious bugs and required improvements.
   * This testing includes two clients, NeoChat and Element.
* On Oniro space has been created with the following channels:
   * #oniro-wg
   * #oniro-dev
* Next steps:
   * The staging service needs to comply with the EF legal and IP frameworks. Investigations and experimentation to defines measures to meet them are ongoing.
   * Additional staf at EF will test the instance.
   * A room for IP and license compliance under the EF space will be created to be used by Oniro, IoT, Edge and automotive projects.
   * We will kick-off the beta-testing phase, where Oniro developers, familiar with matrix, will join as beta testers.

#### AoB

None

## Links and references

### Links

This is the list of relevant links:
* Repository where you will find the agenda and meeting minutes to be approved (private repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve>
* Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc>
* Oniro WG Program Plan: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf>
* Oniro WG Budget: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf>

This is the list to reference and other important notes to understand this document:
* AoB: Any Other Business
* MoM: Minutes of Meeting
* WG: Working Group
* SC: Steering Committee
* EF: Eclipse Foundation
* OH: Open Harmony

Meeting closes at 15:10 UTC

All times UTC unless expressly mentioned
