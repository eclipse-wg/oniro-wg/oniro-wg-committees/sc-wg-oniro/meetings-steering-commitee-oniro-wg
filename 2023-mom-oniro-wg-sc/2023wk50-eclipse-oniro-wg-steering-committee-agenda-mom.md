# Oniro Working Group Steering Committee Meeting 2023wk50

* Date: 2023wk50 2023-14-12
* Time: from 14:00 to 15:00 UTC
    * Oniro WG SC Members Meeting
    * Open to All Members
* Chair: Suhail Khan
* Minutes taker:  Juan Rico
* Join via Zoom
    * Link: https://eclipse.zoom.us/j/85049355252
     * Meeting ID: 85049355252

All times in UTC time.

Meeting is started at hh:mm, quorum is reached

# Agenda
- Approval of the meeting minutes from the 2023-11-16 2023wk48 Oniro WG SC Meeting (All) 5 minutes
- Update of the Eclipse Foundation - Open Atom Agreement - 10 minutes
- Oniro Pipeline - Juan/Adrian 10 minutes
- Marketing activities for 2024 - Juan 10 minutes
- AOB


# Attendees

Steering Committee Members (Quorum =  2 of 3 for this Meeting): reached at 14:06

## Steering Committee

-  Adrian O'Sullivan (Huawei alternate)

## Eclipse Foundation 

- Juan Rico


## Committers

- Stefan Schmidt (Huawei)

## Other Members

- Patrick Ohnewein (NOI Techpark)

## Regrets:
- Luca Miotto (Noi Techpark) 
- Suhail Khan
- Sharon Corbett  
- Gael Blondelle
- Thabang Mashologu


# Minutes

## Discussion

### Approval of the meeting minutes from the 2023-11-30 2023wk48 Oniro WG SC Meeting

Approved at 4559bf65, no objections.

### OpenAtom agreement update

Juan provides updates as to he evolution, new potential members and sharing the content of the agreement with current members.

Adrian walks the attendee through other potential members from the OH ecosystem.

## Next meeting

As planned for wk2 - Jan 11th, skip the one in festivity time.


## Resolutions

- To approve the minutes at commit 4559bf65

- To adjourn to Jan 11 


## AOB

A bit of new marketing sctivities.

## Links and references

### Links

This is the list of relevant links:

Repository where you will find the agenda and meeting minutes to be approved (private repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve

Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc

Oniro WG Program Plan: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf

Oniro WG Budget: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf


This is the list to reference and other important notes to understand this document:

AoB: Any Other Business
MoM: Minutes of Meeting
WG: Working Group
SC: Steering Committee
EF: Eclipse Foundation
OH: Open Harmony


Meeting closes at 14:51 UTC 
