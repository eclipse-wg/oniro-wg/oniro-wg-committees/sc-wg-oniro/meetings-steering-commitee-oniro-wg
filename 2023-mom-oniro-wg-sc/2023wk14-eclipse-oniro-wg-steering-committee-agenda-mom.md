# Oniro Working Group Steering Committee Meeting 2023wk14

* Date: 2023wk14 2023-04-06
* Approved at 6f469dfe on 2023-05-04
* Time: from 13:00 to 14:00 UTC
   * Oniro WG SC Members meeting: from 13:00 to 13:30 UTC
	* Oniro WG All Members meeting: from 13:31 to 14:00 UTC
* Chair: Davide Ricci 
* Minutes taker:  Carlo Piana
* Join via Zoom
   * Link: <https://eclipse.zoom.us/j/86710449041?pwd=dmk1OW56emQvU1YvSjBUY1AyOHIzUT09>
   * Meeting ID: 867 1044 9041 Passcode: 293617

## Agenda

### Oniro WG SC meeting

* Approval of the meeting minutes from the 2023-03-09 2023wk10 Oniro WG SC Meeting - Davide R. 5 minutes
* Operations Update (Elections, PM Hire, and so on) - Paul B.
* OpenAtom Agreement Update - Paul B.
* Development Actitivity Status - Davide R.
* Resolutions - 2.5 minutes
* AoB - 2.5 minutes

#### Oniro Working Group All Members meeting

No formal agenda set

## Attendees

* Davide Ricci (Huawei)
* Adrian O'Sullivan (Huawei alternate)
* Carlo Piana   (Array, Silver Members representative)
* Alberto Pianon (Array Alternate)


### Oniro WG SC Meeting

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

* Carlo Piana (Array - Elected Silver Member representative)
* Davide Ricci (Huawei)

Steering Committee Regrets:

* Amit Kucheria (Committers)

Eclipse Foundation:

* Paul Buck
* Gaël Blondelle


# Minutes

Quorum reached at 13:00 UTC

### Oniro WG SC meeting minutes

#### Approval of the meeting minutes from the 2023-03-09 2023wk10 Oniro WG SC Meeting

2023wk10 Oniro WG SC meeting's MoM needs to be approved.

MoM #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/agenda-minutes-to-approve/2023wk10-eclipse-oniro-wg-steering-committee-agenda-mom.md> 

Approved unanimously commit   070fe086

#### Operations

Paul Buck reports

#### OpenAtom Agreement

Discussion on the status. There are ongoing discussions between the two organizations.


#### Development Actitivity Status

Davide reports.

Completed the second milestone of integration of OpenHarmony in Eclipse Oniro.


### Oniro WG SC All Members Meeting

Beyond the above, the participants in this section were:


#### AoB

- Discussion shifts to creating a Oniro compatible products and whether to apply for OH compatible at all. No resolution is taken.

- Oniro Compliance Toolchain is raising the interest of other EF projects and we are discussing to using it as a horizontal project to all the project that develop embedded OS. Davide also things that ovrall we have many projects in Oniro that can be called out to make them more general to "Embedded OS" projects. Alberto is tasked to inquire and help steering the offline discussion on groups restructuring/renaming also approaching other EF projects.

#### Resolutions

- to approve minutes for 2023wk10 


### All Members Meeting minutes

#### Addtional Attendees 

Luca Miotto (NOI)
Andrea Gallo (Linaro)
Sid Askary (IoTex)
Xinxin Fan (IoTex)

(Gaël left on other commitments)

#### AoB

No other business.

## Links and references

### Links

This is the list of relevant links:
* Repository where you will find the agenda and meeting minutes to be approved (private repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve>
* Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc>
* Oniro WG Program Plan: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf>
* Oniro WG Budget: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf>

This is the list to reference and other important notes to understand this document:
* AoB: Any Other Business
* MoM: Minutes of Meeting
* WG: Working Group
* SC: Steering Committee
* EF: Eclipse Foundation
* OH: Open Harmony

Meeting closes at 13:50 UTC
