# Oniro Working Group Steering Committee Meeting 2023wk46

* Date: 2023wk46 2023-16-11
* Time: from 14:00 to 15:00 UTC
    * Oniro WG SC Members Meeting
    * Open to All Members
* Chair: Suhail Khan
* Minutes taker:  Juan Rico
* Join via Zoom
    * Link: https://eclipse.zoom.us/j/85049355252
     * Meeting ID: 85049355252

All times in UTC time.

Meeting is started at 14:06, quorum is reached

# Agenda

* Approval of the meeting minutes from the 2023-09-07 2023wk40 Oniro WG SC Meeting (All) 5 minutes
* Approval of WG budget for 2024 - All 15 minutes
* OpenAtom Agreement Update - Juan R. 5 minutes
* Typefox collaboration with Oniro - Stefan S. 5 minutes
* Engineering update - Stefan S. 10 minutes
* IP toolchain status - Alberto P. 5 minutes
* Resolutions - 5 minutes
* Next Meeting - November 30 - 2 min
* AOB - 5 minutes

# Attendees

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

## Steering Committee

- Suhail Khan (Huawei primary) 
- Alberto Pianon  (Array Alternate, Silver Members representative)
-  Adrian O'Sullivan (Huawei alternate)

## Eclipse Foundation 

- Juan Rico


## Committers

- Stefan Schmidt (Huawei)

## Other Members
- Luca Miotto (Noi Techpark) 


## Regrets:
- Sharon Corbett  
- Gael Blondelle
- Thabang Mashologu
- Carlo Piana (Array primary, Silver Members representative)

# Minutes

## Discussion

### Approval of the meeting minutes from the 2023-11-02 2023wk44 Oniro WG SC Meeting
- 2023wk44 Oniro WG SC meeting's MoM needs to be approved at 2561b458

### Oniro WG 2024 Budget Approval
- Budget proposal was presented and the different categories included described.
- It was noted by the SC the need of a closing monitoring of the expenses- as a result a quarter review will be implemented in 2024
- The budget proposal was voted by the SC members
    - Huawei (Suahil Kahn) - strategic member - approved
    - Array (Alberto Pianon) - Silver Members representative - approved
    - Comitters Representative (Stefan Schmidt) - approved

### OpenAtom agreement update
This topic was postponed for the next SC in wk48.

### Oniro IDE contribution by Typefox 
- Stefan reported still WIP available in a public repository.
- It is currently under EMO and IP lab review.
- Once it is completed and available the WG will work on a blog post to promote this cooperation.

### IP toolchain status
- It was noted the impact on the shift from Yocto based to OpenHarmony - a follow up call between Array, Noi and Huawei will come to clarify how to keep progressing.

### Engineering Update
- Postponed to the next meeting.

## Resolutions
- To approve the meeting minutes from the 2023-11-02 2023wk44 Oniro WG SC Meeting at e16498e0
- RESOLVED, the Steering Committee approves unanimously the Eclipse Foundation budget as presented for 2024 acknowledging that it aligns with the available revenue in support of the program plan.
- The SC agrees on having a quarterly review of budget execution to monitor WG progress

## Next meeting
As planned for wk48 - Nov 30th

## AOB

No other business

## Links and references

### Links

This is the list of relevant links:

Repository where you will find the agenda and meeting minutes to be approved (private repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve

Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc

Oniro WG Program Plan: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf

Oniro WG Budget: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf


This is the list to reference and other important notes to understand this document:

AoB: Any Other Business
MoM: Minutes of Meeting
WG: Working Group
SC: Steering Committee
EF: Eclipse Foundation
OH: Open Harmony


Meeting closes at 15:23 UTC 
