# Oniro Working Group Steering Committee Meeting 2023wk30

* Date: 2023wk30 2023-07-27
* Time: from 13:00 to 14:00 UTC
    * Oniro WG SC Members Meeting
    * Open to All Members
* Chair: Sharon / Adrian
* Minutes taker:  Carlo Piana
* Minutes approved on 2023-09-07 at commit 109147e4
* Join via Zoom
    * Link: https://eclipse.zoom.us/j/84911609900 (Temporary this week only)
    * Meeting ID: 849 1160 9900 


All times in UTC time.

Meeting is started at 13:03

## Agenda

### Oniro WG SC meeting

* Welcome New Member - Futurewei (Sharon)
* Welcome Huawei's New Primary Representative - Suhail Khan (Sharon)
* Welcome Juan Rico, Oniro Program Manager (Sharon)
* Backfill ChairPerson Position (Sharon)
* Approval of the meeting minutes from the 2023-06-01 2023wk22 Oniro WG SC Meeting (All)
* OpenAtom Agreement Update - Gael B./Thabang M. 10 minutes
* Development Activity Update - Suhail K. 10 minutes
* Renaming of Projects Update - Alberto P. 10 Minutes
* EclipseCon Update - Adrian O. - 5 Minutes
* Next Meeting - Reschedule from August 3? - (All) 2.5 minutes 
* Resolutions - 2.5 minutes
* AoB - 2.5 minutes

## Steering Committee Attendees
Steering Committee Members (Quorum =  2 of 3 for this Meeting):

<!-- * Suhail Khan (Huawei primary) -->
* Adrian O'Sullivan (Huawei alternate)
* Carlo Piana   (Array, Silver Members representative)


## Steering Committee Regrets:
* Stefan Schmidt (Elected Committer representative)


## Eclipse Foundation Attendees

* Sharon Corbett
* Juan Rico
<!-- * Gael Blondelle -->
* Thabang Mashologu


## Other Members

Luca Miotto (NOI)

# Minutes

Quorum reached at  13:05 UTC

### Oniro WG SC meeting minutes

#### Discussion

The SC wants to thank Davide Ricci who has moved on to other important matters.

Sahail Khan cannot attend and sends his regrets, Adrian reports.

The SC welcomes Juan Rico as the new WG Program Manager.

Introducing Thabang as VP Community Development at Eclipse.

Discussing the Chairman position. Adrian informed Eclipse Suhail is available to filling that role. Carlo moves for appointing Suhail.

Adrian takes the lead.

No objections approving them. Resolution is passed.

Thaban reports on Open Atom negotiation.

Adrian reports on an issue raised by Jaroslaw Marek https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/3378 to mirroro OpenHarmony to become OH compliant by design.

Carlo reports on progress for the Compliance toolchain to be adopted as a horizontal project, at IoT-EDGE.

EclipseCon. Half day community is scheduled. Good idea to move to the afternoon of Monday, if at all possible. Invite  people from OH community. 

No chances to have quorum for August 3, push it back to the first Thursday of September.


#### Approval of the meeting minutes from the 2023-06-01 2023wk22 Oniro WG SC Meeting

2023wk22 Oniro WG SC meeting's MoM needs to be approved.
MoM #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/agenda-minutes-to-approve/2023wk22-eclipse-oniro-wg-steering-committee-agenda-mom.md
   

## Resolutions

To appoint Suhail as Chairman of the SC

To approve meeting minutes from the 2023-06-01 2023wk22 Oniro WG SC Meeting at commit a63da70c

## AOB

None.

## Links and references

### Links
This is the list of relevant links:

Repository where you will find the agenda and meeting minutes to be approved (private repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve

Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc

Oniro WG Program Plan: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf

Oniro WG Budget: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf


This is the list to reference and other important notes to understand this document:

AoB: Any Other Business
MoM: Minutes of Meeting
WG: Working Group
SC: Steering Committee
EF: Eclipse Foundation
OH: Open Harmony



Meeting closes at 13:44 UTC
