# Oniro Working Group Steering Committee Meeting 2023wk22

* Date: 2023wk22 2023-06-01
* Time: from 13:00 to 14:00 UTC
    * Oniro WG SC Members Meeting
    * Open to All Members
* Chair: Davide Ricci
* Minutes taker:  Alberto Pianon
* Join via Zoom
    * Link: https://eclipse.zoom.us/j/86710449041?pwd=dmk1OW56emQvU1YvSjBUY1AyOHIzUT09
    * Meeting ID: 867 1044 9041 Passcode: 293617

Minutes approved on 2023-07-27 at a63da70c

## Agenda

### Oniro WG SC meeting

* Welcome New Member - Software Mansion as Newest Silver Member - Davide R 5 minutes
* Approval of the meeting minutes from the 2023-05-04 2023wk18 Oniro WG SC Meeting - Davide R. 5 minutes
* PM Hire Update - Paul B. 5 minutes
* OpenAtom Agreement Update - Paul B. 10 minutes
* Development Activity Update - Davide R./Stefan S. 10 minutes
* Renaming of Projects Update - Alberto P. 10 Minutes
* EclipseCon Update - Davide R. - 5 Minutes
* Resolutions - 2.5 minutes
* AoB - 2.5 minutes

## Steering Committee Attendees
Steering Committee Members (Quorum =  2 of 3 for this Meeting):

* Davide Ricci (Huawei)
* Adrian O'Sullivan (Huawei alternate)
* Alberto Pianon (Array Alternate)
* Stefan Schmidt (Elected Committer representative)

## Steering Committee Regrets:

* Carlo Piana   (Array, Silver Members representative)

## Eclipse Foundation Attendees

* Paul Buck (Eclipse)


## Other Members

* Patrick Ohnewein (NOI)
* Luca Miotto (NOI)
* Nina Ignasik (Software Mansion)
* Caroline Kulig (Software Mansion)

# Minutes

Quorum reached at  13:00 UTC

### Oniro WG SC meeting minutes

#### Approval of the meeting minutes from the 2023-05-04 2023wk18 Oniro WG SC Meeting
2023wk18 Oniro WG SC meeting's MoM needs to be approved.
MoM #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/agenda-minutes-to-approve/2023wk18-eclipse-oniro-wg-steering-committee-agenda-mom.md
   
Unanymously approved

### Welcome New Member  

Roundtable presentation to welcome new member Software Mansion

### PM Hire Update  

Hired a new PM for Oniro, based in Spain, part-time (shared with the Adoptium project), will start on July 17th

### OpenAtom Agreement Update 

Reached consensus on the agreement language; it is going to be translated into Chinese and sent to the boards of both Foundations for final review and execution. 

Software Mansion members are briefed on the scope and the purpose of the agreement.

### Development Activity Update  

Compliance for OpenHarmony certification substantially reached both for Qemu and Raspberry PI platforms. Needed both to fix text cases in order to make them work in the Oniro context, and to make patches on the Oniro/Yocto side. Planning to upstream most of the patches to OpenHarmony in order to reduce the gap and the maintenance costs for the next releases.

### Renaming of Projects Update  

Contacted Leda (EF Project, sponsored by Bosch/ETAS) and Seco (EF member). Both are really interested and second the whole idea of rebranding and repositioning the Oniro-core project as a general horizontal project that may be used in many different contexts in the IoT and automotive fields. They both may be customers of such project by integrating it in their existing software stack and hardware integration/SDK projects. The risk that they see is that they could not afford the maintenance of such a project, so gathering a critical mass of potentially interested actors/contributors is key to the success of the initiative. To this purpose, next week there will be a meeting with Frédéric Desbiens (EF IoT PM) in order to identify other EF members who may actually interested in the initiative. A first draft of the overall proposal will be prepared after collecting feedback by all interested parties.

### Resolutions

Meeting minutes approval (see above)

### AoB

Paul, Davide and Adrian brief new member Software Mansion about the processes and procedures governing SC meetings and activities

## Links and references

### Links
This is the list of relevant links:

Repository where you will find the agenda and meeting minutes to be approved (private repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve

Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc

Oniro WG Program Plan: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf

Oniro WG Budget: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf


This is the list to reference and other important notes to understand this document:

AoB: Any Other Business
MoM: Minutes of Meeting
WG: Working Group
SC: Steering Committee
EF: Eclipse Foundation
OH: Open Harmony



Meeting closes at 14:00 UTC
