
# Oniro Working Group Steering Committee Meeting 2023wk18

* Date: 2023wk18 2023-05-04
* Time: from 13:00 to 14:00 UTC
    * Oniro WG SC Members meeting: from 13:00 to 13:30 UTC
    * Oniro WG All Members meeting: from 13:31 to 14:00 UTC
* Chair: Davide Ricci
* Minutes taker:  Carlo Piana
* Join via Zoom
    * Link: https://eclipse.zoom.us/j/86710449041?pwd=dmk1OW56emQvU1YvSjBUY1AyOHIzUT09
    * Meeting ID: 867 1044 9041 Passcode: 293617


## Agenda

### Oniro WG SC meeting

* Approval of the meeting minutes from the 2023-04-06 2023wk14 Oniro WG SC Meeting - Davide R. 5 minutes
* Operations Update (Elections, PM Hire, and so on) - Sharon C. / Paul B. 5 minutes
* OpenAtom Agreement Update - Paul B. 5 minutes
* Development Activity Update - Davide R. 2.5 minutes
* Renaming of Projects - All 5 minutes
* Membership Update - Sharon C. / Paul B. 2.5 minutes
* Meeting Format Upate Proposal - Sharon C. 2.5 minutes
* Resolutions - 2.5 minutes
* AoB - 2.5 minutes


#### Oniro Working Group All Members meeting
No formal agenda set

## Attendees

* Davide Ricci (Huawei)
* Adrian O'Sullivan (Huawei alternate)
* Carlo Piana   (Array, Silver Members representative)
* Alberto Pianon (Array Alternate)
* Stefan Schmidt (Elected Committer representative)
* Paul Buck (Eclipse)
* Sharon Corbett (Eclipse)

### Oniro WG SC Meeting
Steering Committee Members (Quorum =  2 of 3 for this Meeting):

* Carlo Piana (Array - Elected Silver Member representative)
* Davide Ricci (Huawei)
* Stefan Schmidt (Elected Committer representative)

Steering Committee Regrets:


Eclipse Foundation:


* Gaël Blondelle


# Minutes

Quorum reached at  15:02

### Oniro WG SC meeting minutes

#### Approval of the meeting minutes from the 2023-04-06 2023wk14 Oniro WG SC Meeting
2023wk14 Oniro WG SC meeting's MoM needs to be approved.
MoM #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/agenda-minutes-to-approve/2023wk14-eclipse-oniro-wg-steering-committee-agenda-mom.md
   

Unanymously approved


#### Election

Sharon reports on Silver Members representative election and Committer representative, congratulations.

#### Open Atom agreeement update

Still up in the air. 

Also Eclipse is in the process of recruiting a Community Manager, but this is also linked to the execution of the OA agreement

#### Development Activity Update 

Stefan reports

Also future requirements of relating to OA for interoperability/compatibility and related activities is discussed.

#### Renaming of Projects

Alberto reports on talks with members to update the Toolchain group. Discussing also about the relationship with Leda and the reasonable success applying.

It could help if we could monitor the current situation and desidered outcome of the project. Probably, if Leda gets interested, we could change the name of the actual OS image project.

On the top of renaming, re-architecting could be a requirement.

The goal is to have a clear idea of the rearchitecturing of the subprojects under the Oniro project umbrella or otherwise. 
Mapping structure and value for each member. Next steps map how the nexst snapshot should look like.

Problem / opportunity statement: some technology developed by the Oniro team has value to other EF projects

Goal of the working group: take a snapshopt at current set of projects / subprojects, identify value to different groups 
(and groups themsleves and propose a possible rearchitecturing of the projects/ subproects in order oreach maximum impact and adoption

Alberto shall take the leaed on this.


#### Membership Update - Sharon C. / Paul B. 

Qbit informed they will withdraw.

Software mansion are joining.

#### Meeting Format Upate Proposal

Sharon C Suggest we could make the meetings more open in general. Leaving the "private discussion" if any happening via email. Re-evaluation in six months.

Davide moves, no objectsions, approved.


#### Resolutions

To approve minutes for 2023wk14 at 6f469dfe

To reformat the meeting format to be entirely open

#### All Members Meeting minutes

Addtional Attendees:

Patrick Ohnewein


#### AoB

Talks on stolen marmelade at airport back from London (Carlo)

## Links and references

### Links
This is the list of relevant links:

Repository where you will find the agenda and meeting minutes to be approved (private repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve

Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc

Oniro WG Program Plan: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf

Oniro WG Budget: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf


This is the list to reference and other important notes to understand this document:

AoB: Any Other Business
MoM: Minutes of Meeting
WG: Working Group
SC: Steering Committee
EF: Eclipse Foundation
OH: Open Harmony



Meeting closes at 1400 UTC
