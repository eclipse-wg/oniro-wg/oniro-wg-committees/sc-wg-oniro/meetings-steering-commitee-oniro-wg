# Oniro Working Group Steering Committee Meeting 2023wk44

* Date: 2023wk44 2023-02-11
* Time: from 14:00 to 15:00 UTC
    * Oniro WG SC Members Meeting
    * Open to All Members
* Chair: Suhail Khan
* Minutes taker:  Carlo Piana
* Join via Zoom
    * Link: https://eclipse.zoom.us/j/85933678825
    * Meeting ID: 859 3367 8825

All times in UTC time.

Meeting is started at 14:06, quorum is reached

# Agenda

* Approval of the meeting minutes from the 2023-09-07 2023wk40 Oniro WG SC Meeting (All) 5 minutes
* OpenAtom Agreement Update - Juan R. 5 minutes
* Approval of Program Plan for 2024 - All - 20 minutes 
* Outcomes of Eclipsecon - All - 10 minutes
* Resolutions - 5 minutes
* Next Meeting - November 16 - 2 min
* AOB - 5 minutes

# Attendees

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

## Steering Committee

- Suhail Khan (Huawei primary) 
- Carlo Piana   (Array, Silver Members representative)

## Eclipse Foundation 

- Juan Rico

## Committers

- Stefan Schmidt (Huawei)

## Other Members

- Luca Miotto (Noi Techpark)
- Patrick Ohnewein (Noi Techpark) -- from 14:10
- Bill Fletcher (Linaro) -- from 14:28



## Regrets:

- Adrian O'Sullivan (Huawei alternate)
- Alberto Pianon (Array Alternate)
- Sharon Corbett  
- Gael Blondelle
- Thabang Mashologu

# Minutes

## Discussion

### Approval of the meeting minutes from the 2023-06-01 2023wk40 Oniro WG SC Meeting

2023wk40 Oniro WG SC meeting's MoM needs to be approved. e16498e0

## Other items in the Agenda

### OpenAtom Agreement Update 

Juan reports

### Program Plan

Discussione ensues. Stefan, Juan, Suhai and Carlo discuss the relevant aspects and high level aspects of the Program Plan.

Discussing on Slide 24 (stopping development in Yocto). No current members is currently interested in Yocto, so there is a suggestion that if a new member becomes interested, we would keep it. 

We approve the program plan without slide 24, distribute and approve it.

## Eclipsecon

### sharing compliance information for public consumption

Report on private meeting to discuss with many stakeholders. General decision to go for it, barring working out practical details.

### BoaF

Juan reports.

## Resolutions

To approve of the meeting minutes from the 2023-06-01 2023wk40 Oniro WG SC Meeting at e16498e0

To tentatively approve Program Plan, pending circulation of the final version.


## AOB

No other business

## Links and references

### Links

This is the list of relevant links:

Repository where you will find the agenda and meeting minutes to be approved (private repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve

Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc

Oniro WG Program Plan: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf

Oniro WG Budget: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf


This is the list to reference and other important notes to understand this document:

AoB: Any Other Business
MoM: Minutes of Meeting
WG: Working Group
SC: Steering Committee
EF: Eclipse Foundation
OH: Open Harmony


Meeting closes at 15:00 UTC 
