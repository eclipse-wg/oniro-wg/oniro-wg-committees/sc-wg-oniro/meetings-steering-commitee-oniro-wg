# Oniro Working Group Steering Committee Meeting 2023wk40

* Date: 2023wk40 2023-10-06
* Time: from 13:00 to 14:00 UTC
    * Oniro WG SC Members Meeting
    * Open to All Members
* Chair: Suhail Khan
* Minutes taker:  Carlo Piana
* Join via Zoom
    * Link: https://eclipse.zoom.us/j/85933678825
    * Meeting ID: 859 3367 8825

All times in UTC time.

Meeting is started at 13:05, quorum is reached at 13:10 UTC

Minutes approved on 2023-11-02 14:09 UTC

# Agenda

* Approval of the meeting minutes from the 2023-09-07 2023wk36 Oniro WG SC Meeting (All)
* OpenAtom Agreement Update - Gael B./Juan R. 5 minutes
* Development Activity Update - Stefan S  5 minutes
* EclipseCon Update - Suhail K./Juan R. - 10 Minutes 
* SC Meeting Periodicity - 10 minutes - Juan
* Next Meeting - November 2 - (All) 2.5 minutes
* Resolutions - 2.5 minutes
* AoB - 2.5 minutes

# Attendees

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

## Steering Committee

- Suhail Khan (Huawei primary) 
- Alberto Pianon (Array Alternate)
- Carlo Piana   (Array, Silver Members representative)
- Adrian O'Sullivan (Huawei alternate)

## Eclipse Foundation 

- Juan Rico

## Committers

- Stefan Schmidt (Huawei)

## Other Members


## Regrets:

- Sharon Corbett  
- Gael Blondelle
- Thabang Mashologu

# Minutes

## Discussion

### Approval of the meeting minutes from the 2023-06-01 2023wk36 Oniro WG SC Meeting

2023wk36 Oniro WG SC meeting's MoM needs to be approved.

MoM #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/agenda-minutes-to-approve/2023wk36-eclipse-oniro-wg-steering-committee-agenda-mom.md
   
Approved at commit  5c9ff7bc

### Other items in the Agenda

Juan reports on evolution on OpenAtom agreeement. There is a limited number of open points to discuss.

Stefan reports on evolutions of Oniro, mainly in connection with OpenHarmony. Demo + Mirror

Juan reports on the discussion we had yesterday for coordination of Oniro participation to EclipseCon

Starting a discussion on whether the meetings of the SC should be bi-monthly. Consensus is reached. Next meeting will be held on November 2nd anyway.

## Resolutions

To approve the previous minutes.

To have bi-monthly (i.e. every two weeks) meetings. 


## AOB

We have a refreshing of the website. Early next week we will have a more consistent and correct description. Also we need to have updated links to the current version of all code we have, including the IP compliance toolchain.

## Links and references

### Links

This is the list of relevant links:

Repository where you will find the agenda and meeting minutes to be approved (private repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve

Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc

Oniro WG Program Plan: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf

Oniro WG Budget: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf


This is the list to reference and other important notes to understand this document:

AoB: Any Other Business
MoM: Minutes of Meeting
WG: Working Group
SC: Steering Committee
EF: Eclipse Foundation
OH: Open Harmony


Meeting closes at 13:41 UTC 
