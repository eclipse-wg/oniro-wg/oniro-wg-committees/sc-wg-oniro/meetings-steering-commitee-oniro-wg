# Oniro Working Group Steering Committee Meeting 2023wk05

* Date: 2023wk05 2023-02-02
* Time: from 14:00 to 15:00 UTC
   * Oniro WG SC All Members meeting: from 14:00 to 14:30 UTC
	* Oniro WG SC All Members meeting: from 14:31 to 15:00 UTC
* Chair: Davide Ricci 
* Minutes taker: Carlo Piana
* Join via Zoom
   * Link: <https://eclipse.zoom.us/j/86710449041?pwd=dmk1OW56emQvU1YvSjBUY1AyOHIzUT09>
   * Meeting ID: 867 1044 9041 Passcode: 293617

## Agenda

Oniro WG SC meeting

* Approval of the meeting minutes from the 2022-11-24 2022wk47 Oniro WG SC Meeting - Davide R. 5 minutes
* Oniro WG Spec Committee. Next steps. - Sharon 5 minutes
* Elections at Oniro - Sharon 10 minutes
* Resolutions - 5 minutes
* AoB - 5 minutes

Oniro WG SC All Members meeting

No formal agenda set


## Attendees


### Oniro WG SC Meeting

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

* Carlo Piana - Array. Silver Members representative
 (* Alberto Pianon - Array alternate. Silver Members)
* Davide Ricci - Huawei (* Adrian O'Sullivan - Huawei alternate)

Steering Committee Regrets:

* Amit Kucheria - Committers

Eclipse Foundation:

[comment]: # (* Agustin Benito Bethencourt)
[comment]: # (* Paul Buck )
* Sharon Corbbet

### Oniro WG SC All Members Meeting

Beyond the above, the participants in this section were:

* Steeve Baudy - IoTex
* Xixin Fan - IoTex
* Luca Tavanti - Cubit


## Minutes

Quorum is reached.

Discussing the absence of PMC from meetings. Discussing the frequency of SC meetings.

Carlo moves to switch to once-a-month meeting frequency. Uninamously approved.


### Oniro WG SC meeting minutes

#### Approval of the meeting minutes from the 2022-11-24 2022wk47 Oniro WG SC Meeting

2022wk47 Oniro WG SC meeting was the last formal meeting of 2022. The MoM needs to be approved.
MoM #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/agenda-minutes-to-approve/2022wk47-eclipse-oniro-wg-steering-committee-agenda-mom.md> 

Approved unanimously commit  df444d60 


#### Oniro WG Spec Committee. Next steps.

Sharon explains the situation and why it was decided to have a spec committee in the first place, current status and way forward.

Since at the time being there is no business to discuss, it will remain idle until there will be specifications to write.


#### Elections at Oniro

Invite to "socialize" the idea that elections are open again in early April. 

#### Resolutions

- switching to a monthly meeting cadence. 
- approve meetins WK 47


#### AoB  (Open Floor)


Meeting adjourns at 14:25 UTC until the public session.

### Oniro WG SC All Members meeting

Opens at 14:31

Sharon announces tha the Budget was approved by ED and the elections will be taking place in early April. Also we communicate the change on cadence.


#### AoB

 
Status: overview.

The EF IT team has created the pipelines but now, in order to put them in production, effort from Oniro engineers with knowledge on the tooling and the creation of the distribution is needed. 
   * Review that the existing configurations and implementations are the ones desired.
   * Test the current pipelines to make sure results are like the ones obtained with the existing one, given that the environment is now different.
   * Plan to put the deployment pipelines in production and decomision the existing ones.
   
For this activity to be completed, it requires a sustained effort from engineers and a gradual transition into production to avoid negative impact.

In the meantime, the chat service, priority 2, is gaining traction. This service is in alpha testing phase.
* A testing instance of a matrix server has been implemented, including a web client.
   * The service will be moved now to a staging instance.
* EF staff is testing together with EF IT such instance to detect the most obvious bugs and required improvements.
   * This testing includes two clients, NeoChat and Element.
* On Oniro space has been created with the following channels:
   * #oniro-wg
   * #oniro-dev
* Next steps:
   * The staging service needs to comply with the EF legal and IP frameworks. Investigations and experimentation to defines measures to meet them are ongoing.
   * Additional staf at EF will test the instance.
   * A room for IP and license compliance under the EF space will be created to be used by Oniro, IoT, Edge and automotive projects.
   * We will kick-off in the coming weeks the beta-testing phase, where Oniro developers, familiar with matrix, will join as beta testers.

Marketing Plan 2023 
* Oniro WG Marketing Plan is under development. Please join the Oniro marketing mailing list and participate in the discussions.
   * oniro-wg-marketing mailing list (public): #link <https://accounts.eclipse.org/mailing-list/oniro-wg-marketing>
   * Marketing PLan 2023 draft: #link <https://docs.google.com/presentation/d/1KCEuossZ1QOMqXbn65Tpl2O4Wl0qsdJe1hD8IApSwTM/edit#slide=id.g133004a9946_7_45>


## Links and references

### Links

This is the list of relevant links:
* Repository where you will find the agenda and meeting minutes to be approved (private repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve>
* Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc>
* Oniro WG Program Plan: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf>
* Oniro WG Budget: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf>

This is the list to reference and other important notes to understand this document:
* AoB: Any Other Business
* MoM: Minutes of Meeting
* WG: Working Group
* SC: Steering Committee
* EF: Eclipse Foundation
* OH: Open Harmony

Meeting closes at 14:43 UTC

All times UTC unless expressly mentioned
