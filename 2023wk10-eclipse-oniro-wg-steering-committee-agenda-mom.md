# Oniro Working Group Steering Committee Meeting 2023wk10

* Date: 2023wk10 2023-03-09
* Time: from 14:00 to 15:00 UTC
   * Oniro WG SC Members meeting: from 14:00 to 14:30 UTC
	* Oniro WG All Members meeting: from 14:31 to 15:00 UTC
* Chair: Davide Ricci 
* Minutes taker: Alberto Pianon
* Join via Zoom
   * Link: <https://eclipse.zoom.us/j/86710449041?pwd=dmk1OW56emQvU1YvSjBUY1AyOHIzUT09>
   * Meeting ID: 867 1044 9041 Passcode: 293617

* Approved 2023-04-06 at 070fe086 commit

## Agenda

Oniro WG SC meeting

* Approval of the meeting minutes from the 2023-02-22 2023wk05 Oniro WG SC Meeting - Davide R. 5 minutes
* Eclipse Foundation Update - Paul B. 5 minutes
* Operations Update (Trademark Registration, Chat Service, Elections) - Sharon C. 5 minutes 
* OpenAtom Agreement Status - Davide R./Paul B. 5 minutes
* OpenHarmony features/compatibility progress - David R. 5 minutes
* Resolutions - 2.5 minutes
* AoB - 2.5 minutes

Oniro Working Group All Members meeting

No formal agenda set

## Attendees

* Davide Ricci (Huawei)
* Adrian O'Sullivan (Huawei alternate)
* Alberto Pianon (Array, Silver Members representative)

### Oniro WG SC Meeting

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

* Alberto Pianon (Array - Elected Silver Member representative)
* Davide Ricci (Huawei)

Steering Committee Regrets:

* Carlo Piana (Array)
* Amit Kucheria (Committers)

Eclipse Foundation:

* Paul Buck
* Sharon Corbett

### Oniro WG SC All Members Meeting

Beyond the above, the participants in this section were:

* Steeve (IoTex)
* Xinxin Fan (IoTex)
* Stefan Schmidt (Huawei)

# Minutes

Quorum is reached.

### Oniro WG SC meeting minutes

#### Approval of the meeting minutes from the 2023-02-22 2023wk05 Oniro WG SC Meeting

2023wk05 Oniro WG SC meeting's MoM needs to be approved.

MoM #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/agenda-minutes-to-approve/2023wk05-eclipse-oniro-wg-steering-committee-agenda-mom.md> 

Approved unanimously commit  dfb2e42e

### Eclipse Foundation Update

Discussing the ongoing process for hiring a new Program Manager for Oniro (job announcement link <https://www.linkedin.com/jobs/view/3509598299/>)

Discussing next steps for the Oniro Project (implementing application frameworks, other EF projects may be interested/involved)

### Operations Update

Sharon provides updates on trademark registration (objection to trademark registration in Brasil, decided not to proceed; registered in Australia and in the EU), on the chat service (will be launched soon), Specification Committee elections

### OpenAtom agreement status

Draft has been exchanged this week, no specific time for closure, but parties are confident it will be closed soon.

### OpenHarmony features/compatibility progress

Oniro/meta-openharmony should be ready for OpenHarmony Certification soon. Next year the Oniro Project will be working upstream directly with OpenHarmony, by forking repos amd submitting patches upstream.

#### Resolutions

- approve meetings WK 05


#### AoB

(All Members Meeting)

focusing on maintenance (bug and security fixes) of Oniro 2.0

Discussing about the possibility and the rationale for developing a meta-iotex layer for Oniro.


## Links and references

### Links

This is the list of relevant links:
* Repository where you will find the agenda and meeting minutes to be approved (private repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve>
* Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc>
* Oniro WG Program Plan: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf>
* Oniro WG Budget: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf>

This is the list to reference and other important notes to understand this document:
* AoB: Any Other Business
* MoM: Minutes of Meeting
* WG: Working Group
* SC: Steering Committee
* EF: Eclipse Foundation
* OH: Open Harmony

Meeting closes at 14:50 UTC
