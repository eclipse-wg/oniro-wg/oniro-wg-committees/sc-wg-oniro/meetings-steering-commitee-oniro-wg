# Summary of the brainstorming session

## General information

* Schedule: 2022-10-06 from 16:00 to 17:00 CEST (50 min. effective work)
* Tooling: Zoom + Miro
* Preparations and facilitation: Agustin
* Instructions: check the link below
* Participants: Andrei G, Gianluca V. (only present), Ettore, toscalix, Antonio M., Adrian O'S., Chiara and Francesco

This is the output of the session. You can see more details there, including the instructions of the exercises

Link: https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program_plan_2023_drafts/brainstorming-session-20221006-Oniro-Program-Plan-2023.pdf

## Summary of the activities

This activity should last 120 minutes to provide time to discuss the ideas.

### High priority ideas

* Value Prop Definition -Why Oniro - What's In It For Me - Worth It An Investment - Go To Market + Create a concise value proposition document/deck: 5 votes, 19 points
* Get a product company that can use Oniro as base for production OS: 5 votes, 19 points
* Have 2+ new main members on board for 2023: 5 votes, 13 points
* Focus budget more on backing Oniro Core IT/IP , than to Business Development, Marketing and Program Mgmt to support Oniro
* Core Platform MVP project: 4 votes, 4 points
* Organize tutorials or hackathons at events (conferences, fairs, ...) the goal is to speed-up the learning: 3 votes, 3 points

### Medium priority ideas

* why should I choose to use Oniro instead of X? 5 votes, 21 points
* Foster And Increase The Number Of Collaborations With Other Open Source Projects Inside And Outside Of EF. 5 votes, 15 points
* Create an EDDIE blueprint/demonstrate the distributed capabilities of Oniro. 5 votes, 11 points
* Engineering team plan: 4 votes, 8 points
* Maintain 2.0  release for bugs, CVEs, etc: 5 votes, 7 points
* Do we have in the oniro-core team all the capabilities we need to develop the OSs? 3 votes, 7 points
* New Value Prop used for future pipeline, matching new reduced Oniro scope for 2023. 3 votes, 7 points 
* What will be the main target audience for Oniro in 2023? 4votes, 4 points
* continue alignment with OpenHarmony ( meta-openharmomy ) and associated base compatibility. 3 votes, 3 points
* Review Pipeline Management And Goals To Define The 2023 Strategy For Member Acquisition And Retention 3 votes, 3 points


### Bulk of ideas

* Yocto compatibility
* Maintain 2.0  release for bugs, CVEs, etc
* Resources for CI and CI maintenance
* Contribute back to upstream software components
* Have 2+ new main members on board for 2023
* Engineering team plan
* why should I choose to use Oniro instead of X?
* organize tutorials or hackathons at events (conferences, fairs, ...) the goal is to speed-up the learning
* Create a concise value proposition document/deck
* Make oniro-core smaller so create new projects
* attract other EF projects to work with us.
* Prioritised target leads/prospects profiles. Can we make such list or not at first?
* What will be the main target audience for Oniro in 2023?
* IT migration completed
* Do we have in the oniro-core team all the capabilities we need to develop the OSs?
* What is our maintenance promise?
* Adapt the Oniro WG Charter
* Will we be able to create specifications out of our Oniro releases during 2023?
* Focus on achieving at least one design win
* Reduces Target of + 4 new members in 2023
* Start Oniro brand and compatibility before end of 2023
* lower event participation when compared to 2022, target instead specific high priority events
* Focus on Oniro Core Platform MVP first, then any additional Add-Ons second. Add-ons can be driven by specific member requirements and resourced accordingly
* Reduced focus on Blueprints in 2023, only if member wants to drive specific business case showcase, reduce the target number in the Program plan compared to 2023
* Value Prop Definition -Why Oniro - What's In It For Me - Worth It An Investment - Go To Market
* Share Event List Among All Wg Rep For Better Cross-Promotion
* Blueprint Roadmap To Influence Specific Verticals And Increase Community Contributions And Patticipations
* Define Communication Milestones And Goals
* Foster And Increase The Number Of Collaborations With Other Open Source Projects Inside And Outside Of Ef
* Start Discussing The Openharmony Compatibility Perspective And Co-Branding Strategy
* Review Engineering Support Plan In Light Of The Latest Changes On The Engineering Team
* Review Pipeline Management And Goals To Define The 2023 Strategy For Member Acquisition And Retention
* Create an EDDIE blueprint/demonstrate the distributed capabilities of Oniro
* Get a product company that can use Oniro as base for production OS
* What is the certification  process for an Oniro Product?

## Retrospective

* Too much zooming.
   * Sticky notes should be bigger so zooming is not needed.
* More time to describe and discuss the ideas needed.
   * The activity should be between 90 and 120 minutes
* We could not get into the prioritization of the moderate priority ideas.
   * The activity should be between 90 and 120 minutes
* We could not get into categorization.
   * This is not dramatic at this point given that we were a reduced number of people. 
