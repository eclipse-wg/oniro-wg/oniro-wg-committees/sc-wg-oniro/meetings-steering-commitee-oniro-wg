# Oniro Working Group Steering Committee Meeting 2024wk18

* Date: 2024wk18 2024-5-2
* Time: from 13:00 to 14:00 UTC
    * Oniro WG SC Members Meeting
    * Open to All Members
* Chair: Suhail Kahn 
* Minutes taker:  Carlo Piana
* Join via Zoom
    * Link: https://eclipse.zoom.us/j/85049355252
     * Meeting ID: 85049355252

All times in UTC time.

Meeting starts at 14:05, as quorum is reached

# Agenda

- Approval of the meeting minutes of 2024wk14 SC meeting - All
- Update on the Open Harmony - Oniro collaboration - Juan
- Update of technical activities - 10 min 
- Update of marketing activities - 10 min 
- AOB

# Attendees

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

## Steering Committee

- Adrian O'Sullivan (Huawei Alternate) 
- Carlo Piana (Array representative)
- Suhail Kahn (Huawei representative)
- Alberto Pianon (Array Alternate) 

## Eclipse Foundation 

- Juan Rico

## Committers

- no representatvies

## Other Members

- Patrick Ohnewein (NOI)
- Luca Miotto (NOI)

## Regrets:

- Jaroslaw Marek (Huawei) 
- Stefan Schmidt (Huawei)
- Sharon Corbett  
- Gael Blondelle
- Thabang Mashologu

# Minutes

## Discussion

### Approval of the meeting minutes from the 2024wk14 2024-4-4 Oniro WG SC Meeting

Minutes are apprved at 6277c5b6

## Other items in the Agenda

### Update on the Open Harmony - Oniro collaboration

Juan reports

### Update of technical activities

Juan reports about fast progress. Especially on app-store (inspired on F-droid)

### Update of marketing activities

Juan reports PR about integration. More activities and closer interaction with Eclipse marketing people. SC will be more invited to interact to provide marketing material.

Carlo reports about being invited to Linaro Connect 2024 (Madrid) and Carlo and Alberto will present  the Eclipse Oniro IP toolchain.

Various other events are reported, Juan invites to keep more track of them (on gitlab) as they become committed.

## AOB

None tabled

## Resolutions

- To approve the last minutes for 2024wk14 at 6277c5b6

## Links and references

### Links

This is the list of relevant links:

Repository where you will find the agenda and meeting minutes to be approved (private repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve

Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc

Oniro WG Program Plan: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf

Oniro WG Budget: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf


This is the list to reference and other important notes to understand this document:

AoB: Any Other Business
MoM: Minutes of Meeting
WG: Working Group
SC: Steering Committee
EF: Eclipse Foundation
OH: Open Harmony


Meeting closes at 13:27 UTC 
