# Oniro Working Group Steering Committee Meeting 2024wk8

* Date: 2024wk8 2024-2-22
* Time: from 14:00 to 15:00 UTC
    * Oniro WG SC Members Meeting
    * Open to All Members
* Chair: Suhail Khan
* Minutes taker:  Juan Rico
* Join via Zoom
    * Link: https://eclipse.zoom.us/j/85049355252
     * Meeting ID: 85049355252
* Approved at 65f60678 on 2024-03-07

All times in UTC time.

Meeting starts at 14:06, as quorum is reached

# Agenda

- Approval of the meeting minutes from the 2024-01-25 2024wk6 Oniro WG SC Meeting (All) 5 minutes
- Roadmap planning- Explaining next steps - 5 min
- Oniro Communication Architecture document - 10 min
- Oniro FAQ document - 5 min
- Pipeline review - 15 min
- AOB

# Attendees

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

## Steering Committee

- Suhail Khan (Huawei primary) 
- Alberto Pianon (Array Alternate)

## Eclipse Foundation 

- Juan Rico

## Committers

- Stefan Schmidt (Huawei)

## Other Members

- Luca Miotto (NOI)
- Patrick Ohnewein (NOI)

## Regrets:

- Adrian O'Sullivan (Huawei alternate)
- Carlo Piana (Array) 
- Sharon Corbett  
- Gael Blondelle
- Thabang Mashologu

# Minutes

## Discussion

### Approval of the meeting minutes from the 2024-1-25 2024wk6 Oniro WG SC Meeting
- MoM were approved at commit 794216759801957cff0369edfd3049a210143eac

## Other items in the Agenda
### Oniro Roadmap for 2024
- After the collection of the individual inputs they are currently sourted as features for building the product Roadmap
- There will be some individual contacts to clarify some points
- it will be created a roadmap with timeline to be agreed among all WG members.

### Oniro Communication Architecture document
- This document is going to be developed under Marketing Committee supervision
- It will cover the main messages that Oniro, its projects will transfer to the outside
- The document will be owned by the SC since it is a key asset for communicating the project

### Oniro FAQ document
 - It was originally developed in 2022
 - It requires cleaning and update
 - It is the place for sharing those questions that WG members received to consolidate a clear answer as working group positioning

## Pipeline review
 - It was cover the current status of the pipeline - available here - https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/pipeline-oniro/-/boards/1203
 - It is requested to contribute to the list and count on EF for that.

## AOB
 - Planning of activities for SFSCON. If it is agreed the organization a dev day, it is needed to have enough time for planning.


## Resolutions
- To approve MoM at commit 794216759801957cff0369edfd3049a210143eac


## Links and references

### Links

This is the list of relevant links:

Repository where you will find the agenda and meeting minutes to be approved (private repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve

Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc

Oniro WG Program Plan: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf

Oniro WG Budget: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf


This is the list to reference and other important notes to understand this document:

AoB: Any Other Business
MoM: Minutes of Meeting
WG: Working Group
SC: Steering Committee
EF: Eclipse Foundation
OH: Open Harmony


Meeting closes at 14:25 UTC 
