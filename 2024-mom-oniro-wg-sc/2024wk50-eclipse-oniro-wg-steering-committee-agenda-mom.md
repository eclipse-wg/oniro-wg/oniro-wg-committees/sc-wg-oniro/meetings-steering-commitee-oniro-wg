# Oniro Working Group Steering Committee Meeting 2024wk50

* Date: 2024wk50 2024-12-12
* Time: from 13:30 to 14:30 UTC
    * Oniro WG SC Members Meeting
    * Open to All Members
* Chair: Jaroslaw Marek 
* Minutes taker:  Juan Rico
* Join via Zoom
    * Link: https://eclipse.zoom.us/j/85049355252
     * Meeting ID: 85049355252

All times in UTC time.

Meeting starts at 13:35, as quorum is reached

# Agenda

- Approval of the MoM 2024wk47- all
- Oniro - OpenHarmony cooperation status
- Technical update
- Marketing Update
- Q1 2025 Objectives
- AOB

# Attendees

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

## Steering Committee

- Carlo Piana (Array representative)

## Eclipse Foundation 
- Juan Rico

## Committers
- Francesco Pham (Huawei)

## Other Members
- Luca Miotto (NOI)

## Regrets:
- Sharon Corbett  
- Gael Blondelle
- Thabang Mashologu

# Minutes

## Discussion

### Approval of the meeting minutes from the 2024wk47 2024-11-21 Oniro WG SC Meeting
- MoM approved at commit 217cb343bfdb766ef7324dab277730138d332d9b

### Status of the Cooperation with OpenHarmony
- After OCX we agreed on getting formally the information about the spec process in English. Now pushing for it. 
- It is not a blocker for Oniro, but it is definitely an asset that will boost the branding and compatibility program, specially for claiming OpenHarmony compatibility.
- Development phase trying to understand the building process, and working closely in the audiing part.

### Technical Update
- 10 Apps are already available, increasing application ecosystem and make them available on fOH
- Device side - trying to bring up the Volla phone, but need support with the drivers.
- CI for app verification and mirrors improvements
- Manifest updated to support OH5.0

### Marketing Update
- Marketing plan ongoing based on 4 pillars:
    - Communication activity and engage in bidirectional exchanges
    - Content creation strategy aligned with WG resources and topics of interests
    - Event participation, optimize the participation by setting objectives for the different events. (conferences, workshops, hackathons...)
    - Cooperation with OpenHarmony.

### Q1 2025 Objetives
- Postponed to the next SC.
### AOB
- Next meeting on January 9th 2025

## Resolutions
- MoM approved at commit 217cb343bfdb766ef7324dab277730138d332d9b

## Links and references

### Links

This is the list of relevant links:

Repository where you will find the agenda and meeting minutes to be approved (private repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve

Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc

Oniro WG Program Plan: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf

Oniro WG Budget: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf


This is the list to reference and other important notes to understand this document:

AoB: Any Other Business
MoM: Minutes of Meeting
WG: Working Group
SC: Steering Committee
EF: Eclipse Foundation
OH: Open Harmony


Meeting closes at 13:37 UTC 
