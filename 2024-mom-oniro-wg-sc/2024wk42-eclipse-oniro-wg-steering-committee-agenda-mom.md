# Oniro Working Group Steering Committee Meeting 2024wk42

* Date: 2024wk42 2024-10-17
* Time: from 13:00 to 14:00 UTC
    * Oniro WG SC Members Meeting
    * Open to All Members
* Chair: Jaroslaw Marek 
* Minutes taker:  Juan Rico
* Join via Zoom
    * Link: https://eclipse.zoom.us/j/85049355252
     * Meeting ID: 85049355252

All times in UTC time.

Meeting starts at 13:05, as quorum is reached

# Agenda

- Approval of the MoM 2024wk40- all
- Oniro 2025 Program Plan
- Preparation for OCX
- Next SC meeting
- AOB

# Attendees

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

## Steering Committee
- Adrian O'Sullivan (Huawei Alternate) 
- Carlo Piana (Array representative)
- Jaroslaw Marek (Huawei representative)

## Eclipse Foundation 
- Juan Rico

## Committers



## Other Members

- Luca Mioto (NOI)
- Francesco Pham (Huawei)

## Regrets:
- Sharon Corbett  
- Gael Blondelle
- Thabang Mashologu

# Minutes

## Discussion

### Approval of the meeting minutes from the 2024wk36 2024-10-3 Oniro WG SC Meeting
- MoM approved at commit 13bd7d70c2de0ad48c8d4225fd2ef45c606b8495

## Other items in the Agenda
### 2025 Program Plan
- The session covered the proposal of the Program plan, and there were updates on:
    - Participation of the SC in the pipeline review - Pillar 1
    - Figures for events participation
    - Figures for new memberships during 2025
    - Adding the organization of a hackathon

- Voted with the following result:
    - Jaroslaw Marek - Huawei representative - Approved
    - Carlo Piana - Array and Silver Member representative - Approved
    
- Candidate program plan approved version here - https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/program-plan-budget-templates/Oniro_Program_Plan_2025_v1.0.pdf

### Plan for OCX
- List of activities already shared and encourage everyone to engage with other Oniro members


## Next Meeting
Next meeting in 2 weeks - 17 October

## AOB
Francesco Pham was proposed as Committer representative taking the vacant seat left by Stefan Schmidt.

## Resolutions
- MoM 2024wk40 approved at commit 13bd7d70c2de0ad48c8d4225fd2ef45c606b8495
- Approve Candidate program plan - Approved version --> https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/program-plan-budget-templates/Oniro_Program_Plan_2025_v1.0.pdf

## Links and references

### Links

This is the list of relevant links:

Repository where you will find the agenda and meeting minutes to be approved (private repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve

Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc

Oniro WG Program Plan: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf

Oniro WG Budget: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf


This is the list to reference and other important notes to understand this document:

AoB: Any Other Business
MoM: Minutes of Meeting
WG: Working Group
SC: Steering Committee
EF: Eclipse Foundation
OH: Open Harmony


Meeting closes at 13:37 UTC 
