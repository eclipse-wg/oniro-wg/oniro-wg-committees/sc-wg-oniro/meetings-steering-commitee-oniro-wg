# Oniro Working Group Steering Committee Meeting 2024wk20

* Date: 2024wk20 2024-5-16
* Time: from 13:00 to 14:00 UTC
    * Oniro WG SC Members Meeting
    * Open to All Members
* Chair: Suhail Kahn 
* Minutes taker:  Juan Rico
* Join via Zoom
    * Link: https://eclipse.zoom.us/j/85049355252
     * Meeting ID: 85049355252

All times in UTC time.

Meeting starts at 13:05, as quorum is reached

# Agenda

- AApproval of the MoM 2024wk18- all
- Status of the Oniro - Open Harmony collaboration - 5 min - 
- Update of technical activities - 10 min - 
- Update of marketing activities - 10 min
- Huawei Developer Conference -10 min
- AOB

# Attendees

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

## Steering Committee

- Adrian O'Sullivan (Huawei Alternate) 
- Suhail Kahn (Huawei representative)

## Eclipse Foundation 

- Juan Rico

## Committers

- Stefan Schmidt (Huawei)

## Other Members

- Luca Miotto (NOI)
- Jaroslaw Marek (Huawei)

## Regrets:
- Carlo Piana (Array representative)
- Alberto Pianon (Array Alternate)  
- Sharon Corbett  
- Gael Blondelle
- Thabang Mashologu

# Minutes

## Discussion

### Approval of the meeting minutes from the 2024wk18 2024-5-2 Oniro WG SC Meeting

Approved at commit 132af129ef904cc2e60cf153aee24ad900798663

## Other items in the Agenda

### Update on the Open Harmony - Oniro collaboration
Expected meeting 1:1 during HDC

### Update of technical activities
- Stefan reporting the current status of the development activities
- Apps to be included in Oniro: 
    - Game 2048 adoption under development
    - Wikipedia
    - Targeting high profile app, checking feasibility and resources.
- Contact Futurewei to coordinate RUST developments.  
- Rebranding of the application store whose name needs to be defined.
- Volla phone progress ongoing, the image built is able to run on the phone, still some work to be developed to have everything working.
  
### Update of marketing activities
 - Jarek reports activities about events participated by Oniro - https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro/events-oniro/-/boards
 - Marketing assets currently under development:
    - Oniro Message architecture ongoing 
    - Oniro Pitch deck
    - Oniro Prospectus
    - App store naming
    - Volla press release
 - Reinforce the communication for Huawei Developer Conference

### Huawei Developer Conference
 - Check registration options if video streaming is possible.
 - Workshop organized on the 23rd of June.
 - 6 sessions covering the work in Oniro, success stories, the cooperation with Open Harmony and some of their results.

## AOB

None tabled

## Resolutions

- To approve the last minutes for 2024wk18 at 132af129ef904cc2e60cf153aee24ad900798663

## Links and references

### Links

This is the list of relevant links:

Repository where you will find the agenda and meeting minutes to be approved (private repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve

Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc

Oniro WG Program Plan: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf

Oniro WG Budget: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf


This is the list to reference and other important notes to understand this document:

AoB: Any Other Business
MoM: Minutes of Meeting
WG: Working Group
SC: Steering Committee
EF: Eclipse Foundation
OH: Open Harmony


Meeting closes at 13:50 UTC 
