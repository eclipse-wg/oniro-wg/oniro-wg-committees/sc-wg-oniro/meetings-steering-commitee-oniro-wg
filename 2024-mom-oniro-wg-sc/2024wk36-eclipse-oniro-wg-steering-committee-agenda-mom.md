# Oniro Working Group Steering Committee Meeting 2024wk36

* Date: 2024wk36 2024-9-5
* Time: from 13:00 to 14:00 UTC
    * Oniro WG SC Members Meeting
    * Open to All Members
* Chair: Suhail Kahn 
* Minutes taker:  Juan Rico
* Join via Zoom
    * Link: https://eclipse.zoom.us/j/85049355252
     * Meeting ID: 85049355252

All times in UTC time.

Meeting starts at 13:05, as quorum is reached

# Agenda

- Approval of the MoM 2024wk28- all
- Status of the Oniro - Open Harmony collaboration - 5 min - Juan
- Update of technical activities - 10 min - Jarek, Suhail
- Update of marketing activities - 10 min - Jarek
- Next SC meeting
- AOB

# Attendees

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

## Steering Committee
- Adrian O'Sullivan (Huawei Alternate) 
- Carlo Piana (Array representative)

## Eclipse Foundation 
- Juan Rico

## Committers

- Francesco Pham (Huawei)

## Other Members

- Luca Mioto (NOI)
- Jaroslaw Marek (Huawei)



## Regrets:

- Suhail Kahn (Huawei representative)
- Alberto Pianon (Array Alternate) 
- Sharon Corbett  
- Gael Blondelle
- Thabang Mashologu

# Minutes

## Discussion

### Approval of the meeting minutes from the 2024wk28 2024-7-13 Oniro WG SC Meeting
- MoM approved at commit a9362a2fb7d9275eb6bc48ba6428fa2f1b479aa1

## Other items in the Agenda
### Update on the OpenHarmony - Oniro collaboration
- Specification - Kubi as main contact to connect with the experts at OpenHarmnony side, we have already got:
    - Specification document
    - TCKs (3 applications, hardware and distributed services) - Apache 2.0
- Still pending the process, although they have described slightly one.

- BoF during OCX 
    - WIP; it is under preparation closing time, room and agenda

### Update of technical activities
- Webapps - Discord, Gmail, Duolingo and Youtube
- Development of the onboarding documentation for app development.
- Oniro working on a Raspberry Pi 4b - working pretty well similar and allowing an affordable development board. Working on the documentation.
- Oniro on the VollaPhone - still work in progress.

### Update of marketing activities
- Set of conferences 
    - OSS Linux Foundation Europe - Vienna 16-18 Sept. - Huawei Booth presence 
    - OCX - Mainz 22- 24 October - Booth, BoF, Talks
    - Huawei Connect Europe - November - Oniro/OpenHarmony Session
- Web update
    - Architecture
    - Testimonials
    - Some texts
    - Upcoming product pages

## Next Meeting
Next meeting in 4 weeks 

## AOB
- Some meetings held in the prevous weeks, very possitive feedback
- OCX registration



## Resolutions
- MoM 2024wk28 approved at commit a9362a2fb7d9275eb6bc48ba6428fa2f1b479aa1

## Links and references

### Links

This is the list of relevant links:

Repository where you will find the agenda and meeting minutes to be approved (private repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve

Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc

Oniro WG Program Plan: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf

Oniro WG Budget: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf


This is the list to reference and other important notes to understand this document:

AoB: Any Other Business
MoM: Minutes of Meeting
WG: Working Group
SC: Steering Committee
EF: Eclipse Foundation
OH: Open Harmony


Meeting closes at 13:37 UTC 
