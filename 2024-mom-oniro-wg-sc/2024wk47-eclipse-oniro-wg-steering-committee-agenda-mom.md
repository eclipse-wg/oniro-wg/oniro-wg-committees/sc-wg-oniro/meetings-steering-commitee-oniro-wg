# Oniro Working Group Steering Committee Meeting 2024wk47

* Date: 2024wk47 2024-11-21
* Time: from 13:30 to 14:30 UTC
    * Oniro WG SC Members Meeting
    * Open to All Members
* Chair: Jaroslaw Marek 
* Minutes taker:  Juan Rico
* Join via Zoom
    * Link: https://eclipse.zoom.us/j/85049355252
     * Meeting ID: 85049355252

All times in UTC time.

Meeting starts at 13:35, as quorum is reached

# Agenda

- Approval of the MoM 2024wk42- all
- Oniro 2025 Budget discussion
- AOB

# Attendees

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

## Steering Committee
- Adrian O'Sullivan (Huawei Alternate) 
- Carlo Piana (Array representative)
- Jaroslaw Marek (Huawei representative)

## Eclipse Foundation 
- Juan Rico

## Committers
- Francesco Pham (Huawei)

## Other Members
- Luca Mioto (NOI)

## Regrets:
- Sharon Corbett  
- Gael Blondelle
- Thabang Mashologu

# Minutes

## Discussion

### Approval of the meeting minutes from the 2024wk42 2024-10-17 Oniro WG SC Meeting
- MoM approved at commit 1a444f76b1dcee2381023c3ed0747054d2090eda

## Other items in the Agenda
### 2025 Budget discussion
- It was presented the differences between 2024 budget and 2025 estimations.
- EF confirmed the commitment of the Foundation to develop all the activities in the program plan, including the branding and compatibility program and its related activities.
- Starting at -48K€ looks challenging, but through the engagement with potential new members and the upgrade of supporting members it is possible to achieve this balance, and use a potential benefit in marketing and event support activities.
- After the explanation we proceed with the voting:
    - Jaroslaw Marek (Huawei representative) - approve
    - Carlo Piana (Array - Silver Member representative) - approve
    - Francesco Pham (Huawei - Committer representative) - approve
-The budget proposed is approved by the SC

## AOB
- The meeting continue reflecting on the experience after all the events organized in the last months, overall the feeling is extremely possitive and the outcomes of meeting OpenHarmony community are very good.
- 2025 key events
    - FOSSDEM - CfP closes on Dec 1st
    - IoT Day - Potentially a hackathon organized by Bosch
    - OpenHarmony technical conference Europe
- It was highlited the value of in person community meetnigs (Oniro and with OpenHarmony) and we will try to organize 3 during the year. Some of the WG members are willing to host them.


## Resolutions
- MoM approved at commit 1a444f76b1dcee2381023c3ed0747054d2090eda 
- RESOLVED, the Steering Committee approves the Eclipse Foundation budget as presented for 2025 acknowledging that it aligns with the available revenue in support of the Program Plan.

## Links and references

### Links

This is the list of relevant links:

Repository where you will find the agenda and meeting minutes to be approved (private repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve

Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc

Oniro WG Program Plan: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf

Oniro WG Budget: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf


This is the list to reference and other important notes to understand this document:

AoB: Any Other Business
MoM: Minutes of Meeting
WG: Working Group
SC: Steering Committee
EF: Eclipse Foundation
OH: Open Harmony


Meeting closes at 13:37 UTC 
