# Oniro Working Group Steering Committee Meeting 2024wk12

* Date: 2024wk12 2024-3-21
* Time: from 14:00 to 15:00 UTC
    * Oniro WG SC Members Meeting
    * Open to All Members
* Chair: Suhail Khan
* Minutes taker:  Juan Rico
* Join via Zoom
    * Link: https://eclipse.zoom.us/j/85049355252
     * Meeting ID: 8504935525

* Minutes approved at 3938950d

All times in UTC time.

Meeting starts at 14:06, as quorum is reached

# Agenda

- Approval of the meeting minutes of 2024wk10 SC meeting - All
- Update on the Open Harmony - Oniro collaboration - Juan
- Roadmap next Steps - Juan/Suhail
- AOB

# Attendees

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

## Steering Committee

- Suhail Khan (Huawei primary) 
- Alberto Pianon (Array Alternate)

## Eclipse Foundation 

- Juan Rico

## Committers

- Stefan Schmidt (Huawei)

## Other Members

- Jaroslaw Marek (Huawei)
- Patrick Ohnewein (NOI)

## Regrets:

- Carlo Piana (Array representative)
- Sharon Corbett  
- Gael Blondelle
- Thabang Mashologu
- Luca Miotto (NOI)

# Minutes

## Discussion

### Approval of the meeting minutes from the 2024-2-22 2024wk10 Oniro WG SC Meeting

- Minutes are approved at c2440b16a449c3466775c3ee9842ae4e05b9f4ab.

## Other items in the Agenda

### Update on the Open Harmony - Oniro collaboration
- The cooperation is moving forward but still there is no Specification received. Without an specification of Oniro, it is not possible to make any compatibility claim neither about products nor about OpenHarmony.
- This is not affecting the technical work Oniro is developing.

### Roadmap next Steps
- The first exercise was getting inputs and validate ideas presented in the workshop, additionally there are 1:1 meetings with those people who could not attend the meeting to get more info.
- After processing all the inputs a second workshop will be organized in the week of the 22nd of April to consolidate the view.
- Once the roadmap is closed there will be a webinar to share with the community Working Group plans.



## AOB
 


## Resolutions

- Minutes are approved at c2440b16a449c3466775c3ee9842ae4e05b9f4ab.

## Links and references

### Links

This is the list of relevant links:

Repository where you will find the agenda and meeting minutes to be approved (private repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve

Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc

Oniro WG Program Plan: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf

Oniro WG Budget: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf


This is the list to reference and other important notes to understand this document:

AoB: Any Other Business
MoM: Minutes of Meeting
WG: Working Group
SC: Steering Committee
EF: Eclipse Foundation
OH: Open Harmony


Meeting closes at 14:33 UTC 
