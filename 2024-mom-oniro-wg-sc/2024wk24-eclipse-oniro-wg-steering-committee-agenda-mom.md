# Oniro Working Group Steering Committee Meeting 2024wk24

* Date: 2024wk20 2024-6-13
* Time: from 13:00 to 14:00 UTC
    * Oniro WG SC Members Meeting
    * Open to All Members
* Chair: Suhail Kahn 
* Minutes taker:  Juan Rico
* Join via Zoom
    * Link: https://eclipse.zoom.us/j/85049355252
     * Meeting ID: 85049355252

All times in UTC time.

Meeting starts at 13:05, as quorum is reached

# Agenda

- AApproval of the MoM 2024wk20- all
- Status of the Oniro - Open Harmony collaboration - 5 min - 
- Update of technical activities - 10 min - 
- Update of marketing activities - 10 min
- Next SC meeting
- AOB

# Attendees

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

## Steering Committee

- Suhail Kahn (Huawei representative)
- Carlo Piana (Array representative)

## Eclipse Foundation 

- Juan Rico

## Committers

- Stefan Schmidt (Huawei)

## Other Members

- Partick Ohnewein (NOI)


## Regrets:
- Adrian O'Sullivan (Huawei Alternate) 
- Alberto Pianon (Array Alternate)  
- Sharon Corbett  
- Gael Blondelle
- Thabang Mashologu

# Minutes

## Discussion

### Approval of the meeting minutes from the 2024wk20 2024-5-16 Oniro WG SC Meeting

Approved at commit cf2f903eda7edd2adacf10785051ba56415a14c1

## Other items in the Agenda

### Update on the Open Harmony - Oniro collaboration
- Governance charter from OH has been received. It is a good document but still there are some aspects to be clarified:
    - Specification Process is not referred, there is only to the Product Compatibility Specification, but that is not enough for us.
    - IP protection, not enough information about how IP is protected specially in what concerns to patents.
- During the HDC EF and OA will have a meeting to go deeper into this two topics as well as the promotion of both WG to cross-cooperate in involving partners in the other initiative.

### Update of technical activities
- Stefan reporting the current status of the development activities
    - Progressing in the first PoC
    - Volla phone able to boot, now working on moving forward for loading GUI and apps
    - Checking how Signal can be included in the Oniro Phone 
  
### Update of marketing activities
 - This week Oniro is going to be represented in 3 events:
    - OW2 with a booth and an Oniro talk by Suhail
    - DES participating in a panel to showcase Oniro as a solution for mobile and into
    - Volla community days presenting the achievements and the plans for Oniro to engage deeper with the Volla community.

 - For next week we have the workshop in China:
    - Speakers have been confirmed on Oniro side.
    - Agenda is closed and OH is working on recruiting local attendees.
    - Good opportunity to promote Oniro brand and the opportunities we are creating in Western Countries.

## Next Meetingç
 - It is proposed to go back to the monthly meetings and it is unanimously approved
## AOB

None tabled

## Resolutions

- To approve the last minutes for 2024wk20 at cf2f903eda7edd2adacf10785051ba56415a14c1
- To go back to monthly meetings in the WG

## Links and references

### Links

This is the list of relevant links:

Repository where you will find the agenda and meeting minutes to be approved (private repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve

Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc

Oniro WG Program Plan: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf

Oniro WG Budget: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf


This is the list to reference and other important notes to understand this document:

AoB: Any Other Business
MoM: Minutes of Meeting
WG: Working Group
SC: Steering Committee
EF: Eclipse Foundation
OH: Open Harmony


Meeting closes at 13:32 UTC 
