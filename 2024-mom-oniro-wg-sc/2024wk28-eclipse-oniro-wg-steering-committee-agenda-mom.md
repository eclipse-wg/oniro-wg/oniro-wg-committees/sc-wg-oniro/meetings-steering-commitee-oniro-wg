# Oniro Working Group Steering Committee Meeting 2024wk28

* Date: 2024wk28 2024-7-13
* Time: from 13:00 to 14:00 UTC
    * Oniro WG SC Members Meeting
    * Open to All Members
* Chair: Suhail Kahn 
* Minutes taker:  Juan Rico
* Join via Zoom
    * Link: https://eclipse.zoom.us/j/85049355252
     * Meeting ID: 85049355252

All times in UTC time.

Meeting starts at 13:05, as quorum is reached

# Agenda

- AApproval of the MoM 2024wk24- all
- Status of the Oniro - Open Harmony collaboration - 5 min - Juan
- Update of technical activities - 10 min - Stefan & Francesco
- Update of marketing activities - 10 min - Jarek
- Think Global Code Local workshop outcomes -10 min - Suhail & Juan
- Next SC meeting
- AOB

# Attendees

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

## Steering Committee

- Suhail Kahn (Huawei representative)
- Carlo Piana (Array representative)

## Eclipse Foundation 

- Juan Rico

## Committers

- Stefan Schmidt (Huawei)

## Other Members

- Partick Ohnewein (NOI)
- Jaroslaw Marek (Huawei)
- Francesco Pham (Huawei)


## Regrets:
- Adrian O'Sullivan (Huawei Alternate) 
- Alberto Pianon (Array Alternate)  
- Sharon Corbett  
- Gael Blondelle
- Thabang Mashologu

# Minutes

## Discussion

### Approval of the meeting minutes from the 2024wk20 2024-6-13 Oniro WG SC Meeting

Minutes approved at commit 85e389d79b74f416b13908e29f9984dee8f28d35

## Other items in the Agenda
### Update on the Open Harmony - Oniro collaboration
- Specification - Product Compatibility Spaecification is the ref document, it includes both product Specification and Software distribution specifications
- Still pending to define the specification process.

### Update of technical activities
- App development shifting from Wikipedia to Signal.
    - Signal based on Rust so try to follow the same approach to the one used with Servo.
- Mirror API to get the repos is not working and now it is a pretty manual time consuming activity.
- Developer phone work is progressing well with phone services already running.
### Update of marketing activities
- We target a refresh of resource section in the website
- Provide materials presented in the events
- We will track event participation in the gitlab Oniro events board

### Think Global Code Local outcomes
- Raise awareness and agree on the track
- Very possitive feelings and opportunities identified
- Aiming at replicate the gathering at OCX

## Next Meeting
Next meeting in 4 weeks 

## AOB
We all thank Stefan Schmidt his hard work and commitment and welcome Francesco Pham to his new role as committers' representative.

## Resolutions
Approve minutes of the previous meeting at commit 85e389d79b74f416b13908e29f9984dee8f28d35

## Links and references

### Links

This is the list of relevant links:

Repository where you will find the agenda and meeting minutes to be approved (private repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve

Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc

Oniro WG Program Plan: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf

Oniro WG Budget: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf


This is the list to reference and other important notes to understand this document:

AoB: Any Other Business
MoM: Minutes of Meeting
WG: Working Group
SC: Steering Committee
EF: Eclipse Foundation
OH: Open Harmony


Meeting closes at 13:37 UTC 
