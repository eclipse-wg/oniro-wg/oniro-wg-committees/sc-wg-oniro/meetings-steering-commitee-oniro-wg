# Oniro Working Group Steering Committee Meeting 2024wk2

* Date: 2024wk4 2024-1-25
* Time: from 14:00 to 15:00 UTC
    * Oniro WG SC Members Meeting
    * Open to All Members
* Chair: Suhail Khan
* Minutes taker:  Juan Rico
* Join via Zoom
    * Link: https://eclipse.zoom.us/j/85049355252
     * Meeting ID: 85049355252

Approved on 2024-02-08 at 037a3d38

All times in UTC time.

Meeting is started at 14:02, quorum is reached

# Agenda

- Approval of the meeting minutes from the 2023-12-14 2023wk50 Oniro WG SC Meeting (All) 5 minutes
- Oniro WG Charter vote - 10 minutes
- EF - OA agreement implementation status - 15 min
- Oniro Roadmap - 15 min
- AOB

# Attendees

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

## Steering Committee

- Suhail Khan (Huawei primary) 
- Adrian O'Sullivan (Huawei alternate)
- Alberto Pianon (Array Alternate)

## Eclipse Foundation 

- Juan Rico


## Committers

- Stefan Schmidt (Huawei)

## Other Members

- Luca Miotto (NOI)
- Patrick Ohnewein (NOI)

## Regrets:

- Sharon Corbett  
- Gael Blondelle
- Thabang Mashologu

# Minutes

## Discussion

### Approval of the meeting minutes from the 2024-1-11 2024wk2 Oniro WG SC Meeting
- MoM were approved at commit 1239f0c583e87965ad0cc07512eec8e889114bf1

## Other items in the Agenda

## Oniro WG Charter update vote
Huawei as Strategic Member +1
Array, representative of Silver members  +1
Committers representative +1 

## EF - OA status of the implementation
- IP due diligence
    - IF OA finds well matrix as communication channel - EF to create a Matrix Channel for IP Compliance 
    - Wenesday 31st meeting with OA - Suhail to confirm EF to send the invite.

- FOSDEM
    - Set up a meeting with OA legal team

## Oniro Roadmap
- One representative per WG member interested to participate in roadmap creation
- Target date for workshop February

## Resolutions
- Minutes of the previous meeting approved at commit 1239f0c583e87965ad0cc07512eec8e889114bf1
- WG Charter modifications unanimously approved.

## AOB

No other business

## Links and references

### Links

This is the list of relevant links:

Repository where you will find the agenda and meeting minutes to be approved (private repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve

Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc

Oniro WG Program Plan: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf

Oniro WG Budget: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf


This is the list to reference and other important notes to understand this document:

AoB: Any Other Business
MoM: Minutes of Meeting
WG: Working Group
SC: Steering Committee
EF: Eclipse Foundation
OH: Open Harmony
OA: Open Atom Foundation


Meeting closes at 14:27 UTC 
