# Oniro Working Group Steering Committee Meeting 2024wk40

* Date: 2024wk40 2024-10-3
* Time: from 13:00 to 14:00 UTC
    * Oniro WG SC Members Meeting
    * Open to All Members
* Chair: Suhail Kahn 
* Minutes taker:  Juan Rico
* Join via Zoom
    * Link: https://eclipse.zoom.us/j/85049355252
     * Meeting ID: 85049355252

All times in UTC time.

Meeting starts at 13:05, as quorum is reached

# Agenda

- Approval of the MoM 2024wk36- all
- Status of the Oniro - Open Harmony collaboration - 5 min - Juan
- Update of technical activities - 10 min - Francesco
- Update of marketing activities - 10 min - Jarek
- Oniro 2025 Program Plan
- Next SC meeting
- AOB

# Attendees

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

## Steering Committee
- Adrian O'Sullivan (Huawei Alternate) 
- Carlo Piana (Array representative)
- Suhail Kahn (Huawei representative)
- Alberto Pianon (Array Alternate)

## Eclipse Foundation 
- Juan Rico

## Committers



## Other Members

- Francesco Pham (Huawei)
- Luca Mioto (NOI)
- Jaroslaw Marek (Huawei)

## Regrets:
- Sharon Corbett  
- Gael Blondelle
- Thabang Mashologu

# Minutes

## Discussion

### Approval of the meeting minutes from the 2024wk36 2024-9-5 Oniro WG SC Meeting
- MoM approved at commit 813a1ecb6908fe206733a5999de2c1ea0c5c055f

## Other items in the Agenda
### Update on the OpenHarmony - Oniro collaboration
- Specification - good progress still need to get the process writing and delivered from OpenHarmony
- Meeting during OCX 
    - Agenda topics included (Specification, recruitment of members,marketing and events and IP toolchain)

### Update of technical activities
- Demos for OSS: 
    - Volla phone running Oniro - Working with Volla to finalize the full implementation
    - Raspberry Pi running Oniro - Based on OpenHarmony 4.1, updated assets to support developers and adopters

### Update of marketing activities
- Set of conferences 
    - OpenHarmony Technical Conference - Mike Milinkovich giving a Keynote about Oniro
    - OCX - Mainz 22- 24 October - Keynote by Chen Haibo, Huawei booth with an Oniro space, BoF Think Global Code Local on Wednesday afternoon, Talks (3) - Link to the Agenda
    - Presentation of Oniro at UPV (7th Nov) as part of the Hackathon
    - SFS Con - 8-9 Nov Bolzano (Italy)-  Presentations from Array and Eclipse Foundation  
    - Huawei Connect Europe 14-15 Nov Paris- Oniro/OpenHarmony Session
Web updated in several sections.
    
### 2025 Program Plan
- https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/program-plan-budget-templates/Oniro_Program_Plan_2025_v0.1.pdf
- Calendar of Actions:
    - Feedback by Friday 11th Oct. - All SC Members
    - Consolidated version -14th Oct - Juan 
    - Voting on Final Version of the candidate program plan - 17th Oct.

## Next Meeting
Next meeting in 2 weeks - 17 October

## AOB
- Jaroslaw Marek taking Oniro SC Chair and Huawei representative roles as Suhail Khan is leaving.
- The whole WG thank Suhail for his work and dedication.

## Resolutions
- MoM 2024wk36 approved at commit 813a1ecb6908fe206733a5999de2c1ea0c5c055f

## Links and references

### Links

This is the list of relevant links:

Repository where you will find the agenda and meeting minutes to be approved (private repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve

Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc

Oniro WG Program Plan: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf

Oniro WG Budget: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf


This is the list to reference and other important notes to understand this document:

AoB: Any Other Business
MoM: Minutes of Meeting
WG: Working Group
SC: Steering Committee
EF: Eclipse Foundation
OH: Open Harmony


Meeting closes at 13:37 UTC 
