# Oniro Working Group Steering Committee Meeting 2024wk10

* Date: 2024wk10 2024-3-7
* Time: from 14:00 to 15:00 UTC
    * Oniro WG SC Members Meeting
    * Open to All Members
* Chair: Suhail Khan
* Minutes taker:  Carlo Piana
* Join via Zoom
    * Link: https://eclipse.zoom.us/j/85049355252
     * Meeting ID: 85049355252

All times in UTC time.

Meeting starts at 14:04, as quorum is reached

# Agenda

- Approval of the meeting minutes of 2024wk8 SC meeting - All
- Update on the Open Harmony - Oniro collaboration - Juan
- Update on Oniro members - Juan
- Roadmap next Steps - Juan/Suhail
- Communication document organization - Juan
- AOB

# Attendees

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

## Steering Committee

- Suhail Khan (Huawei primary) 
- Alberto Pianon (Array Alternate)
- Carlo Piana (Array) 
- Adrian O'Sullivan (Huawei alternate)

## Eclipse Foundation 

- Juan Rico

## Committers



## Other Members

- Luca Miotto (NOI)
- Patrick Ohnewein (NOI)

## Regrets:

- Stefan Schmidt (Huawei)
- Sharon Corbett  
- Gael Blondelle
- Thabang Mashologu

# Minutes

## Discussion

### Approval of the meeting minutes from the 2024-2-22 2024wk8 Oniro WG SC Meeting

Minutes are approved at 65f60678 with the change of minute taker.

## Other items in the Agenda

### Update on the Open Harmony - Oniro collaboration

Discussion on how to document progress (kanban board? Google docs?). Probably we will use Gitlab and Gitee with Juan bridging the update.

### Update on Oniro members

Typefox should become a member of the WG as Silver Members.

Juan reports on prospects. More will be contacted shortly.

### Roadmap next Steps

- We are probably going to have a workshop next week.
- We are finalizing the timeline
- There is vertical in process

### Oniro Communication document organization

Juan invites members to familiarize and provide input to the document.

## AOB
 
- Planning of activities for SFSCON. If it is agreed the organization a dev day, it is needed to have enough time for planning.


## Resolutions



## Links and references

### Links

This is the list of relevant links:

Repository where you will find the agenda and meeting minutes to be approved (private repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve

Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc

Oniro WG Program Plan: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf

Oniro WG Budget: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf


This is the list to reference and other important notes to understand this document:

AoB: Any Other Business
MoM: Minutes of Meeting
WG: Working Group
SC: Steering Committee
EF: Eclipse Foundation
OH: Open Harmony


Meeting closes at 14:33 UTC 
