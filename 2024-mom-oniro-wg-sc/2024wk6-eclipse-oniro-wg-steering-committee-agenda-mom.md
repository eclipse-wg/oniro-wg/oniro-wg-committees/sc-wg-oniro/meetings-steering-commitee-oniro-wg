# Oniro Working Group Steering Committee Meeting 2024wk6

* Date: 2024wk6 2024-2-8
* Time: from 14:00 to 15:00 UTC
    * Oniro WG SC Members Meeting
    * Open to All Members
* Chair: Suhail Khan
* Minutes taker:  Carlo Piana
* Join via Zoom
    * Link: https://eclipse.zoom.us/j/85049355252
     * Meeting ID: 85049355252

All times in UTC time.

Meeting starts at 14:06, as quorum is reached

# Agenda

- Approval of the meeting minutes from the 2024-01-25 2024wk4 Oniro WG SC Meeting (All) 5 minutes
- Open Atom Foundation - Eclipse Foundation Agreement - 15 minutes
- Oniro Roadmap for 2024 - 15 minutes
- AOB

# Attendees

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

## Steering Committee

- Suhail Khan (Huawei primary) 
- Adrian O'Sullivan (Huawei alternate)
- Carlo Piana (Array)

## Eclipse Foundation 

- Juan Rico

## Committers

- Stefan Schmidt (Huawei)

## Other Members

- Luca Miotto (NOI)

## Regrets:

- Patrick Ohnewein (NOI)
- Alberto Pianon (Array Alternate)
- Sharon Corbett  
- Gael Blondelle
- Thabang Mashologu

# Minutes

## Discussion

### Approval of the meeting minutes from the 2024-1-25 2024wk4 Oniro WG SC Meeting

- MoM were approved at commit 037a3d38

## Other items in the Agenda

### Open Atom Foundation - Eclipse Foundation Agreement

Rico reports of the meeting with Atom Foundation during  FOSDEM,in particular, the specification building and approval process.  Things are progressing relatively well. 

### Oniro Roadmap for 2024

We need to trasform the Action Plan into an actual Roadmap, consulting the WG members. Suhail calls for a meeting where to provide input (with prior offline contributions). Juan will provide a template.
 

## AOB

No other business


## Resolutions

- to approve the minutes for 2024-1-25 wk4 meeting at 037a3d38.

## Links and references

### Links

This is the list of relevant links:

Repository where you will find the agenda and meeting minutes to be approved (private repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve

Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc

Oniro WG Program Plan: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf

Oniro WG Budget: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf


This is the list to reference and other important notes to understand this document:

AoB: Any Other Business
MoM: Minutes of Meeting
WG: Working Group
SC: Steering Committee
EF: Eclipse Foundation
OH: Open Harmony


Meeting closes at 14:25 UTC 
