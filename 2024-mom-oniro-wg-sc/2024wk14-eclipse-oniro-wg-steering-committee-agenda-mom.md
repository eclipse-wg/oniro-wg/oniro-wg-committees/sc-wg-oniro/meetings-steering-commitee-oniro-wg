# Oniro Working Group Steering Committee Meeting 2024wk14

* Date: 2024wk14 2024-4-4
* Time: from 13:00 to 14:00 UTC
    * Oniro WG SC Members Meeting
    * Open to All Members
* Chair: Adrian O'Sullivan 
* Minutes taker:  Carlo Piana
* Join via Zoom
    * Link: https://eclipse.zoom.us/j/85049355252
     * Meeting ID: 85049355252

Approved at  6277c5b6 

All times in UTC time.

Meeting starts at 14:06, as quorum is reached

# Agenda

- Approval of the meeting minutes of 2024wk12 SC meeting - All
- Update on the Open Harmony - Oniro collaboration - Juan
- Update of technical activities - 10 min - Stefan
- Update of marketing activities - 10 min - Jarek
- Reminder about WG elections - 5 min - Juan
- AOB

# Attendees

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

## Steering Committee

- Adrian O'Sullivan (Huawei Alternate) 
- Carlo Piana (Array representative)
- Alberto Pianon (Array Alternate) joins at 13:12

## Eclipse Foundation 

- Juan Rico

## Committers

- Stefan Schmidt (Huawei)

## Other Members

- Jaroslaw Marek (Huawei) joins at 13:12g
- Patrick Ohnewein (NOI)
- Luca Miotto (NOI)

## Regrets:

- Suhail Kahn (Huawei representative)
- Sharon Corbett  
- Gael Blondelle
- Thabang Mashologu

# Minutes

## Discussion

### Approval of the meeting minutes from the 2024wk12 2024-3-21 Oniro WG SC Meeting

Approved at 3938950d

## Other items in the Agenda

### Update on the Open Harmony - Oniro collaboration

Juan reports

### Update of technical activities

Stefan reports on progress. Especially for the toolchain, kernel setup, third party software. Getting OH application running, as well as eyeing certain OS Android (targeting for porting). We are on track. 

### Update of marketing activities

Jarek reports. Activities. Relationship with the marketing agency (Vola) we are going to hire for launch. Other activities.

### Oniro WG Elections

Elections are happening in April, tomorrow or next week will be announced.

## AOB
 
Carlo reports that most members will be engaged otherwise. Next meeting is cancelled.

## Resolutions

To approve previous meeting's minutes (2024wk12 2024-3-21)


## Links and references

### Links

This is the list of relevant links:

Repository where you will find the agenda and meeting minutes to be approved (private repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve

Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc

Oniro WG Program Plan: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf

Oniro WG Budget: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf


This is the list to reference and other important notes to understand this document:

AoB: Any Other Business
MoM: Minutes of Meeting
WG: Working Group
SC: Steering Committee
EF: Eclipse Foundation
OH: Open Harmony


Meeting closes at 13:44 UTC 
