# Oniro Working Group Steering Committee Meeting 2024wk2

* Date: 2024wk2 2024-1-11
* Time: from 14:00 to 15:00 UTC
    * Oniro WG SC Members Meeting
    * Open to All Members
* Chair: Suhail Khan
* Minutes taker:  Juan Rico
* Join via Zoom
    * Link: https://eclipse.zoom.us/j/85049355252
     * Meeting ID: 85049355252

All times in UTC time.

Meeting is started at 14:04, quorum is reached

# Agenda

- Approval of the meeting minutes from the 2023-12-14 2023wk50 Oniro WG SC Meeting (All) 5 minutes
- Oniro WG Charter updates - 25 minutes
- Marketing activities for 2024 - Actions requested - Juan 10 minutes
- Open Code Experience - https://opencode-x.org/ - Juan 5 minutes
- AOB - 5 minutes

# Attendees

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

## Steering Committee

- Suhail Khan (Huawei primary) 
- Adrian O'Sullivan (Huawei alternate)
- Alberto Pianon (Array Alternate)

## Eclipse Foundation 

- Juan Rico


## Committers

- Stefan Schmidt (Huawei)

## Other Members

- Luca Miotto (NOI)
- Patrick Ohnewein (NOI)

## Regrets:

- Sharon Corbett  
- Gael Blondelle
- Thabang Mashologu

# Minutes

## Discussion

### Approval of the meeting minutes from the 2023-12-14 2023wk50 Oniro WG SC Meeting
- MoM were approved at commit cd674e2ffec19e56fe016d8ed074cb0d28f3b92d

## Other items in the Agenda

## Oniro WG Charter updates
- It was presented the changes proposed to the current version of Oniro WG Charter and the reasoning behind them - Agreement with the Open Atom Foundation and update of Eclipse Foundation Process
- The slides used for presenting the changes, the redline version and the final version of the charter will be shared to vote for its approval.
- It is agreed to target next Oniro SC in 2024 week4, but in case anyone need more time to go through it, we can postpone maximum 2 weeks (SC meeting wk 6)

## Oniro Marketing activities
- In the program plan for 2024 marketing has a strong presence, and although the main activities are driving in the Marketing Committee, it will be reported the status to the SC since the involvement of the members is key to achieve the objetives. Three aspects were commented during the meeting:
    - Social Media Plan - Marketing Committee shared a calendar for promoting WG activities and the members 
    - Web updates - It has been defined an updated process to have more freedom in the WG to perform web updates, however it is needd to identify the person who will be in charge of implementing the changes
    - Marketing assets - During 2024 a set of marketing assets (i.e. prospectus) will be delivered.
- Common for the three is the need of the WG creating the content and then rely on the EF services to give it the final formatting.

## Open Code Experience
- It was presented Open Code Experience taking place in Mainz in Oct 2024.
- It is requested to the WG to start thinking on potential actions that can be taken, the event is getting bigger and collaborations will happen across several WG.

## Resolutions
- Approved minutes for Approved MoM from the 2023-12-14 2023wk50 Oniro WG SC Meeting at commit cd674e2ffec19e56fe016d8ed074cb0d28f3b92d

## AOB

No other business

## Links and references

### Links

This is the list of relevant links:

Repository where you will find the agenda and meeting minutes to be approved (private repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve

Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc

Oniro WG Program Plan: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf

Oniro WG Budget: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf


This is the list to reference and other important notes to understand this document:

AoB: Any Other Business
MoM: Minutes of Meeting
WG: Working Group
SC: Steering Committee
EF: Eclipse Foundation
OH: Open Harmony


Meeting closes at 14:58 UTC 
