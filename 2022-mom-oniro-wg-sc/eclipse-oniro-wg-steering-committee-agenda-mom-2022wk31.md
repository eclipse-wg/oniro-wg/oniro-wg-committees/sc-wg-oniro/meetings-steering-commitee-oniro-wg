# Oniro Working Group Steering Committee Meeting 2022wk31

MoM Status: approved on 2022-08-18 commit d8e734545779d72e6c112b03b15ce94d3eb772d2

* Date: 2022-08-04
* Time: from 15:02 to 16:04 CEST
   * Oniro WG SC meeting: from 15:01 to 15:31 CEST
   * Oniro WG SC All Members meeting: from 15:31 to 16:00 CEST
* Chair: Adrian O.S. 
* Minutes taker: Carlo Piana
* Join via Zoom
   * #link <https://eclipse.zoom.us/j/86710449041?pwd=dmk1OW56emQvU1YvSjBUY1AyOHIzUT09>
   * Meeting ID: 867 1044 9041
   * Passcode: 293617

## Agenda

Oniro WG SC meeting

* Approval of the meeting minutes from the 2022-07-21 Oniro WG SC Meeting - Davide R. 5 minutes
* Oniro 2022Q2 report. Analysis.
   * State of the repport - Agustin B. 5 min
   * Discussion - 10 min,
* Resolutions - 5 minutes 
* AoB (Open Floor) - 5 minutes

Oniro WG SC All Members meeting

* Oniro 2022Q2 report. Analysis.
   * State of the repport - Agustin B. 5 min
   * Discussion - 20 min,
* AoB - 5 minutes

## Attendees

### Oniro WG SC Meeting

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

* Davide Ricci - Huawei)
* Adrian O'Sullivan - Huawei alternate)
* Carlo Piana - Array. Silver Members representative)
* Alberto Pianon - Array alternate)
* Amit Kucheria - Committers)


Steering Committee Regrets:


Other Attendees:


Eclipse Foundation:

* Agustin Benito Bethencourt
* Paul Buck (until 15:30)


### Oniro WG SC All Members Meeting

Beyond the above, the participants in this section were:

[comment]: # (* Bill Fletcher - Linaro)
* Luca Tavanti - Cubit (form 15:32)
* Andrea Basso - Synesthesia (from 14:45?)
[comment]: # (* Luca Miotto - NOI)

## Minutes

### Oniro WG SC meeting minutes

#### Approval of the meeting minutes from the 2022-07-21 Oniro WG SC Meeting

* Agenda and meeting minutes to be approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/agenda-minutes-to-approve/eclipse-oniro-wg-steering-committee-agenda-mom-2022wk29.md>

Commit considered: 2dd0572e

No objections recorded, minutes are approved



#### Oniro 2022Q2 report. Analysis.

These are the links where you can get the Oniro Program Plan 2022Q2 report:
* Oniro Program Plan 2022Q2 Report draft in .pdf version #link: <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro_Program_Plan_report_2022Q2.pdf>
* Oniro Program PLan 2022Q2 Report draft collaboration version #link: <https://docs.google.com/presentation/d/1R-oBfH7lxmFvXa0GH_Yw35UYCzeRSZpV6jBKG9ygADg/edit?usp=sharing>

Please add your feedback in the collaboration version. You can also send your feedback to Agustin so he adds it.

The SC goes through the current situation of the Program Objectives. Where we are lacking and need more effort. Onboarding of new members. Discussing about how to raise commitment by each Member Company in this respect. 

- Oniro awareness. Value proposition must be communicated and we should have all the necessary tools available. Also participation to next events and which to focus.

- Program Strategies and tactics

   - Compatibility, we need to focus on opening lines of communication with OH to start the   compatibility program
   - better integration within eclipse (pipeline release, IT), Oniro used within EF projects. Cross pollination. Make a list of projects we would like to interact with.

A slightly curated for typos and shorthand excerpt of the chat discussion follows:

|              |                                                                                                          |
| ------------ | -------------------------------------------------------------------------------------------------------- |
| Davide Ricci | good progress with pipeline release and integration inside eclipse                                       |
| Davide Ricci | to be improved                                                                                           |
| Davide Ricci | leveraging eclipse resources                                                                             |
| Davide Ricci | against program milestones                                                                               |
| Davide Ricci | i.e IT                                                                                                   |
| Davide Ricci | infrastructure                                                                                           |
| Davide Ricci | and to tie all things together form requirements to pipeline to release                                  |
| Davide Ricci | then clear ownerships                                                                                    |
| Davide Ricci | (this are all normal improvements)                                                                       |
| Davide Ricci | re pipeline - I think there are obvious synergies between working groups that eclipse can facilitate     |
| Davide Ricci | i.e SDV / Kanto etc                                                                                      |
| Davide Ricci | Eclipse has to take a more active role to promote cross memberships and cross ties                       |
| Agustin - EF | summary of Adrian comment: oniro used within EF projects                                                 |
| Agustin - EF | cross-pollination with SDV                                                                               |
| Agustin - EF | andrea basso: contributions of other projects into oniro. He supports the cross-pollination project      |
| Agustin - EF | EF PgMs spot opportunities for Oniro to be used within their environment                                 |
| Davide Ricci | I would suggest the EF to track not only membership growth but also stickiness of projects by creating   |
| Davide Ricci | strong ties between companies and working groups                                                         |
| Davide Ricci | this in turn reduces churn of members in the long run                                                    |
| Davide Ricci | (along the same line of cross pollination etc)                                                           |
| Davide Ricci | as an example Linaro does and excellent job Andrea can certainly tell us and Eclipse more                |
| Agustin - EF | create a list of projects we would like to interact with                                                 |
| Agustin - EF | create a list of projects we would like to interact with                                                 |
| Davide Ricci | agreed                                                                                                   |
| Davide Ricci | this helps marshalling and focusing support from EF management                                            |
| Davide Ricci | we are going to have to address our weakness around product management                                   |
| Davide Ricci | I have a volunteer … product management should drive such analysis and prioritize the list of activities |
| Davide Ricci | to achieve business targets                                                                              | 
| Davide Ricci | this is my point about tie all together                                                                  |                                                                                  |

#### Resolutions

- minutes for the last meeting are approved


#### AoB  

None

### Oniro WG SC All Members Meeting minutes

#### Oniro 2022Q2 report. Analysis.

These are the links where you can get the Oniro Program PLan 2022Q2 report:
* Oniro Program PLan 2022Q2 Report draft in .pdf version #link: <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro_Program_Plan_report_2022Q2.pdf>
* Oniro Program PLan 2022Q2 Report draft collaboration version #link: <https://docs.google.com/presentation/d/1R-oBfH7lxmFvXa0GH_Yw35UYCzeRSZpV6jBKG9ygADg/edit?usp=sharing>

Please add your feedback in the collaboration version. You can also send your feedback to Agustin so he adds it.

- Discussion started during private and extended through the entire open session


#### AoB  (Open Floor)



## Links and references

### Links

This is the list of relevant links:
* Repository where you will find the agenda and meeting minutes to be approved (private repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve>
* Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc>
* Oniro WG Program Plan: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro_Program_Plan_2022.pdf>
* Oniro WG Budget: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro-2022-Working-Group-Budget-Summary.pdf>

This is the list to reference and other important notes to understand this document:
* AoB: Any Other Business
* MoM: Minutes of Meeting
* WG: Working Group
* SC: Steering Committee
* EF: Eclipse Foundation
* OH: Open Harmony
