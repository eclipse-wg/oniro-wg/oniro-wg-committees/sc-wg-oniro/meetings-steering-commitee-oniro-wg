# Oniro Working Group Steering Committee Launch Meeting 2022wk27

MoM Status: approved on 2022-07-21 commit 5b33591e646b3dd75c435392f582ca49f9cd9a5d

* Date: 2022-07-07
* Time: from 15:02 to 16:04 CEST
   * Oniro WG SC meeting: from 15:01 to 15:31 CEST
   * Oniro WG SC All Members meeting: from 15:31 to 16:00 CEST
* Chair: Amit 
* Minutes taker: Carlo Piana
* Join via Zoom
   * #link <https://eclipse.zoom.us/j/86710449041?pwd=dmk1OW56emQvU1YvSjBUY1AyOHIzUT09>
   * Meeting ID: 867 1044 9041
   * Passcode: 293617

## Agenda

Oniro WG SC meeting

* Approval of the meeting minutes from the 2002-05-26  Oniro WG SC Meeting - Amit Kucheria 5 minutes
* Approval of the Roadmap changes - Amit K. 15 min 
* Resolutions - 5 minutes 
* AoB (Open Floor) - 5 minutes

Oniro WG SC All Members meeting

* Cubit Innovation Labs presentation - Luca T. 10 minutes
* Program Plan status - Agustin B. 10 minutes
* Topics for the Oniro WG Sepcification Projects - Agustin B. 5 minutes
* AoB - 5 minutes

## Attendees

### Oniro WG SC Meeting

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

[comment]: # (* Davide Ricci - Huawei)
* Carlo Piana - Array
* Amit Kucheria - Committers


Steering Committee Regrets:


Other Attendees:

[comment]: # (* Adrian O'Sullivan - Huawei alternate) 
* Alberto Pianon - Array alternate

Eclipse Foundation:

* Agustin Benito Bethencourt)
* Paul Buck (excuses himeslf @ 15:31)

[comment]: # (* Sharon Corbett) 


### Oniro WG SC All Members Meeting

Beyond the above, the participants in this section were:

[comment]: # (* Andrea Basso - Synesthesia)
[comment]: # (* Gianluca Venere - SECO)
[comment]: # (* Andrea Gallo - Linaro)
Luca Tavanti - Cubit
Luca Miotto - NOI


## Minutes

### Oniro WG SC meeting minutes

#### Approval of the meeting minutes from the 2022-05-26 Oniro WG SC Meeting

* Agenda and meeting minutes to be approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/agenda-minutes-to-approve/eclipse-oniro-wg-steering-committee-agenda-mom-2022wk25.md>

Commit considered: cf8c72174da5983f0b1b89efc2efd34a6c8c6aad

No objections, approved.


#### Approval of the Roadmap changes

Amit shares the document and presents the modifications, altough since it has not been shared beforehand we should leave time for in-depth reading. Amit reports no substantial changes occurred, more as data are shown. Therefore it should be approved next time, Amit will share it later today in the mailing list.

The document is in the resources section of the Oniro WG Steering Committee in Gitlab: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/resources_documentation_presentations/oniro_platform_release_roadmap_alpha_beta_20220708.pdf> 


#### Resolutions

- Approve the minutes at db5cfbee
- Postpone the approval of the Roadmap changes to the next SC meeting to allow people enough time to go through them in detail.


#### AoB  

We discussed on the progress on the IP compliance, so far the external company retained by EF (Orcro) is satisfied with the general process. We seem to be on track. Yocto and packages freeze should be imminent anyway, so also the end of the of the audit should be approach the final 20% (roughly). Also Amit reports that the PMC will not consider necessary that the toolchain runs on the EF infrastructure, since the process is open, validated, trusted and can be consumed by EF after review. Yet EF would like the transition happens soon, but it is not a blocker anymore.

### Oniro WG SC All Members Meeting minutes

#### Topics for the Oniro WG Specification Projects

Selection of specifications to start is under way.

- Avoid overalps with Open Harmony
- Meaningful
- We can finish in time

Martha is looking for ideas. A template is being circulated to that effect.

All specifications must come from code (code-driven, not even "code first"). Want to finish pretty soon.


#### Cubit Innovation Labs presentation

Luca T presents. Link to the corporate presentation #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/marketing-oniro/visuals-resources-oniro/-/blob/main/deck-visuals-resources-oniro/cubit/CUBIT_pres_2022.07_ENG.pdf>

#### Program Plan status


#### AoB  (Open Floor)

None tabled

## Links and references

### Links

This is the list of relevant links:
* Repository where you will find the agenda and meeting minutes to be approved (private repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve>
* Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc>
* Oniro WG Program Plan: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro_Program_Plan_2022.pdf>
* Oniro WG Budget: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro-2022-Working-Group-Budget-Summary.pdf>

This is the list to reference and other important notes to understand this document:
* AoB: Any Other Business
* MoM: Minutes of Meeting
* WG: Working Group
* SC: Steering Committee
* EF: Eclipse Foundation
* OH: Open Harmony

Approved version: 5b33591e646b3dd75c435392f582ca49f9cd9a5d
