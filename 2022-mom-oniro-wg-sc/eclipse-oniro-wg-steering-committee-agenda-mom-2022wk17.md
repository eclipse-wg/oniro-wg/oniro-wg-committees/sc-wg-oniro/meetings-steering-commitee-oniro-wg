# Oniro Working Group Steering Committee Launch Meeting 2022wk17

MoM Status: approved by on the 2022wk19 session commit 41d1c4f7458fd7ecd13521d1fb10bba06a6fce90S

[comment]: # (approved on YYYY-MM-DD)

* Date: 2022-04-28
* Time: from 15:00 to 16:00 CEST
   * Oniro WG SC meeting: from 15:00 to 15:30 CEST
   * Oniro WG SC All Members meeting: from 15:30 to 16:00 CEST
* Chair: Davide Ricci
* Minutes taker: Carlo Piana
* Join via Zoom
   * #link <https://eclipse.zoom.us/j/86710449041?pwd=dmk1OW56emQvU1YvSjBUY1AyOHIzUT09>
   * Meeting ID: 867 1044 9041
   * Passcode: 293617

## Agenda

Oniro WG SC meeting
* Approval of the meeting minutes from the 2022-04-14 Oniro WG SC Meeting - Davide R. 5 minutes
* Candidate Program Plan approval - Davide R. 5 min
* Budget proposal - Sharon C. 15 minutes
* AoB (Open Floor) - 5 minutes

Oniro WG SC All Members meeting

* Program Plan discussion and next steps - Davide R. 25 min
* AoB (Open Floor) - 5 minutes

## Attendees

### Oniro WG SC Meeting

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

* Davide Ricci - Huawei
* Carlo Piana - Array


Steering Committee Regrets:

* Amit Kucheria - Committers

Other Attendees:

* Adrian O'Sullivan - Huawei alternate (at 15:10, during discussion of budget)

Eclipse Foundation:

* Agustin Benito Bethencourt
* Sharon Corbett


### Oniro WG SC All Members Meeting

Beyond the above, the participants in this section were:

* Andrea Basso - Synesthesia
* Gianlucca Venere - SECO


## Minutes

### Oniro WG SC meeting minutes

#### Approval of the meeting minutes from the 2022-04-14 Oniro WG SC Meeting

* Agenda and meeting minutes to be approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/agenda-minutes-to-approve/eclipse-oniro-wg-steering-committee-agenda-mom-2022wk15.md>

* Approved


#### Candidate Program Plan consolidated draft approval

* Please find the Program PLan draft in .pptx , .odp and .pdf versions in this folder of the repository: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/program-plan-and-budget-oniro-wg>

* approved 


### Budget proposal

Sharon presents a budget proposal. This presentation will be provided as PDF to SC members. Since the members have not had the opportunity to review beforehand, it is proposed to postpone any decisions to next SC meeting and to consolidate it with the Program Plan.


#### Resolutions

* approve the minutes
* approve the Candidate Program Plan consolidated


#### AoB  (Open Floor)

No AoB

### Oniro WG SC All Members Meeting minutes

#### Program Plan discussion and next step

* Please find the Program PLan draft in .pptx , .odp and .pdf versions in this folder of the repository: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/program-plan-and-budget-oniro-wg>

* Agustin presents to the audience and invites comments by the WG members participating. No main comments, general appreciation.



##### Links

* #link to the diagram that summarises the relation between specs, initiatives and a release <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/roadmap-oniro-wg/wishlist-roadmap/wishlist-repo/-/raw/main/Oniro_elements_relation.png>
* #link Infography that summarises the process (draft) as well as relevant links <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/roadmap-oniro-wg/wishlist-roadmap/wishlist-repo/-/blob/main/oniro_roadmapping_process_infographic.pdf>
* #link Roadmapping process description (draft) <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/roadmap-oniro-wg/wishlist-roadmap/-/wikis/home>
* You can find a more detailed description of next steps in the ticket #link: <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-management/-/issues/5>


#### AoB  (Open Floor)

Agustin mentions two items:

* Specification committee first meeting next week. Please particpate. 
* Oniro platform release roadmap process. Version 3 out for evaluation.

Adrian mentions next two main opportunity to meet prospect:

* Keynote Dublin Tech Summit
* IOT Week


## Links and references

### Links

This is the list of relevant links:
* Repository where you will find the agenda and meeting minutes to be approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve>
* Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/>
* Oniro WG Program Plan: WIP
* Oniro WG Budget: WIP

This is the list to reference and other important notes to understand this document:
* AoB: Any Other Business
* MoM: Minutes of Meeting
* WG: Working Group
* SC: Steering Committee

Meeting is adjourned at 15:59
