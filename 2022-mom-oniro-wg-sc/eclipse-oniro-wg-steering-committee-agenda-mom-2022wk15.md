# Oniro Working Group Steering Committee Launch Meeting 2022wk15

MoM Status: not approved

[comment]: # (approved on YYYY-MM-DD)

* Date: 2022-04-14
* Time: from 15:00 to 16:00 CEST
   * Oniro WG SC meeting: from 15:00 to 15:30 CEST
   * Oniro WG SC All Members meeting: from 15:30 to 16:00 CEST
* Chair: Davide Ricci
* Minutes taker: Carlo Piana
* Join via Zoom
   * #link <https://eclipse.zoom.us/j/86710449041?pwd=dmk1OW56emQvU1YvSjBUY1AyOHIzUT09>
   * Meeting ID: 867 1044 9041
   * Passcode: 293617

## Agenda

Oniro WG SC meeting
* Approval of the meeting minutes from the 2022-03-31 Oniro WG SC Meeting - Davide R. 5 minutes
* Candidate Program Plan approval - Davide R. 15 min
* Resolutions - Davide R. 5 minutes
* AoB (Open Floor) - 5 minutes

Oniro WG SC All Members meeting

* Program Plan - Davide R. 10 min
* Roadmap process discussion - Agustin B.B. 5 minutes
* Specification Committee and projects feedback of shared material discussion - Davide R. 10 min
* AoB (Open Floor) - 5 minutes

## Attendees

### Oniro WG SC Meeting

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

* Davide Ricci - Huawei

* Carlo Piana - Array

* Amit Kucheria - Committers

Steering Committee Regrets:

* none

Other Attendees:

* Adrian O'Sullivan - Huawei alternate

Eclipse Foundation:

* Agustin Benito Bethencourt

[comment]: # (* Sharon Corbett)

* Yves

* Paul Buck

### Oniro WG SC All Members Meeting

Beyond the above, the participants in this section were:

* Andrea Basso - Synesthesia

* Andrea Gallo - Linaro

* Bill Fletcher - Linaro

[comment]: # (* Gianlucca Venere - SECO)

[comment]: # (* Patrick Onhewein - NOI Techpark)


## Minutes

### Oniro WG SC meeting minutes

#### Approval of the meeting minutes from the 2022-03-31 Oniro WG SC Meeting

* Agenda and meeting minutes to be approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/agenda-minutes-to-approve/eclipse-oniro-wg-steering-committee-agenda-mom-2022wk13.md>

Meeting minutes and Agenda are approved.


#### Program Plan consolidated draft approval

This is only a preliminary version for larger consideration and formal appproval. Davide illustrates. There is no objections to move it to the next phase for approval.

#### Specification Committee and projects feedback of shared material discussion

Description:

We have started to take the initial steps towards creating the Oniro WG Specification Committee and Specification projects. These are the main steps of such process:
1. Strategic Member appoint Oniro WG Spec. Committee representative.
1. Specification Committee Launch meeting.
1. Election of the additional two committee members representing Silver members and committers.
1. Specification Committee Meeting with all members.
   1. 30, 60, 90 days plan as guidance.
1. Specification project submission.
1. Adoption of responsibility by the Oniro Spec. Committee of the specification project. 
1. Specification project approved.
1. Committers appointed.

For lack of time, and consensus to discuss it openly, moved to the public phase.

#### Resolutions

- Approve Minutes
- Approve candidate program plan.


#### AoB  (Open Floor)

No AoB

### Oniro WG SC All Members Meeting minutes

#### Program Plan

* Please find the Program PLan draft in .pptx , .odp and .pdf versions in this folder of the repository: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/program-plan-and-budget-oniro-wg>

Prgram plan as approved in its candidate version is shared with the people attending.

#### Roadmap process discussion

Description

Current state of the process:
* First round of feedback adressed.
* Second round of feedback has been acknowledged and discussed. It is in the process of being included in the process documentation.
   * The team working on this (Agustin and Sebastian) have collected feedback from (not only): Jarek, Phil, Marta, Carlo and Andrei, in addition to several people at Eclipse Foundation.
 
Next steps:
* Add the remain changes to the documentation and socialise it.
* Start the third and final iteration.
* Provide the documentation to the Oniro WG SC.
* Include the approval of the process in the next Oniro WG SC meeting.
* Publish the process.
* Select the Roadmap Team members.
* Start implementing it.

##### Links

* #link to the diagram that summarises the relation between specs, initiatives and a release <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/roadmap-oniro-wg/wishlist-roadmap/wishlist-repo/-/raw/main/Oniro_elements_relation.png>
* #link Infography that summarises the process (draft) as well as relevant links <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/roadmap-oniro-wg/wishlist-roadmap/wishlist-repo/-/blob/main/oniro_roadmapping_process_infographic.pdf>
* #link Roadmapping process description (draft) <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/roadmap-oniro-wg/wishlist-roadmap/-/wikis/home>
* You can find a more detailed description of next steps in the ticket #link: <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-management/-/issues/5>

##### Discussion

Agustin runs the attending people through it. Agustin mentions that soon the elections for the Specification committee will be announced soon.


#### AoB  (Open Floor)

No AoB

## Links and references

### Links

This is the list of relevant links:
* Repository where you will find the agenda and meeting minutes to be approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve>
* Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/>
* Oniro WG Program Plan: WIP
* Oniro WG Budget: WIP

This is the list to reference and other important notes to understand this document:
* AoB: Any Other Business
* MoM: Minutes of Meeting
* WG: Working Group
* SC: Steering Committee

Meeting is adjourned at 16:03
