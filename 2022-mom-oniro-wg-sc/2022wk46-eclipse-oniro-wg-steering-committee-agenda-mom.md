# Oniro Working Group Steering Committee Meeting 2022wk44

MoM Status: approved on 2022-11-24 commit de69075c

* Date: 2022-11-17
* Time: from 15:00 to 15:45 UTC
   * Oniro WG SC All Members meeting: from 15:00 to 15:45 UTC
* Chair: Davide Ricci
* Minutes taker: Carlo Piana
* Join via Zoom

## Agenda

Oniro WG SC meeting

* Approval of the meeting minutes from the 2022-10-13 2022wk41 Oniro WG SC Meeting - Davide R. 5 minutes
* Approval of the meeting minutes from the 2022-10-26 to 2022-10-27 2022wk43 Oniro WG SC Meeting - Davide R. 5 minutes
* Approval of the meeting minutes from the 2022-11-03 2022wk44 Oniro WG SC Meeting - Davide R. 5 minutes
* Oniro WG Budget 2023 and Program Plan 2023 confirmation - Davide R. 20 minutes
* Next steps - Agustin - 5 minutes
* AoB - 5 minutes


## Attendees

All Member representatives have been invited to the full meeting. No private session.

### Oniro WG SC Meeting

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

* Davide Ricci - Huawei
* Alberto Pianon - Array alternate
* Carlo Piana - Array. Silver Members representative
* Adrian O'Sullivan - Huawei alternate


There is quorum

Steering Committee Regrets:

* Amit Kucheria - Commiters

Other Attendees:

[comment]: # (* Luca Tavanti - Cubit)
[comment]: # (* Luca Miotto - NOI alternate)
[comment]: # (* Patrick Ohnewein - NOI)
[comment]: # (* Xinxin Fan - IoTex alternate)
[comment]: # (* Steeve Baudy - IoTex)

[comment]: # (* Bill Fletcher - Linaro alternate)
[comment]: # (* Gianluca Venere - SECO)
[comment]: # (* MArco Sogli - SECO alternate)
 

Eclipse Foundation:

* Agustin Benito Bethencourt
* Sharon Corbbet

[comment]: # (* Paul Buck)

[comment]: # (* Gael Blondelle)

## Minutes

### Oniro WG SC meeting minutes

#### Approval of the meeting minutes from the 2022-10-13 2022wk41 Oniro WG SC Meeting

* MoM to approve: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/agenda-minutes-to-approve/2022wk41-eclipse-oniro-wg-steering-committee-agenda-mom.md>

#### Approval of the meeting minutes from the 2022-10-26 to 2022-10-27 2022wk43 Oniro WG SC Meeting

* MoM to approve: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/2022-mom-oniro-wg-sc/2022wk43-eclipse-oniro-wg-steering-committee-agenda-mom.md>
* This MoM is in our Oniro WG SC public repository despite not being approved yet because it correspond to the meeting we held at Eclipsecon 2022 where we agree that the meeting would be open to Member representatives and PMC. In addition, we invited additional people.

#### Approval of the meeting minutes from the 2022-11-03 2022wk44 Oniro WG SC Meeting

* MoM to approve: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/agenda-minutes-to-approve/2022wk44-eclipse-oniro-wg-steering-committee-agenda-mom.md>

Carlo and Davide approve, that's a quorum, approved.

#### Oniro WG Budget 2023 and Program Plan 2023

Budget to be introduced by Sharon C. from EF:
* Budget Oniro WG 2023: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf>
* Oniro Program Plan candidate to confirm: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf>
   * You can find it in other formats in this forlder: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023>

Sharon presents the above documents.

Questions from the audience on the budget.

No objections, the budget is approved at commit `[xxxxxx]`.


#### Next steps

Next steps:
* Focus on changes at product level understanding that, in the current setup of the project, the baseline specs will also be (soft) linked to OpenHarmony specs.
* Put energy in creating the value proposition of Oniro based on the new product architecture.

Agustin leads 

#### Resolutions

##### MoM approved:

| Week     | commit   |
| -------- | -------- |
| 2022wk41 | 30ef91bf |
| 2022wk43 | 6c9c5cf7 |
| 2022wk44 | 3160fb0e |

Are Approved

##### Budget approved

Budget is approved at commit 31f49bda.  

**Note**: pending reception of the actual document to be reviewed, as shown in the presentation.

The document was reviewed and the corresponding commit changed based on The following MR <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/merge_requests/39>

#### AoB  (Open Floor)



## Links and references

### Links

This is the list of relevant links:
* Repository where you will find the agenda and meeting minutes to be approved (private repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve>
* Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc>
* Oniro WG Program Plan: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro_Program_Plan_2022.pdf>
* Oniro WG Budget: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro-2022-Working-Group-Budget-Summary.pdf>

This is the list to reference and other important notes to understand this document:
* AoB: Any Other Business
* MoM: Minutes of Meeting
* WG: Working Group
* SC: Steering Committee
* EF: Eclipse Foundation
* OH: Open Harmony

Meeting closes at xxxx UTC

All times UTC unless expressly mentioned
