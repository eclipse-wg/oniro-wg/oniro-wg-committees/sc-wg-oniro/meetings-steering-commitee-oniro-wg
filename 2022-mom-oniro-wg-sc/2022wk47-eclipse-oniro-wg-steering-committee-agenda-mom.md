# Oniro Working Group Steering Committee Meeting 2022wk47

MoM Status: approved on 2023-02-02 commit df444d60

* Date: 2022-11-24
* Time: from 14:00 to 15:00 UTC
   * Oniro WG SC All Members meeting: from 15:00 to 15:45 UTC
* Chair: Davide Ricci
* Minutes taker: Carlo Piana
* Join via Zoom

## Agenda

Oniro WG SC meeting

* Approval of the meeting minutes from the 2022-10-13 2022wk46 Oniro WG SC Meeting - Davide R. 5 minutes
* Summary of the Oniro v2.0 release - Luca Miotto 20 minutes
* IoTeX introduction - 10 minutes
* Resolutions - 5 minutes
* AoB - 5 minutes

## Attendees

All Member representatives have been invited to the full meeting. No private session.

### Oniro WG SC Meeting

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

* Davide Ricci - Huawei
* Alberto Pianon - Array alternate
* Carlo Piana - Array. Silver Members representative
* Adrian O'Sullivan - Huawei alternate
* Amit Kucheria - Commiters
* Andrei Gerzan - Committers alternative

Steering Committee Regrets:

Other Attendees:

* Luca Tavanti - Cubit
* Luca Miotto - NOI alternate
* Patrick Ohnewein - NOI
* Xinxin Fan - IoTex alternate
* Steeve Baudy - IoTex
* Andrea Gallo (Linaro) from 14:30
* Kai Hudalla (until 14:36)
* Janian Zhang
* Ettore Chimenti (Seco) 

[comment]: # (* Bill Fletcher - Linaro alternate)
[comment]: # (* Gianluca Venere - SECO)
[comment]: # (* MArco Sogli - SECO alternate)

Eclipse Foundation:

* Paul Buck

[comment]: # (* Agustin Benito Bethencourt)
[comment]: # (* Sharon Corbbet) 
[comment]: # (* Gael Blondelle)

## Minutes

### Oniro WG SC meeting minutes

#### Approval of the meeting minutes from the 2022-10-13 2022wk41 Oniro WG SC Meeting

* MoM to approve: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/agenda-minutes-to-approve/2022wk46-eclipse-oniro-wg-steering-committee-agenda-mom.md>

Approved unanymously de69075c

#### Summary of the Oniro v2.0 release by Luca Miotto. Anyone else?

Luca provides a high level summary of the release. #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/resources_documentation_presentations/20221124_Summary_Release_2.0.0_Oniro.pdf>

Carlo replies to Kai Hudalia question on IP policy, checks,  CVE checks, bug triaging etc. Gives the links to some of the most important documents.

* #link <https://git.ostc-eu.org/oss-compliance/ip-policy>
* #link <https://docs.oniroproject.org/en/latest/security/index.html> 


#### IoTeX introduction

Xinxin Fan introduces the company.

#### Resolutions

To adopt minutes from the 2022-10-13 2022wk41 Oniro WG SC Meeting at commit de69075c

#### AoB  (Open Floor)



## Links and references

### Links

This is the list of relevant links:
* Repository where you will find the agenda and meeting minutes to be approved (private repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve>
* Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc>
* Oniro WG Program Plan: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/Oniro_Program_Plan_2023.pdf>
* Oniro WG Budget: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program-plan-budget-2023/oniro-budget-2023/2023-Oniro-Workking-Group-Budget.pdf>

This is the list to reference and other important notes to understand this document:
* AoB: Any Other Business
* MoM: Minutes of Meeting
* WG: Working Group
* SC: Steering Committee
* EF: Eclipse Foundation
* OH: Open Harmony

Meeting closes at 14:58 UTC

All times UTC unless expressly mentioned
