# Oniro Working Group Steering Committee Meeting 2022wk33

MoM Status: approved on 2022-09-01 commit 7295c096

* Date: 2022-08-18
* Time: from 15:02 to 16:04 CEST
   * Oniro WG SC meeting: from 15:01 to 15:31 CEST
   * Oniro WG SC All Members meeting: from 15:31 to 16:00 CEST
* Chair: Adrian O.S. 
* Minutes taker: Agustin B.B.
* Join via Zoom
   * #link <https://eclipse.zoom.us/j/86710449041?pwd=dmk1OW56emQvU1YvSjBUY1AyOHIzUT09>
   * Meeting ID: 867 1044 9041
   * Passcode: 293617

## Agenda

Oniro WG SC meeting

* Approval of the meeting minutes from the 2022-08-04 Oniro WG SC Meeting - Davide R. 5 minutes
* Oniro 2022Q2 report. Analysis.
   * State of the report - Agustin B. 5 min
   * Budget report - Sharon C. 5 min.
   * Approval of the Oniro Program Plan 2022Q2 report - 5 min,
* Resolutions - 5 minutes 
* AoB (Open Floor) - 5 minutes

Oniro WG SC All Members meeting

* Oniro 2022Q2 report. Analysis.
   * State of the report - Agustin B. 5 min
   * Discussion - 15 min
* EclipseCon 2022 goals and participation - Agustin 5 min
* AoB - 5 minutes

## Attendees

### Oniro WG SC Meeting

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

* Davide Ricci - Huawei
* Amit Kucheria - Committers
* Adrian O'Sullivan - Huawei alternate


Steering Committee Regrets:

* Carlo Piana - Array. Silver Members representative
* Alberto Pianon - Array alternate

Other Attendees:


Eclipse Foundation:

* Agustin Benito Bethencourt
* Paul Buck 
* Sharon Corbbet (left at 15:54)

### Oniro WG SC All Members Meeting

Beyond the above, the participants in this section were:

[comment]: # (* Bill Fletcher - Linaro)
[comment]: # (* Luca Tavanti - Cubit)
[comment]: # (* Andrea Basso - Synesthesia)
[comment]: # (* Luca Miotto - NOI)

* Carlo Piana - Array. Silver Members representative (joined at 15:48)

## Minutes

### Oniro WG SC meeting minutes

#### Approval of the meeting minutes from the 2022-08-04 Oniro WG SC Meeting

* Agenda and meeting minutes to be approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/agenda-minutes-to-approve/eclipse-oniro-wg-steering-committee-agenda-mom-2022wk31.md>

Commit considered: d8e734545779d72e6c112b03b15ce94d3eb772d2

No objections recorded, minutes are approved

#### Oniro 2022Q2 report. Analysis.

These are the links where you can get the Oniro Program Plan 2022Q2 report:
* Oniro Program Plan 2022Q2 Report draft in .pdf version #link: <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro_Program_Plan_report_2022Q2.pdf>
* Oniro Program Plan 2022Q2 Report draft collaboration version #link: <https://docs.google.com/presentation/d/1R-oBfH7lxmFvXa0GH_Yw35UYCzeRSZpV6jBKG9ygADg/edit?usp=sharing>  
* Oniro 2022Q2 budget report: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro-2022-Working-Group-Budget-June-30th-Update.pdf>                                                                                                                                             |

Sharon provides a summary of the budget #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program_plan_2022_budget_reports/Oniro-2022-Working-Group-Budget-June-30th-Update.pdf>

Q: if budget doesn't match the proposed activities, when is the discussion to conceil the two happening?
A: between Nov 4th and Nov 18 EF prepares the budget and needs to be discussed and ack. within the WGs

#### Resolutions

* Approved unanimously the 2022wk31 MoM 


#### AoB  

None

### Oniro WG SC All Members Meeting minutes

#### Oniro 2022Q2 report. Analysis.

These are the links where you can get the Oniro Program Plan 2022Q2 report:
* Oniro Program PLan 2022Q2 Report draft in .pdf version #link: <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro_Program_Plan_report_2022Q2.pdf>
* Oniro Program Plan 2022Q2 Report draft collaboration version #link: <https://docs.google.com/presentation/d/1R-oBfH7lxmFvXa0GH_Yw35UYCzeRSZpV6jBKG9ygADg/edit?usp=sharing>


#### EclipseCon 2022 goals and participation

* Participants list: #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/Events/EclipseCon#attendees-from-oniro>
Please join if you attend to EclipseCon 2022

##### Oniro f2f committee meetings

* Link to the section of the wiki page about the f2f Oniro Committee meetings at EclipseCon 2022: #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/Events/EclipseCon#5-oniro-committees-f2f-meetings>
* Current schedule: #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/Events/EclipseCon#schedule>

Oniro meetings will take place on Thursday 27 October during the morning:
* From 9:00 to 9:55: Oniro WG Steering Committee
* From 10:05 to 10:55: Oniro Prospects Pipeline meeting
* From 11:05 to 11:55: Oniro WG Marketing Committee
* From 12:05 to 12:55: Oniro PMC
* From 13:05 to 13:55: Oniro WG Specification Committee
* From 14:05 to 14:55: Oniro Platform Release Roadmap team

##### Oniro WG SC Meeting October 27th

* Link to Program Plan 2023 timeline: #link <https://www.eclipse.org/lists/oniro-wg/msg00269.html> 

###### Program Plan timeline

The key steps that determine the approval of every WG Program Plan are:
* Program Plan Development
* Draft Program Plan creation
* Candidate Program Plan approved by the WG Steering Committee
* Based on such candidate, EF creates the budget.
* Budget and Program Plan approved by the WG SC.
* Program Plan and budget approved by the EF Exec. Director.
* Overall EF budget approved by the EF Board of Directors.  
 
Sharon provides a description of the Program Plan timeline #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program_plan_2023_drafts/2023-WG-PP-&-Budget-Timeline-image.png>

Link to additional information provided by Sharon
* #link to template <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program_plan_2023_drafts/2023-Program-Plan-Template.pptx>
* #link to Program PLan approval process deck <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program_plan_2023_drafts/2023-Program-Plan-and-Budget-Process-Deck.pdf>

###### Timeline for Oniro

Oniro Program Plan 2023 timeline is as follows:
* Early October: Oniro Program Plan 2023 discussion
* Mid October: draft created
* Approval of the Oniro Program Plan 2022 candidate at EclipseCon 2022 on October 27th (f2f Oniro WG SC meeting).
   * Deadline is November 4th
* Oniro Program Plan 2023 and budget approved by Oniro WG SC on November 10th
   * Dealine is November 18th

##### Discussion

Brought here part of the discussion that took place in the private part on this topic, which was not planned there.
* Sharon ecaurages the WG to start the Program Plan discussions early in September.
* Discussion about how at Oniro we need to adjust our previous program plan discussion timeline to the one from EF.
   * Agustin proposal is based on the EF program plan 2023 discussion timeline. 
   * Adrian and Agustin points of view are reflected in a mail conversation at oniro-wg-steering-committee mailing list (no archive)

#### AoB  (Open Floor)

* IT plan is in its final review stage. It will be presented to the PMC and the Oniro WG SC by Agustin.
* Huawei's sponsors talk at EclipseCon 2022 will be about Oniro. Open Source Way track.
* Davide informs about his efforts to recruit device makers.

## Links and references

### Links

This is the list of relevant links:
* Repository where you will find the agenda and meeting minutes to be approved (private repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve>
* Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc>
* Oniro WG Program Plan: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro_Program_Plan_2022.pdf>
* Oniro WG Budget: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro-2022-Working-Group-Budget-Summary.pdf>

This is the list to reference and other important notes to understand this document:
* AoB: Any Other Business
* MoM: Minutes of Meeting
* WG: Working Group
* SC: Steering Committee
* EF: Eclipse Foundation
* OH: Open Harmony
