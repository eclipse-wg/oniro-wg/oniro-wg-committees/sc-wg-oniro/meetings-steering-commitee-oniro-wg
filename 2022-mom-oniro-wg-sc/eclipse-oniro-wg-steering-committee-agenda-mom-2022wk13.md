# Oniro Working Group Steering Committee Launch Meeting 2022wk13

State: approved on 2022-04-14

* Date: 2022-03-31
* Time: from 15:00 to 16:00 CEST
* Chair: Davide Ricci
* Minutes taker: Carlo Piana
* Join via Zoom
   * #link <https://eclipse.zoom.us/j/86710449041?pwd=dmk1OW56emQvU1YvSjBUY1AyOHIzUT09>
   * Meeting ID: 867 1044 9041
   * Passcode: 293617

## Agenda

Oniro WG SC meeting
* Approval of the meeting minutes from the 2022-03-03 Oniro WG SC Meeting - Davide R. 5 minutes
* Oniro Goofy Roadmap approval - Davide R. 15 minutes
* Resolutions - Davide R. 5 minutes
* AoB (Open Floor) - 5 minutes

Oniro WG SC All Members meeting
* Roadmap process discussion - Agustin B.B. 15 minutes
* Program Plan Introduction - Agustin B.B. 10 min
* AoB (Open Floor) - 5 minutes

## Attendees

### Oniro WG SC Meeting

Steering Committee Members (Quorum =  2 of 3 for this Meeting):
* Davide Ricci (Huawei)
* Carlo Piana (Array)
* Amit Kucheria (PMC)

Steering Committee Regrets:
* none

Other Attendees:
* Adrian O'Sullivan (Huawei alternate)

Eclipse Foundation:
* Sharon Corbett

* Agustin Benito Bethencourt

### Oniro WG SC All Members Meeting

Beyond the above, the participants in this section were:

* Yves (EF)

## Minutes

### Approval of the meeting minutes from the 2022-03-17 Oniro WG SC Meeting

* Agenda and meeting minutes to be approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/2022-mom-oniro-wg-sc/eclipse-oniro-wg-steering-committee-agenda-mom-2022wk11.md>

Meeting minutes and Agenda are approved


### Oniro Goofy Roadmap Approval

* #link to the current epics <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/roadmap-oniro-wg/engineering-roadmap/-/epic_boards/59>

Davide describes the single points that are problematic (in terms of confidence we have enough workforce to complete them in time), or are outright struck out for the same reasons.

* #link to the proposed roadmap to approve <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/resources_documentation_presentations/Oniro-Goofy-Roadmap-approved-20220331.pdf>

Questions and objections?

Amit: no.

Davide: container and optimization we know matter a lot to prospective partners -- if we end up not achieving 100% we need to prioritize we'll be less appealing, so we need to work with them which kind of features are minimum set to be viable for them to join.

Agustin: How do we re-approve any changes in requirements from here to the next one-two months? Davide: any requirements that needs to be done within Goofy needs to be brought to the attention of the SC, otherwise no chances will occur.

A: if in one month we have RM in place, what we can do is bring it again and approve changes. D: yes, it would be minimal.

Sharon -- this is a document that is going to be approved, so it needs to be in the agenda and circulated well beforehand so people can take informed decisions, has it? Davide: it has. Sharon -- Everybody needs to be have a chance to review diligently. Davide: anyway if a member requests more time he's allowed and we can wait until the next SC or we can have an electronic vote in the intervening time.

Carlo suggests that the roadmap is "sealed and marked" as a result of today's approval, circulated to be discussed in the following days in case there are unnoticed modification or points that went unaddressed. Agustin asks to have a PDF to be put on Gitlab whose address is shared with the SC members.

### Resolutions

- Agenda is approved

- Minutes of last meeting are approved

- Roadmap for Goofy is approved (80% confidence). Roadmap will be transformed into a PDF and will be open for inspection at designated place until next meeting for final approval (of the document). #link to the .pdf version <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/resources_documentation_presentations/Oniro-Goofy-Roadmap-approved-20220331.pdf>



- UNtil the roadmapping process is approved, changes to this roadmap will need to be reviewed and approved. We will do a quarterly review and will bring to Oniro WG SC significnat changes if there are.


### AoB  (Open Floor)

None, private part of the meeting is adjourned at 15:34

Open discussion participants are allowed to join.

### Roadmap process discussion

* #link to the diagram that summarises the relation between specs, initiatives and a release <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/roadmap-oniro-wg/wishlist-roadmap/wishlist-repo/-/raw/main/Oniro_elements_relation.png>
* #link Infography that summarises the process (draft) as well as relevant links <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/roadmap-oniro-wg/wishlist-roadmap/wishlist-repo/-/blob/main/oniro_roadmapping_process_infographic.pdf>
* #link Roadmapping process description (draft) <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/roadmap-oniro-wg/wishlist-roadmap/-/wikis/home>

This is "public phase". Agustin re-describes the flow for forming the roadmap. Invites people to review and provide feedback during the intervening time, at the next meeting he will bring a further draft of the document.

Adrian: is it going to be run past the PMC? Agustin confirms for the release all the items must be approved. There is a need to avoid inconsistencies between roadmap and release. So the corner cases need anyway to be resolved within the Roadmap Team.


### Program Plan introduction

* Please find the Program PLan draft in .pptx , .odp and .pdf versions in this folder of the repository: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/program-plan-and-budget-oniro-wg>

Agustin discusses:

- The program plan is upon the WG, EF can only support.
- Runs through the slides above referenced.
- No-brainers are no brainers.
- Program objectives is where decisions start needing to be made. For each there are strategies and tactics.
- Growth metrics are addressed.
- Member participation is discussed

We need to finalize the Program plan so that we start spending budget.

Davide is going to take over the process and go rounds to receive input from members and have the doc uploaded in 7 days so that everybody can add, subtract. 

Agustin takes the task of disseminating. We can have 1/2 hr next week to discuss the status of the document. Good idea for Davide. 3:30 CEST Thursday April 7th

### AoB  (Open Floor)

We repeat the same format next time. More attention to advertise the scope and format to the larger audience. Agustin to send again the invitation for the member representatives.

Meeting closes at 16:07.

## Links and references

* Repository where you will find the agenda and meeting minutes to be approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve>
* Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/>

* AoB: Any Other Business
* MoM: Minutes of Meeting
