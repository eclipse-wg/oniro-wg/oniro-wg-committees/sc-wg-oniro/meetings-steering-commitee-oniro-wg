# Oniro Working Group Steering Committee Meeting 2022wk44

MoM Status: approved on 2022-11-17 commit 3160fb0e

* Date: 2022-11-03
* Time: from 14:00 to 14:45 UTC
   * Oniro WG SC All Members meeting: from 14:00 to 14:45 UTC
* Chair: Davide Ricci
* Minutes taker: Alberto Pianon
* Join via Zoom

## Agenda

Oniro WG SC meeting

* Discussion and Approval of the Oniro Program Plan 2023 candidate - Davide R. 35 minutes
* Resolutions - Davide R. - 5 minutes
* AoB - 5 minutes


## Attendees

All Member representatives have been invited to the full meeting. No private session.

### Oniro WG SC Meeting

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

* Davide Ricci - Huawei
* Alberto Pianon - Array alternate
* Carlo Piana - Array. Silver Members representative

[comment]: # (* Adrian O'Sullivan - Huawei alternate)

There is quorum

Steering Committee Regrets:

* Amit Kucheria (Commiters)
* Adrian O'Sullivan - Huawei alternate

Other Attendees:

* Luca Tavanti - Cubit
* Luca Miotto - NOI alternate
* Patrick Ohnewein - NOI
* Xinxin Fan - IoTex alternate
* Steeve Baudy - IoTex

[comment]: # (* Bill Fletcher - Linaro alternate)
[comment]: # (* Gianluca Venere - SECO)
[comment]: # (* MArco Sogli - SECO alternate)
 

Eclipse Foundation:

* Agustin Benito Bethencourt

[comment]: # (* Paul Buck)
[comment]: # (* Sharon Corbbet)
[comment]: # (* Gael Blondelle)

## Minutes

### Oniro WG SC meeting minutes

#### Discussion and Approval of the Oniro Program Plan 2023 candidate

Description of the changes introduced in the last version 202211v0.2 by Agustin based on feedback collected:

* Slide 2: erased.
* Slide 8 and 9: cosmetic/format changes.
* Slide 14
   + Prioritize the sustainability of oniro-core preventing “the tragedy of the commons”
* Slide 15
   - Ecosystem fully engaged in attracting new Member related activities.
	+ Ecosystem fully engaged in Oniro’s Prospects Pipeline related activities, supported by Eclipse Foundation.
* Slide 20
   - Oniro Compatibility Program for OpenHarmony profile aligned with OpenHarmony project (from OpenAtom) compatibility program.
   + Oniro Compatibility Program design for OpenHarmony profile aligned with OpenHarmony project (from OpenAtom) compatibility program.


Patrick catches a bug in slide 22 changing 2022 by 2023 in the title. Agustin will change it, together with erasing the word draft in the first page. Since the Program PLan and the budget needs to be confirmed by the Oniro WG SC, such version will include this change, not approved today.

The vote of the [Oniro Program Plan 2023 draft 202211v0.2 commit](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program_plan_2023_drafts/Oniro_Program_Plan_2023_draft.pdf) 54cdb332f281ec1e614e17891b4d457c8487142b:
* Carlo approves
* Davide approves

Program PLan 2023 approved so now moves from draft to candidate

##### Program Plan 2023 links

These are the links where you can find the Oniro Program PLan draft:
* In .pptx format: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program_plan_2023_drafts/Oniro_Program_Plan_2023_draft.pptx>
* In .odt format: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program_plan_2023_drafts/Oniro_Program_Plan_2023_draft.odp>
* In .pdf format #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program_plan_2023_drafts/Oniro_Program_Plan_2023_draft.pdf>


#### Resolutions

The Program Plan *draft* is unanimously approved so it turns into a Program Plan *candidate*

#### AoB  (Open Floor)

Clarification about the meaning of verticals/profiles, led by members' use cases.

## Links and references

### Links

This is the list of relevant links:
* Repository where you will find the agenda and meeting minutes to be approved (private repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve>
* Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc>
* Oniro WG Program Plan: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro_Program_Plan_2022.pdf>
* Oniro WG Budget: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro-2022-Working-Group-Budget-Summary.pdf>

This is the list to reference and other important notes to understand this document:
* AoB: Any Other Business
* MoM: Minutes of Meeting
* WG: Working Group
* SC: Steering Committee
* EF: Eclipse Foundation
* OH: Open Harmony

Meeting closes at 14:41 UTC

All times UTC unless expressly mentioned
