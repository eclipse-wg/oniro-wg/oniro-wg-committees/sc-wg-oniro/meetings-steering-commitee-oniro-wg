# Oniro Working Group Steering Committee Meeting 2022wk43

MoM Status: approved on 2022-11-17 commit 6c9c5cf7

* Multi-session meeting open to All Member representatives
* Dates: From 2022-10-26 to 2022-10-27
* Time slots:
  * Session 1: 2022-10-26 from 15:30 to 17:30 CEST
  * Session 2: 2022-10-27 from 9:00 to 12:00 CEST
* Location: Forum am Schlosspark, Stuttgarter Straße 33, 71638 Ludwigsburg,
Germany.
  * #link https://www.eclipsecon.org/2022/conference-venue
  * Seminar room 5, on the ground floor, at the end of the hall
* Chair: Davide Ricci
* Minutes taker: Carlo Piana
* Join via Zoom
  * #link https://eclipse.zoom.us/j/86710449041?
pwd=dmk1OW56emQvU1YvSjBUY1AyOHIzUT09
  * Meeting ID: 867 1044 9041
  * Passcode: 293617

## Agenda

### Session 1 2022-10-26

Oniro WG SC All Members meeting:
* 15:30 to 16:00 Topic 1 - Agenda definition for the 2 sessions. Meetings
goals.
* 16:05 to 17:25 Topic 2 - tbd
* 16:35 to 17:00 Topic 3 - tbd
* 17:05 to 17:30 Topic 4 - tbd

### Session 2 2022-10-27

* 9:00 to 9:25 Topic 5 - Program objectives: definition
* 9:30 to 9:50 Topic 6 - Program objective 1 and 2
* 10:00 to 10:20 Topic 7 - Program objective 3 and 4
* 10:25 to 10:45 Topic 8 - Program objective 5 and 6
* 11:50 to 11:10 Topic 9 - KPIs and contributions
* 11:30 to 11:40 Approval of the Oniro Program Plan 2023 candidate - Davide
Ricci
* 11:40 to 11:45 Resolutions - Davide Ricci
* 11:45 to 11:55 AoB

All times in this section in CEST

## Attendees

All Member representatives at the different committees as well as the PMC
members have been invited to the full meeting.

### Session 1

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

* Davide Ricci - Huawei
* Amit Kucheria - Committers at 16:10
* Adrian O'Sullivan - Huawei alternate
* Carlo Piana - Array. Silver Members representative
* Alberto Pianon - Array alternate


Steering Committee Regrets:


Other Attendees:

* Luca Tavanti - Cubit
* Luca Miotto - NOI
* Patrick Ohnewein - NOI

[comment]: # (* Andrea Gallo - Linaro )
[comment]: # (* Antonio Miele - Politecnico Milano)
[comment]: # (* Francesco Amigoni - Politecnico Milano)
[comment]: # (* Gianluca Venere - SECO)
* Marta Rybczynska - Huawei
* Chiara Del Fabbro - Huawei
* Ettore Chimenti - SECO
* Stefan Schmidt - Huawei (after 17:00)
* Zygmunt Krynicki - Huawei

Eclipse Foundation:

* Agustin Benito Bethencourt

[comment]: # (* Paul Buck)
[comment]: # (* Sharon Corbbet)

Meeting starts at 15:30 CEST

### 2023 vision

#### Technical Direction

Andrei mainly reports

A Blueprint for a vertical will live in a separate add-on. In a separate subproject. Showing both technology and an use case.

We need One single big element strongly associated to the platform and visible. Find a company that wants to bring Oniro into production. We already have the concept of design win. We could not execute.

We need to invest more to support the lifecycle. Done well for Linux, less so for Zephyr.

Davide: The does the horizontal layer need more work next year or is it ready to support building components on the top?

Showing features part is ok, as for production-ready artifacts, there is still much to do. Many further aspects we need to handle, like identity etc., to be production ready. There is a tension between adding more features and finishing them to make them production ready. We might not have the resources to do both.

Ettore: for the security part we need to implement some security features, but the part we are more concerned into is functional. My clients need to be working and being reliable.

Davide: """

    I see here two directions

    - Not about breadth -- being a good base for addons
    - Work on production ready details

    We need production partners (like Seco) to define production details to showcase. """

We need more feedback from partners. We have no good system to receive feedback, especially requirements.

More manpower from partners also is needed.

Yocto compatibility would raise the value.

#### Specific on the compliance toolchain

Alberto reports.

We have added a graphical database to achieve graph queries. We have found another EF member, Kinetics, they are interested in helping us to design, develop the functionality.

We could be able to finally use all the information we have achieved to spot any incompatibility. Not any manual work any more.

The work we have done should be put in the open, through some API. We want to involve other EF members to invest in the project with developing, spec definition, standard.

Our current production toolchain covers only sources. The extra mile to binaries is done manually. Nobody else is doing this in Yocto at least. We are close to filling the gap. We also need to sell the concept that we need to add to Yocto some facilities to collect metadata at build time to such respect.

Adrian: do we have the ability to accept external contributions given the infrastructure?

No, but they can do MR -- actually the present team is doing the same for the Dashboard. Before externals can be elected committers, many more things should happen, not just code, but also requirements, design, architecture suggestions, etc.

Adrian: Oniro Compliance toolchain raises a lot of interest any time we present it. Needs not to be a WG member to consume and contribute to the project.

Agustin: you can do two ways: as a tool or as a service (in or outside Eclipse project, sharing resources). It's something you must decide, and be clear about.

It's not easy to port the toolchain to other projects, so many implementation details.

Andrei: do we have something ready or we'll figure out later?

We can do via API or to find a way to integrate on a case by case basis.

Adrian: Yocto does something like layer checking as a service.

**Feedback garnered from remote attendees.**

Amit adds his view that in general, making Oniro a platform for other projects is important.

Luca Tavanti finds that the terms "strategic goals" and "strategic objectives" are a bit
confusing: are they the same thing? If not, what is the difference? Also, are
the strategic objective descriptions the same as the Program Objectives?

Strategic Goal 1: the second point, "Design and execute a branding...", sounds like a commercial/marketing action, suggestion to move to SG2.

Suggestion to add some action aimed at "recruiting" developers to the Oniro cause


### Value proposition

#### Content and positioning

Davide: anything to add to what Oniro misses to be ready for production adoption (in depth)

Chiara: Is it an operating system, shall we pitch it like this?

Davide: or is it a platform?

Adrian: we currently claim and advertise it's an open source platform containing an operating system.

Agustin: we should avoid the "tragedy of the Commons", IE we need to find a way to fund the project by making the verticals to become a member.

Amit: separate Hardware from Core would not be a good idea

Davide: you should load the project with enough value to have the verticals to join, but not so much that they don't find value added

Stefan S: Avoid to have profiles created for someone/something and they don't give anything back to the project.

Everybody agrees it profiles vs core requires a lot of work. Value will probably come from services, as opposed to software. The platform is enough flexible to allow doing whatever (Andrei)

Agustin: we must be prepared to adapt to certain behaviours, EG the HW guys have different attitude compared to Tooling guys.

#### Branding and association with other brands

Oniro is strongly tied to OpenHarmony. We want to mitigate that association, but not to the point of losing the chance to have OpenHarmony compatibility.

Carlo: it's a bit of chicken and egg, if we don't start being attractive to other big players, the incumbency of OpenHarmony and Huawei is not going to be reduced. Needs an outside recognizable strategy followed by facts.

The main blockers for prospective is the strong link to OpenHarmony. Already associated internally to Huawei. They need and like the baseline, but they don't want to be associated.

Chiara: We need marketing action to change this perception. We should be pushing on communication this year, beside brand recognition.

It's imperative to change the webpage as soon as possible [to convey the right message].

Meeting adjourned at 18:10


#### Session 2

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

 * Davide Ricci - Huawei
 * Amit Kucheria - Committers
 * Adrian O'Sullivan - Huawei alternate
 * Carlo Piana - Array. Silver Members representative
 * Alberto Pianon - Array alternate


Steering Committee Regrets:


Other Attendees:

 * Luca Tavanti - Cubit
 * Luca Miotto - NOI
 * Patrick Ohnewein - NOI

[comment]: # (* Andrea Gallo - Linaro )
[comment]: # (* Antonio Miele - Politecnico Milano)
[comment]: # (* Francesco Amigoni - Politecnico Milano)
[comment]: # (* Gianluca Venere - SECO)
* Marta Rybczynska - Huawei
* Chiara Del Fabbro - Huawei
* Ettore Chimenti - SECO


Eclipse Foundation:

 * Agustin Benito Bethencourt

[comment]: # (* Paul Buck)
[comment]: # (* Sharon Corbbet)

## Minutes

Meeting restarts at 9:10 CEST

Daniel Izquierdo from Bitergia is invited to provide some analytics concerning Oniro, using their tools.


### Program Objectives

#### List current

A. We have completed the onboarding Oniro.

Switch to "Evolve Oniro into  a platform which supports multiple use cases"

B. (Increase membership) Leave with that, explain better. Have more interaction with adjacent project/ecosystems. Project to be leveraged by them.

C. Remains accurate. Look the verticals which give us more traction, quick. Remove the examples from this point, describe more in the tactics. Position Oniro as the all-scenarios platform that can be leveraged by targeted industries.

#### Commercial Adoption

A. "First oniro release": change to "Restructure Oniro to become a base production ready project that supports an all-scenario common platform". An Oniro profile must be Oniro-ready one in all.

B. Commercial Adoption: Enablement

C. Support the concept of broad oniro compatible spec to support vertical profiles such as OpenHarmony. Keep them slim, realistic. Separate horizontal from vertical specifications.

### KPIs and contributions

(here names are for the member raising the subject, not the statement)

#### Members

Davide: Need to preserve the Commons and reflect into the KPI. The way to keep the commons active: list key functions, staff and resource those functions that support it, get money to fund it. To avoid that a member monopolize the attention and becomes the sole "client" to the project.

Agustin: give a clear indication as to a minimum effort in the platform as opposed to the profile (ratio/absolute).

Davide: we should change the charter to define what amount of engineering effort (eg 2 engineers) must be contributed. See Linaro example. Of sufficient skill level (interview them).

Adrian: A percentage can be optimal, like "20% of the engineers time", whatever the effort is in aggregate. We need to make sure the promises are kept.

Agustin: We need to figure out internally, we will come up with a proposal after consulting with EF on what has been discussed above.

#### Members (number)

12 is still a good number. We decide 15 as a good compromise.

#### Blueprints

Scrap blueprints, put one blueprint per vertical profile. Two more profiles by the end of the year.

#### V3 release

Off-train profiles for verticals. We ask for one vertical profile releases. IoT the best candidate.

### Member Goals

#### contribution

20% of the effort in supporting the platform.

The blueprint per member is not relevant.

#### marketing

Increase the contribution from members in terms of content (video/text)

Workshops, each member must organize or support effectively.

Clarify the goals are for every member.


#### Nice-to-haves

("how you can help")

Keep it.


### Approval of the Oniro Program Plan 2023 candidate

Agustin suggests to take it offline, convene a WG SC meeting, collect consensus on a proposal on specific text in the intervening time.  

10 minutes recess.

Reconvened, additional items tabled for discussion.

Chiara, Carlo: we should discuss reshuffling the organization on account of new members and to raise efficiency.


### Resolutions

none taken

### AoB  (Open Floor)

Agustin addresses the SC and members thanking for their contribution and dedication in these complicated times.


## Links and references

### Links to the Oniro Program Plan 2023 draft

* .pdf version:
#link<https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/
program-plan-and-budget-oniro-wg/program_plan_2023_drafts/
Oniro_Program_Plan_2023_draft.pdf>
* .pptx version: #link
<https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/
program-plan-and-budget-oniro-wg/program_plan_2023_drafts/
Oniro_Program_Plan_2023_draft.pptx>
* .odt version: #link
<https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/
program-plan-and-budget-oniro-wg/program_plan_2023_drafts/
Oniro_Program_Plan_2023_draft.odp>
* All versions (folder):
* Oniro WG Program Plan2022: #link <https://gitlab.eclipse.org/eclipse-wg/
oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-
wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro_Program_Plan_2022.pdf>

### Links

This is the list of relevant links:
* Repository where you will find the agenda and meeting minutes to be approved
(private repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/
oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/
main/agenda-minutes-to-approve>
* Repository where the Oniro WG SC Minutes of Meetings (MoM) are published
once approved (public repository) #link
<https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc>
* Oniro WG Program Plan 2022: #link <https://gitlab.eclipse.org/eclipse-wg/
oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-
wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro_Program_Plan_2022.pdf>
* Oniro WG Budget 2022: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/
oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/
main/program-plan-and-budget-oniro-wg/Oniro-2022-Working-Group-Budget-
Summary.pdf>

This is the list to reference and other important notes to understand this
document:
* AoB: Any Other Business
* MoM: Minutes of Meeting
* WG: Working Group
* SC: Steering Committee
* EF: Eclipse Foundation
* OH: Open Harmony

Meeting closes at 10:05 UTC
