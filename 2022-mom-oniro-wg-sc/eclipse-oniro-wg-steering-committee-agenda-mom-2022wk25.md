# Oniro Working Group Steering Committee Launch Meeting 2022wk25

MoM Status: approved

(approved on  2022-07-07 at db5cfbee)

* Date: 2022-06-23
* Time: from 13:02 to 14:10 UTC
   * Oniro WG SC meeting: from 13:02 to 13:38 UTC
   * Oniro WG SC All Members meeting: from 13:38 to 14:10 UTC
* Chair: Davide Ricci
* Minutes taker: Carlo Piana
* Join via Zoom
   * #link <https://eclipse.zoom.us/j/86710449041?pwd=dmk1OW56emQvU1YvSjBUY1AyOHIzUT09>
   * Meeting ID: 867 1044 9041
   * Passcode: 293617

## Agenda

Oniro WG SC meeting
* Approval of the meeting minutes from the 2002-05-26  Oniro WG SC Meeting - Davide R. 5 minutes
* Goofy roadmap rescoping for beta based on expected team capacity - Amit 10 minutes
* Updates from Eclipse on the IP review for Oniro for December release - Agustin 5 minutes
* Resolutions - 5 minutes 
* AoB (Open Floor) - 20 minutes

Oniro WG SC All Members meeting

* Introduction of Cubit Innovation Labs - Agustin 10 minutes
* AoB (Open Floor) - 5 minutes


## Attendees

### Oniro WG SC Meeting

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

* Davide Ricci - Huawei
* Carlo Piana - Arrayfrt
* Amit Kucheria - Committers


Steering Committee Regrets:



Other Attendees:

* Adrian O'Sullivan - Huawei alternate
* Alberto Pianon - Array alternate

Eclipse Foundation:

* Agustin Benito Bethencourt
* Paul Buck
* Sharon Corbett (until 13:36 UTC)


### Oniro WG SC All Members Meeting

Beyond the above, the participants in this section were:

[comment]: # (* Andrea Basso - Synesthesia)
[comment]: # (* Gianluca Venere - SECO)
[comment]: # (* Andrea Gallo - Linaro)


## Minutes

### Oniro WG SC meeting minutes

#### Approval of the meeting minutes from the 2022-05-26 Oniro WG SC Meeting

* Agenda and meeting minutes to be approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/agenda-minutes-to-approve/eclipse-oniro-wg-steering-committee-agenda-mom-2022wk21.md>

Approved at commit 65f2e4f3 


[comment]: # (Commit considered: xxxxxx)

[comment]: # (No objections, approved.)


#### Goofy roadmap rescoping for beta based on expected team capacity

Amit presents a proposal for dropping features and rescoping the release due to lack of capacity, for OS, reference hardware, testing and infrastructure, reference blueprints.

Q are we dropping FreeRTOS altogether (Alberto)
A Yes, we don't have "customers" asking for it.



#### Updates from Eclipse on the IP review for Oniro for December release

Agustin: reports there is not much for the Alpha, since there is no distribution of binary.

For the final release, we don't have the first output for the licenses review. We need to wait for it, it's undergoing. 

Agustin is opening a ticket with EMO and with further information we'll shortly know when we need to freeze the code.

Carlo reports Array has met with Orcro explaining what is not already transparent from the documentation, showing some of the tools and explaining some of the decisions taken and assumptions made, with good feedback from Orcro.

Amit hoped that the IP review was done already for alpha so that we can concentrate on the final release with the IP already in good order as much as possible. Suggests that we "freeze" (create a minor milestone) the code and perform the further analysis only on the incremental delta. Hoping that EF completes the review within September, for the final feature freeze.  

Agustin thinks that since the process is already going on, creating an interim milestone would not be a big issue.

Davide suggests to table the topic for the next SC meeting. Agustin will talk to the IP 

#### Resolutions

- To approve the minutes for Week 21 


#### AoB  (Open Floor)

None tabled

### Oniro WG SC All Members Meeting minutes

- Luca Favaretto (Kalpa)
- Luca Tavanti (Cubit)

Joined and are welcomed.

#### Introduction of Cubit Innovation Labs

Cubit Innovation labs' Luca Tavanti, Research and Innovation Projects, introduces the company.


#### AoB  (Open Floor)

Luca Favaretto shortly introduce the Test plan, plus questions on completeness and scope. 

Link to test plan #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/kalpa-report-Software-Test-Plan_v.2.1.pdf>

## Links and references

### Links

This is the list of relevant links:
* Repository where you will find the agenda and meeting minutes to be approved (private repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve>
* Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/>
* Oniro WG Program Plan: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro_Program_Plan_2022.pdf>
* Oniro WG Budget: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro-2022-Working-Group-Budget-Summary.pdf>

This is the list to reference and other important notes to understand this document:
* AoB: Any Other Business
* MoM: Minutes of Meeting
* WG: Working Group
* SC: Steering Committee
* EF: Eclipse Foundation
* OH: Open Harmony
