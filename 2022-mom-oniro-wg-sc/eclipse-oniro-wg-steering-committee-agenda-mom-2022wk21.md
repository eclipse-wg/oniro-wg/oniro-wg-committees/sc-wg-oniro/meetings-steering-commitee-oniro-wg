# Oniro Working Group Steering Committee Launch Meeting 2022wk21

MoM Status: approved on 2022-06-23 (Commit 65f2e4f3)

* Date: 2022-05-26
* Time: from 15:02 to 16:00 CEST
   * Oniro WG SC meeting: from 15:02 to 15:33 CEST
   * Oniro WG SC All Members meeting: from 15:33 to 16:00 CEST
* Chair: Davide Ricci
* Minutes taker: Carlo Piana
* Join via Zoom
   * #link <https://eclipse.zoom.us/j/86710449041?pwd=dmk1OW56emQvU1YvSjBUY1AyOHIzUT09>
   * Meeting ID: 867 1044 9041
   * Passcode: 293617

## Agenda

Oniro WG SC meeting
* Approval of the meeting minutes from the 2022-05-12 Oniro WG SC Meeting - Davide R. 5 minutes
* Agenda: recurring topics - Davide R. 10 min
* IT migration plan. - Agustin 10 minutes
* AoB (Open Floor) - 5 minutes

Oniro WG SC All Members meeting

* Progress against Program Plan 2022 (recurrent) - Davide 10 min
* Oniro Platform Release Roadmap highlights - 10 min
* EclipseCon 2022 - Agustin. 5 minutes
* AoB (Open Floor) - 5 minutes

## Attendees

### Oniro WG SC Meeting

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

* Davide Ricci - Huawei
* Carlo Piana - Array


Steering Committee Regrets:

* Amit Kucheria - Committers

Other Attendees:

* Adrian O'Sullivan - Huawei alternate 

Eclipse Foundation:

* Agustin Benito Bethencourt
* Paul Buck
* Sharon Corbett 


### Oniro WG SC All Members Meeting

Beyond the above, the participants in this section were:

[comment]: # (* Andrea Basso - Synesthesia)
* Gianluca Venere - SECO
* Andrea Gallo - Linaro


## Minutes

### Oniro WG SC meeting minutes

#### Approval of the meeting minutes from the 2022-05-12 Oniro WG SC Meeting

* Agenda and meeting minutes to be approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/agenda-minutes-to-approve/eclipse-oniro-wg-steering-committee-agenda-mom-2022wk19.md>

Commit considered: 34c4e278

No objections, approved.


#### Agenda: recurring topics

* Evaluation of our progress against the Program Plan.
* Contingencies
* Oniro Platform Release Roadmap initiatives approvals
   * Assessment: monthly
   * Approval: bi-monthly
* QPR: quarterly
   * All committees together for an hour: a analysis and reporting and discussion. Open to anyone.
   * Quarterly Program review instead of Bbusiness?


Agustin outlines the proposal and its pace. The proposal is slightly adjusted to have a more natural flow. A tentative version is placed at #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/cf14d52f71d3fcf4292c08697dd756073518c84f/resources_documentation_presentations/Oniro-meetings-timeline.pdf> 

In a couple of weeks Agustin will come up with a final proposal. 


### IT migration plan

EF has increased capacity and it's time to proceed with migration from Huawei to EF. 
Priorities need to be established.
Document is planned to be socialized next week.
On the compliance toolchain Carlo raises the issue that the expertise is uncanny and needs to be well planned and urges a meeting to verify how much.


#### Resolutions

- Last meeting minutes, commit # 34c4e278, are approved.


#### AoB  (Open Floor)

None tabled

### Oniro WG SC All Members Meeting minutes

Opens at 15.33

#### Progress against Program Plan 2022 (recurrent) 

There is now sponsorship agreement from Huawei and EF is going to be main sponsor of SFScon, rather than Huawei.

#### Oniro Platform Release Roadmap highlights 

Davide outlines. Alpha on June 6, we'll have a view over what we are able to achieve, prioritize and deprioritize features. 

The main issue coming form Meta-harmony. The main job is consolidating the heterogeneous environment that OH is operating on, better said than done. Currently it should work on any board on any version of the kernels.

#### EclipseCon 2022 - Agustin. 5 minutes

The Wiki starts to be populated with proposals already submitted and on their way. Call For Papers is not going very well, only one submission by far to meet the early bird. After the chances to be approved are less. We urge to submit more talks ASAP.

Quick run through of the other organizational points.

#### AoB  (Open Floor)

None tabled

## Links and references

### Links

This is the list of relevant links:
* Repository where you will find the agenda and meeting minutes to be approved (private repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve>
* Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/>
* Oniro WG Program Plan: WIP
* Oniro WG Budget: WIP

This is the list to reference and other important notes to understand this document:
* AoB: Any Other Business
* MoM: Minutes of Meeting
* WG: Working Group
* SC: Steering Committee
* EF: Eclipse Foundation
* OH: Open Harmony
