# Oniro Working Group Steering Committee Meeting 2022wk41

MoM Status: approved on 2022-11-17 commit 30ef91bf

* Date: 2022-10-13
* Time: from 13:00 to 14:00 UTC
   * Oniro WG SC meeting: from 13:01 to 13:30 UTC
   * Oniro WG SC All Members meeting: from 13:31 to 14:00 UTC
* Chair: Davide Ricci
* Minutes taker: Carlo Piana
* Join via Zoom
   * #link <https://eclipse.zoom.us/j/86710449041?pwd=dmk1OW56emQvU1YvSjBUY1AyOHIzUT09>
   * Meeting ID: 867 1044 9041
   * Passcode: 293617

## Agenda

Oniro WG SC meeting

* Approval of the meeting minutes from the 2022-09-29 2022wk39 Oniro WG SC Meeting - Davide R. 5 minutes
* Program Plan 2023 discussion - Davide R. 15 minutes 
* Resolutions - Davide Ricci 5 minutes 
* AoB - 5 minutes

Oniro WG SC All Members meeting

* Welcome Politecnico Milano to Oniro - Agustin B.B. 5 minutes
* Program Plan 2023 discussion - Davide R. 15 minutes
   * Conclusions of the brainsotrming session. - Agustin 3 minutes
   * Description of the Program Plan 2023 draft - Agustin 3 minutes
   * New ideas - Davide R. 9 minutes
* EclipseCn 2022 meeting series to approve the Oniro Program PLan 2023 candidate. - Agustin 5 Minutes
* AoB - 5 minutes

## Attendees

All Member representatives have been invited to the full meeting. No private session.

### Oniro WG SC Meeting

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

* Davide Ricci - Huawei
* Adrian O'Sullivan - Huawei alternate
* Alberto Pianon - Array alternate

There is quorum.

Steering Committee Regrets:

* Amit Kucheria - Committers
* Carlo Piana - Array. Silver Members representative

Other Attendees:

Eclipse Foundation:

* Agustin Benito Bethencourt
* Paul Buck during the second part of the meeting

[comment]: # (* Sharon Corbbet)
* Gael Blondelle first part of the meeting only
* Luca Tavanti - Cubit  - second part of the meeting
* Luca Miotto - NOI - second part of the meeting

[comment]: # (* Patrick Ohnewein - NOI)
[comment]: # (* Andrea Gallo - Linaro )
[comment]: # (* Antonio Miele - Politecnico Milano)
[comment]: # (* Francesco Amigoni - Politecnico Milano)
[comment]: # (* Gianluca Venere - SECO)
 
## Minutes

### Oniro WG SC meeting minutes

#### Approval of the meeting minutes from the 2022-09-29 2022wk39 Oniro WG SC Meeting

* Agenda and meeting minutes to be approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/agenda-minutes-to-approve/2022wk39-eclipse-oniro-wg-steering-committee-agenda-mom.md>

Approved unanimously.

Commit considered: 27e1bff66595d78e8288df0843a0d99b2f42e47d

#### Program Plan 2023 discussion

These are the links where you can find the Oniro Program PLan draft:
* In .pptx format: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program_plan_2023_drafts/Oniro_Program_Plan_2023_draft.pptx>
* In .odt format: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program_plan_2023_drafts/Oniro_Program_Plan_2023_draft.odp>
* In .pdf format #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program_plan_2023_drafts/Oniro_Program_Plan_2023_draft.pdf>

* Agustin described the Brainstorming session. More input from people that did not participated needed.
* Adrian suggest to use the Program Plan 2022 as input too. The plan needs to be detailed enough so EF can create a budget.
* Action point on Davide to try to come to EclipseCon 2022 with a draft of the Program Plan.

#### Resolutions

* MoM 022-09-29 2022wk39 Oniro WG SC Meeting approved 

#### AoB

Release:
* Alberto P.: Confirmed the decision of dropping the release of binaries on this Oniro v2.0. The release will be basically yocto layers and source code. Still, from the compliance and security perspective, some effort will be done for those willing to create and distribute binaries.
   * Description of the work that will be done from the security and compliance perspective.
   * For compliance, there will be a reference SBOM, complementary information coming from FOSSology and a legal notice for those who want to create and distribute binaries (products).
* We can attack the topic of releasing binaries in the near future.
* Who is our target audience? The the creation and distribution of the binaries needs to have in consideration the answer to this question.
* Alberto points out that we should provide images as references only in any case.

Restructuring (license change)
* The process have finished. Now devs are good to upstream the code to the Yocto Project under MIT license.

### Oniro WG SC All Members Meeting minutes

#### Welcome Politecnico Milano to Oniro

Not present. Will move for the next meeting.

#### Program Plan 2023 discussion

These are the links where you can find the Oniro Program PLan draft:
* In .pptx format: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program_plan_2023_drafts/Oniro_Program_Plan_2023_draft.pptx>
* In .odt format: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program_plan_2023_drafts/Oniro_Program_Plan_2023_draft.odp>
* In .pdf format #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program_plan_2023_drafts/Oniro_Program_Plan_2023_draft.pdf>

##### Conclusions of the brainsotrming session

* Brainstorming session summary #link <https://www.eclipse.org/lists/oniro-wg/msg00282.html>
* Agustin describes the session and some of the results.
* Adrian proposes to use the Oniro Program PLan as input and match the commitments there with the new ideas.

##### Description of the Program Plan 2023 draft

* Davide summarises what was discussed in the first part of the meeting.

##### New ideas

* Luca Tavanti proposes the following points to consider for the Oniro Program Plan 2023
   * agrees with the high priority point (Value prop.)
   * Increase the number of members has a dependency on the current state of the website, that should be improved.
   * Cubit went over the documentation and believe that there is room for improvement for somebody who want to start bring Oniro to different boardss, using the supported ones as examples.

#### EclipseCn 2022 meeting series to approve the Oniro Program Plan 2023 candidate.

* Agustin explains what is confirmed for EclipseCon 2022 when it comes to the schedule
   * He shows [where anyone can access](https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/Events/EclipseCon#general-information) to the Oniro at EclipseCon 2022 calendar.

#### AoB  (Open Floor)

* Paul Buck ask for the release of binaries.
   * Davide explain the conclusions pointed in the first part of the meeting.
* Alberto: will the community day be recorded? No. The talks will be.

## Links and references

### Links

This is the list of relevant links:
* Repository where you will find the agenda and meeting minutes to be approved (private repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve>
* Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc>
* Oniro WG Program Plan: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro_Program_Plan_2022.pdf>
* Oniro WG Budget: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro-2022-Working-Group-Budget-Summary.pdf>

This is the list to reference and other important notes to understand this document:
* AoB: Any Other Business
* MoM: Minutes of Meeting
* WG: Working Group
* SC: Steering Committee
* EF: Eclipse Foundation
* OH: Open Harmony

Meeting closes at 13:53 UTC

All times UTC unless expressly mentioned
