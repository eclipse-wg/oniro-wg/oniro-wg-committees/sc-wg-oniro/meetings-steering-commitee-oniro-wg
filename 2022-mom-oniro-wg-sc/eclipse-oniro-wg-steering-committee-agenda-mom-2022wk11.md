# Oniro Working Group Steering Committee Meeting 2022wk11

Approved by the Oniro WG SC on 2022-03-31

* Date: 2022-03-17
* Time: from 15:00 to 16:00 CET
* Chair: Davide Ricci
* Minutes taker: Carlo Piana
* Join via Zoom
   * #link <https://eclipse.zoom.us/j/86710449041?pwd=dmk1OW56emQvU1YvSjBUY1AyOHIzUT09> 
   * Meeting ID: 867 1044 9041
   * Passcode: 293617

## Agenda

* Approval of the meeting minutes from the 2022-03-03 Oniro WG SC Meeting - Davide R. 5 minutes
* Ratify the Prospects Pipeline Management Process - Agustin 10 minutes
* Roadmap process overview - Agustin 15 minutes
* Program Plan - Davide R. 10 min
* Oniro Goofy Roadmap (draft) - Davide R. 10 minutes 
* Resolutions
* AoB (Open Floor) - 5 minutes

## Attendees

Steering Committee Members (Quorum =  2 of 3 for this Meeting):
* Davide Ricci (Huawei)
* Carlo Piana (Array)
* Amit Kucheria (PMC)

Steering Committee Regrets:
* none


Other Attendees:


Eclipse Foundation:
* Paul Buck
* Agustin Benito Bethencourt

## Minutes 

### Approval of the meeting minutes from the 2022-03-03 Oniro WG SC Meeting

* Agenda and meeting minutes to be approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/agenda-minutes-to-approve/eclipse-oniro-wg-steering-committee-agenda-mom-2022wk9.md>

* Agenda approved unanimously.


### Ratify the Prospects Pipeline Management Process

* #Link to the Prospects Pipeline Management Process description <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/pipeline-oniro/-/wikis/home> 
* #link to the different boards used to report about progress and status <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/pipeline-oniro/-/wikis/home#reports>

* Presentation of the content by Agustin
* Agreement on allowing Oniro WG SC representatives to select people from a different Oniro Member to the Oniro Prospect Pipeline Management team.
* Ratified unanimously.

### Roadmap process overview

* #link to the diagram that summarises the relation between specs, initiatives and a release <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/agenda-minutes-to-approve/oniro-release-schema.pdf>
* #link Infographic that summarises the process (draft) as well as relevant links <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/agenda-minutes-to-approve/Oniro-roadmapping-process-infography.pdf>
* #link Roadmapping process description (draft) <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/roadmap-oniro-wg/wishlist-roadmap/-/wikis/home>

* Summary done by Agustin.
* Next week the proposal will be socialised. Next week is about getting input.
* The following week will be about agreements.
* #task Agustin will bring to the Oniro WG SC next session the agreements and the points of controversy for consideration.
* The process needs to be put up to test for validity. We need to be flexible based on practice.
* Other company representatives that put capacity will be invited to the next opportunity this topic is presented to the Oniro WG SC. 

### Program Plan.

* Agustin apologised for not bringing the initial draft. #task Point for next meeting.

### Oniro Goofy Roadmap (draft) 

* #link to the current epics <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/roadmap-oniro-wg/engineering-roadmap/-/epic_boards/59>

* Amit goes over the planning being done by him and the team. More effort will be put on it. 

### Resolutions 

* Agenda from previous meeting 2022wk09 approved unanimously.
* Oniro prospect pipeline management process ratified unanimously.


### AoB  (Open Floor)

* Amit will submit two more projects: blueprints and IP compliance toolchain

## Links and references

* Repository where you will find the agenda and meeting minutes to be approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve>
* Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/>

* AoB: Any Other Business
* MoM: Minutes of Meeting
