# Oniro Working Group Steering Committee Launch Meeting 2022wk19

MoM Status: approved on 2022-05-26 commit 41d1c4f7

[comment]: # (approved on YYYY-MM-DD)

* Date: 2022-05-12
* Time: from 15:00 to 16:00 CEST
   * Oniro WG SC meeting: from 15:00 to 15:30 CEST
   * Oniro WG SC All Members meeting: from 15:30 to 16:00 CEST
* Chair: Davide Ricci
* Minutes taker: Carlo Piana
* Join via Zoom
   * #link <https://eclipse.zoom.us/j/86710449041?pwd=dmk1OW56emQvU1YvSjBUY1AyOHIzUT09>
   * Meeting ID: 867 1044 9041
   * Passcode: 293617

## Agenda

Oniro WG SC meeting
* Approval of the meeting minutes from the 2022-04-28 Oniro WG SC Meeting - Davide R. 5 minutes
* Program Plan 2022 consideration and approval - Davide R. 5 min
* Budget 2022 consideration - Sharon C. 5 minutes
* Oniro Release Roadmap Process Approval  - 5 minutes
* Oniro Release Roadmap Team Members candidates appointment and approval - 5 minutes
* AoB (Restricted) - 5 minutes

Oniro WG SC All Members meeting

* Program Plan update - Davide R. 10 minutes
* Release Roadmap Process update - Agustin 5 minutes
* Goofy Status - 10 minutes
* AoB (Open Floor) - 5 minutes

## Attendees

### Oniro WG SC Meeting

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

* Davide Ricci - Huawei
* Carlo Piana - Array
* Amit Kucheria - Committers

Steering Committee Regrets:


Other Attendees:

[comment]: # (* Adrian O'Sullivan - Huawei alternate )

Eclipse Foundation:

* Agustin Benito Bethencourt (EF)
* Paul Buck (EF) - excused himself from the All Members
* Sharon Corbett (EF) - excused herself from the All Members


### Oniro WG SC All Members Meeting

Beyond the above, the participants in this section were:

[comment]: # (* Andrea Basso - Synesthesia)
* Gianluca Venere - SECO
* Yves Maître - EF


## Minutes

### Oniro WG SC meeting minutes

#### Approval of the meeting minutes from the 2022-04-28 Oniro WG SC Meeting

* Agenda and meeting minutes to be approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/agenda-minutes-to-approve/eclipse-oniro-wg-steering-committee-agenda-mom-2022wk17.md>

No objection is recorded, minutes approved.

#### Program Plan 2022 consideration and approval

During the previous Oniro WG SC Session the Oniro Program Plan candidate was approved.


* Please find the Program Plan candidate in .pptx , .odp and .pdf versions in this folder of the repository: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/program-plan-and-budget-oniro-wg>

Since then, different improvements has been introduced as well as format improvements. They are all included in the Oniro_Program_Plan_2022_draft_20220505 presentation, where you can find in the same folder than the candidate.

#link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/program-plan-and-budget-oniro-wg>

Please review the changes. The Oniro_Program_Plan_2022_draft_20220505 is the version that is proposed for approval.

* Agustin provides a quick run-through to the few changes described above
* Execution will run in 2023 (Sharon asked)
* Everyone has had sufficient time to evaluate, so we are moving to vote
* All approve
* The Program will be finally formatted, presented and approved by Executive Director and the SC will have the opportunity to verify the final commit.


### Budget 2022 consideration

The Oniro 2022 Budget was sent to the oniro-wg-steering-committee mailing list on 2022-04-29. The attached file can be found in gitlab: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/program-plan-budget-templates/oniro-working-group-draft-2022-budget-presentation.pdf>

The request is to confirm this budget as Support for the Program Plan 2022.

* Sharon summarizes what has already been presented
* A resolution to adopt it is presented and unanimously approved

#### Oniro Release Roadmap Process Approval

* The Oniro Release Roadmap Process is in the following #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/roadmap-oniro-wg/wishlist-roadmap/-/wikis/Home/R-R-process?version_id=b3a6bee199be9ac8e14fa1ae99a91f23417f8f96>
* The current version (commit) is b3a6bee1:
   * Check the history of the wiki page #link: <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/roadmap-oniro-wg/wishlist-roadmap/-/wikis/Home/R-R-process/history>
   * Check the specific version proposed for approval #link: <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/roadmap-oniro-wg/wishlist-roadmap/-/wikis/Home/R-R-process?version_id=b3a6bee199be9ac8e14fa1ae99a91f23417f8f96>

It is requested for the Oniro WG SC to approve the Oniro Release Roadmap Process commit b3a6bee1

* A resolution to approve the Roadmap is presented and unanimously approved

#### Oniro Release Roadmap Team Members appointment

[comment]: #  Candidates to be appointed by the Oniro WG SC:
[comment]: #  * xxxx from xxxx
[comment]: #  * xxxx from xxxx

[comment]: #  Candidates appointed by the Oniro WC SC:
[comment]: #  * xxx from xxxx
[comment]: #  * xxx from xxx

As per the approved Oniro Release Roadmap Process, the number of Members appointed by the Oniro WG SC is 3 (maximum). 

* Amit candidates himself
* Davide candidates himself

* No objections is raised, appointment is approved


#### Resolutions

* Approve the minutes (41d1c4f7)
* Approve the Program Plan 
* Adopt the Budget
* Approve the Release Roadmap Process
* Appoint Amit and Davide as part of the Roadmap Team

#### AoB  (restricted meeting)

* Short discussion on what happens in case of absence of one member for vacation.


### Oniro WG SC All Members Meeting minutes

Opens at 15:36

#### Program Plan approved

* Please find the Program Plan draft in .pptx , .odp and .pdf versions in this folder of the repository: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/program-plan-and-budget-oniro-wg>


#### AoB  (Open Floor)

* Agustin mentions that as soon ED approves the budget, it is going to be sent over to the members of the WG.

## Links and references

### Links

This is the list of relevant links:
* Repository where you will find the agenda and meeting minutes to be approved (private repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve>
* Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/>
* Oniro WG Program Plan: WIP
* Oniro WG Budget: WIP

This is the list to reference and other important notes to understand this document:
* AoB: Any Other Business
* MoM: Minutes of Meeting
* WG: Working Group
* SC: Steering Committee
