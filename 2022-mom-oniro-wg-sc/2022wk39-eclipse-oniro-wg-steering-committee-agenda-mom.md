# Oniro Working Group Steering Committee Meeting 2022wk39

MoM Status: approved on 2022-10-13 commit 27e1bff66595d78e8288df0843a0d99b2f42e47d

* Date: 2022-09-29
* Time: from 13:00 to 14:00 UTC
   * Oniro WG SC meeting: from 13:01 to 13:30 UTC
   * Oniro WG SC All Members meeting: from 13:31 to 14:00 UTC
* Chair: Davide Ricci
* Minutes taker: Carlo Piana
* Join via Zoom
   * #link <https://eclipse.zoom.us/j/86710449041?pwd=dmk1OW56emQvU1YvSjBUY1AyOHIzUT09>
   * Meeting ID: 867 1044 9041
   * Passcode: 293617

## Agenda

Oniro WG SC meeting

* Approval of the meeting minutes from the 2022-09-01 2022wk35 Oniro WG SC Meeting - Davide R. 5 minutes
* Support the decision of the PMC of changing the license to some of the Oniro components - Amit 5 minutes
* Resolutions - Davide Ricci 5 minutes 

Oniro WG SC All Members meeting

* Welcome Politecnico Milano to Oniro - Agustin B.B. 5 minutes
* 2022 & 2023 Huawei R&D involvement / realistic capacity - Davide Ricci 10 minutes
* Lessons learned: Oniro minimum viable core team and risk mitigation strategies - Davide Ricci 10 minutes
* Oniro v2.0 roadmap plan and alignment - Davide Ricci 10 minutes
* 2023 KPIs initial brainstorm - Davide Ricci 5 minutes
* AoB - 5 minutes

## Attendees

All Member representatives have been invited to the full meeting. No private session.

### Oniro WG SC Meeting

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

Davide Ricci - Huawei
Amit Kucheria - Committers
Adrian O'Sullivan - Huawei alternate
Carlo Piana - Array. Silver Members representative
Alberto Pianon - Array alternate


Steering Committee Regrets:


Other Attendees:

Eclipse Foundation:

* Agustin Benito Bethencourt
* Paul Buck
* Sharon Corbbet

* Luca Tavanti - Cubit
* Luca Miotto - NOI 
* Patrick Ohnewein - NOI
* Andrea Gallo - Linaro )
* Antonio Miele (Politecnico Milano)
* Francesco Amigoni (Politecnico Milano)


 
## Minutes

### Oniro WG SC meeting minutes

#### Approval of the meeting minutes from the 2022-09-01 2022wk35 Oniro WG SC Meeting

* Agenda and meeting minutes to be approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/agenda-minutes-to-approve/2022wk35-eclipse-oniro-wg-steering-committee-agenda-mom.md>

Commit considered: 897cea19

No objections recorded, minutes are approved

#### Support the decision of the PMC of changing the license to some of the Oniro components 

The Oniro repository (https://gitlab.eclipse.org/eclipse/oniro-core/oniro) currently uses 3 licenses for the included files:
* Apache-2.0 (Yocto/OE layers files)
* CC-BY-4.0 (documentation)
* MIT.txt (Yocto/OE layers files)

Due to how these Yocto/OE layers interact with upstream layers (including oe-core), having parts of our layers licensed under Apache when most (if not all) of our upstream dependencies (and generally the entire ecosystem) are MIT, would pose a couple of issues:
* Using our layers would introduce a new license to a downstream consumer that needs to be assessed.
* Reusing parts of our code would pose an issue of relicensing on Oniro or the downstream side.
* Upstreaming Apache code would need relicensing on the Oniro side before upstream could accept contributions (unless upstream is up for a license addition which in most cases is not an option).
* Mistakes can happen and end up with "silent" relicensing in different situations.

To avoid all the above we propose to change all the existing Yocto/OE Apache-2.0 files in this repository to MIT. The list of the Apache-2.0 files in the scope of this relicensing is: #link <https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/issues/824#note_1020152>  Please check also at the end of this document the full list of affected files.

Amit K. proposes a vote to support this decision so both, projects and working group are aligned. 

Sharon asks wheter all the SC membersa and copyright holders have been consulted and approve the change.

Davide and Agustin report that PMC has already approved the change and the committers have had the opportunity.

Amit, Carlo, Stefano, acknowledge and have no further recommendation.


#### Resolutions

* Approved unanimously the 2022wk35 MoM 
* The PMC decision of relicensing the list is acknowledged

### Oniro WG SC All Members Meeting minutes


#### Welcome Politecnico Milano to Oniro

Andrea Miele provide a quick introduction.


#### 2023 Huawei R&D involvement / realistic capacity

Davide reports on the reduced budget/technical capacity of Huawei owing to a company-wide reduction of certain activities.

This brings an urge to speed up the 2.0 release. 

After mid-Octobr, there will be still some technical capacity (overall 4-12 including other partners). Asks what would Oniro be like next year? 

Carlo asks if the economic capacity will be unaffected (budged contribution) by the decision. Strategic level.  But other non-budgetary contributions shall be taken care by the WG at large, by sponsorship and new members fees.

Sharon: Program plan - EF budgets according to the budget approved. Davide suspects that as far as activities are concerned, there will be a shortage. 


#### Lessons learned: Oniro minimum viable core team and risk mitigation strategies

Is relying on one single member a wise decision (budget, technical, IP compliance).

Makers leveraging a project for making their products should support and share the burdens of the project to make it viable in the long run, at least after some time after bootstrap.

Strategic assets/activities that need to be shared and kept alive:

- IP 
- IT & Infrastructure / Gitlab runners
- Marketing
- Technical oversight, technical 
- Maintainig OS images, bugfixing platforms, testing.

This had the input of the members.

#### Oniro v2.0 roadmap plan and alignment

Product documentation:
* Current Oniro Platform Release Roadmap #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/resources_documentation_presentations/oniro_platform_release_roadmap_alpha_beta_20220708-share.pdf>
* Proposed Oniro Platform Release Roadmap (draft) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/resources_documentation_presentations/oniro_platform_release_roadmap_draft_20220922.pdf>
* Oniro v2.0 detailed roadmap: #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/products-services-oniro-wg/release-roadmap-oniro-wg/-/epic_boards/59>

The different versions of the Oniro Platform Release Roadmap are stored in this folder: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/resources_documentation_presentations>

Oniro v2.0 release checklist (WIP): #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/release-roadmap-oniro-wg/initiative-qualification-roadmap-oniro-wg/-/issues/23>

Release early so that the feedback of the EF release team lands when technical capacity is still with us.

Amit presents.

Davide suggests that for the next release the single platforms are split in subprojects, so that Oniro per se concentrates on a smaller project and allows more frequent release of updates, IP  validation and etc, minimizing shared costs when some features are belonging to a single member.


#### 2023 KPIs initial brainstorm - Davide Ricci 10 minutes

These are the links where you can find the Oniro Program PLan draft:
* In .pptx format: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program_plan_2023_drafts/Oniro_Program_Plan_2023_draft.pptx>
* In .odt format: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program_plan_2023_drafts/Oniro_Program_Plan_2023_draft.odp>
* In .pdf format #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program_plan_2023_drafts/Oniro_Program_Plan_2023_draft.pdf>

All the Oniro Program PLan drafts and templates are stored in Gitlab: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/program-plan-and-budget-oniro-wg/program_plan_2023_drafts>

Due to shortage of time, we punt it to offline discussion.

How: offline via MR or braninstorming meeting next week 

#### AoB  (Open Floor)

None

## Links and references

### references

List of files affected by the relicensing decision (second point of the agenda):

```
meta-oniro-staging/conf/layer.conf:# SPDX-License-Identifier: Apache-2.0
meta-oniro-staging/recipes-graphics/libqrcodegen/libqrcodegencpp_1.7.0.bb:# SPDX-License-Identifier: Apache-2.0
meta-oniro-staging/README.md:# SPDX-License-Identifier: Apache-2.0
meta-oniro-staging/recipes-devtools/binutils/binutils_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-staging/recipes-devtools/go/go_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-staging/recipes-devtools/ninja/ninja_1.10.2.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-staging/recipes-connectivity/tayga/tayga_0.9.2.bb:# SPDX-License-Identifier: Apache-2.0
meta-oniro-staging/recipes-connectivity/tayga/files/tayga.service:# SPDX-License-Identifier: Apache-2.0
meta-oniro-staging/recipes-connectivity/zenoh/zenoh-c.inc:# SPDX-License-Identifier: Apache-2.0
meta-oniro-staging/recipes-connectivity/zenoh/zenohd_git.bb:# SPDX-License-Identifier: Apache-2.0
meta-oniro-staging/recipes-connectivity/zenoh/files/zenohc.pc:# SPDX-License-Identifier: Apache-2.0
meta-oniro-staging/recipes-connectivity/zenoh/zenoh-c_git.bb:# SPDX-License-Identifier: Apache-2.0
meta-oniro-staging/recipes-connectivity/zenoh/zenohd.inc:# SPDX-License-Identifier: Apache-2.0
meta-oniro-staging/recipes-connectivity/jool/jool-kmod_git.bb:# SPDX-License-Identifier: Apache-2.0
meta-oniro-staging/recipes-connectivity/jool/jool_git.bb:# SPDX-License-Identifier: Apache-2.0
meta-oniro-staging/recipes-connectivity/jool/jool.inc:# SPDX-License-Identifier: Apache-2.0
meta-oniro-staging/recipes-connectivity/bind/bind/bind-libunwind-header.patch:# SPDX-License-Identifier: Apache-2.0
meta-oniro-staging/recipes-connectivity/bind/bind_9.18%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-staging/recipes-connectivity/libuv/libuv_1.44.2.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-staging/dynamic-layers/meta-networking/recipes-connectivity/networkmanager/networkmanager_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-staging/dynamic-layers/meta-java/recipes-core/openjdk/openjdk-7_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-staging/dynamic-layers/meta-arm/recipes-bsp/uefi/edk2-firmware_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-staging/classes/ninja.bbclass:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-bsp/bootfiles/rpi-cmdline.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-bsp/grub/grub_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-bsp/grub/oniro-grub-bootconf.bb:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-bsp/grub/files/grub.cfg:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-bsp/grub/grub-efi_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/conf/distro/oniro-freertos.conf:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/conf/distro/include/oniro.inc:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/conf/distro/include/oniro-packageconfig.inc:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/conf/distro/include/oniro-wic.inc:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/conf/distro/oniro-linux.conf:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/conf/distro/oniro-zephyr.conf:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/conf/machine/include/qemu-efi.inc:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/conf/machine/qemuarm64-efi.conf:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/conf/machine/qemuarm-efi.conf:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/conf/layer.conf:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-gnome/gtk+/gtk+3_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/dropbear/dropbear_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/packagegroups/packagegroup-containers.bb:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/packagegroups/packagegroup-oniro-core.bb:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/packagegroups/packagegroup-connectivity.bb:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/packagegroups/packagegroup-oniro-tests.bb:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/rauc/rauc_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/rauc/insecure-keys/key.pem.license:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/rauc/insecure-keys/cert.pem.license:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/rauc/insecure-keys/gen-keys.sh:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/rauc/files/system.conf.in:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/rauc/files/rauc-state-dir.conf:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/ncurses/ncurses_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/util-linux/util-linux_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/sysota/files/sysotad.conf.in:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/sysota/sysota_git.bb:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/images/oniro-image-base-tests.bb:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/images/oniro-image-base.bb:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/images/oniro-image-base-dev.bb:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/images/oniro-image-extra.bb:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/images/oniro-image-common.inc:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/images/oniro-image-extra-tests.bb:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/images/oniro-image-extra-dev.bb:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/oniro-modprobe/oniro-modprobe.bb:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/oniro-modprobe/oniro-modprobe/oniro-kernel-mod.conf:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/oniro-sysctl/oniro-sysctl.bb:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/oniro-sysctl/oniro-sysctl/10-oniro-net-ipv4.conf:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/oniro-sysctl/oniro-sysctl/10-oniro-general.conf:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/oniro-sysctl/oniro-sysctl/10-oniro-net-ipv6.conf:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/oniro-sysctl/oniro-sysctl/10-oniro-bpf.conf:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/oniro-mounts/oniro-mounts.bb:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/oniro-mounts/oniro-mounts/run-mount-devdata.mount:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/oniro-mounts/oniro-mounts/run-mount-sysdata.mount:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/oniro-mounts/oniro-mounts/oniro-homes.conf.tmpfiles:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/oniro-mounts/oniro-mounts/home.mount:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/oniro-mounts/oniro-mounts/run-mount-boot.mount:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/oniro-mounts/oniro-mounts/run-mount-appdata.mount:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/pim435/pim435_git.bb:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/busybox/busybox_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/psplash/psplash_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/libxml/libxml2_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/bundles/oniro-bundle-base.bb:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-core/systemd/systemd_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-support/libgcrypt/libgcrypt_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-support/gmp/gmp_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-support/rauc-hawkbit-updater/files/is-provisioned.conf:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-support/liburcu/liburcu_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-support/db/db_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-support/gpgme/gpgme_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-support/libpcre/libpcre2_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-support/gnupg/gnupg_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-graphics/wayland/weston-init.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-graphics/cairo/cairo_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-graphics/mesa/mesa_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/README.md:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-devtools/bison/bison_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-devtools/flex/flex_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-devtools/sed/sed_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-devtools/gcc/gcc-cross_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-devtools/make/make_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-devtools/zmk/zmk-native_0.5.1.bb:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-kernel/linux/linux-oniro_5.10.bb:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-kernel/linux/linux-yocto-tiny_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-kernel/linux/linux-intel_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-kernel/linux/linux-seco_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-kernel/linux/linux-oniro-tweaks-all.inc:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-kernel/linux/linux-yocto_5.10.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-kernel/linux/linux-raspberrypi_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-kernel/linux/linux-raspberrypi.inc:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-kernel/linux/linux/squashfs.cfg:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-kernel/linux/linux/hardening_allocator.cfg:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-kernel/linux/linux/hardening_toolchain.cfg:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-kernel/linux/linux/hardening_allocator_perf.cfg:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-kernel/linux/linux/misc.cfg:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-kernel/linux/linux/rauc.cfg:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-kernel/linux/linux/hardening_usercopy.cfg:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-kernel/linux/linux/hardening_oops.cfg:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-kernel/linux/linux/hardening_fortify_source.cfg:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-kernel/linux/linux/hardening_validation_checks.cfg:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-kernel/linux/linux/hardening_disable_misc.cfg:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-kernel/linux/linux/hardening_dmesg.cfg:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-kernel/linux/linux/hardening_memory.cfg:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-kernel/linux/linux-raspberrypi-v7_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-kernel/linux/linux-yocto-rt_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-kernel/linux/linux-yocto_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-kernel/kernel-selftest/kernel-selftest.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-connectivity/modbus/modbus-testing_git.bb:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-connectivity/openssl/openssl_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-connectivity/socat/socat_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-connectivity/networkmanager/networkmanager_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-connectivity/openthread/ot-br-posix_git.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-extended/shadow/shadow_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-extended/diffutils/diffutils_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-extended/hdparm/hdparm_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-extended/zip/zip_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-extended/ltp/ltp_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-extended/unzip/unzip_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-containers/skopeo/skopeo/storage.conf:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-containers/skopeo/skopeo_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/recipes-containers/podman/podman_%.bbappend:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/classes/oniro-sanity.bbclass:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/classes/oniro-image.bbclass:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/classes/oniro-debug-linux.bbclass:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/classes/oniro-user.bbclass:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/classes/zmk.bbclass:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/classes/oniro-debug-common.bbclass:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/classes/oniro-debug-zephyr.bbclass:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/classes/writables.bbclass:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/wic/x-raspberrypi.wks.in:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/wic/grub.cfg:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/wic/x-efi-systemd-microcode.wks.in:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/wic/x-gpt-efi-disk.wks.in:# SPDX-License-Identifier: Apache-2.0
meta-oniro-core/wic/x-imx-uboot-bootpart.wks.in:# SPDX-License-Identifier: Apache-2.0
flavours/freertos/local.conf.sample:# SPDX-License-Identifier: Apache-2.0
flavours/freertos/conf-notes.txt.license:# SPDX-License-Identifier: Apache-2.0
flavours/freertos/bblayers.conf.sample:# SPDX-License-Identifier: Apache-2.0
flavours/linux/local.conf.sample:# SPDX-License-Identifier: Apache-2.0
flavours/linux/conf-notes.txt.license:# SPDX-License-Identifier: Apache-2.0
flavours/linux/bblayers.conf.sample:# SPDX-License-Identifier: Apache-2.0
flavours/zephyr/local.conf.sample:# SPDX-License-Identifier: Apache-2.0
flavours/zephyr/conf-notes.txt.license:# SPDX-License-Identifier: Apache-2.0
flavours/zephyr/bblayers.conf.sample:# SPDX-License-Identifier: Apache-2.0
scripts/recipes-with-no-cves.py:# SPDX-License-Identifier: Apache-2.0
scripts/cve-report.py:# SPDX-License-Identifier: Apache-2.0
scripts/cve-diff.py:# SPDX-License-Identifier: Apache-2.0

This would be 163 files released under Apache-2.0 license and each one of them has the Copyright (ignoring the copyright year) as:

SPDX-FileCopyrightText: Huawei Inc.
```

### Links

This is the list of relevant links:
* Repository where you will find the agenda and meeting minutes to be approved (private repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve>
* Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc>
* Oniro WG Program Plan: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro_Program_Plan_2022.pdf>
* Oniro WG Budget: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro-2022-Working-Group-Budget-Summary.pdf>

This is the list to reference and other important notes to understand this document:
* AoB: Any Other Business
* MoM: Minutes of Meeting
* WG: Working Group
* SC: Steering Committee
* EF: Eclipse Foundation
* OH: Open Harmony

Meeting closes at 14:08 UTC

All times UTC unless expressly mentioned
