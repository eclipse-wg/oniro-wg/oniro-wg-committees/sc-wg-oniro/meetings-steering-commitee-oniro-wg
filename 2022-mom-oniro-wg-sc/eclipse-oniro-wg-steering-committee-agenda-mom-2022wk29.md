# Oniro Working Group Steering Committee Launch Meeting 2022wk29

MoM Status: approved

# approved on 2022-08-04 commit 2dd0572e

* Date: 2022-07-21
* Time: from 15:02 to 16:04 CEST
   * Oniro WG SC meeting: from 15:01 to 15:31 CEST
   * Oniro WG SC All Members meeting: from 15:31 to 16:00 CEST
* Chair: Adrian O.S. 
* Minutes taker: Carlo Piana
* Join via Zoom
   * #link <https://eclipse.zoom.us/j/86710449041?pwd=dmk1OW56emQvU1YvSjBUY1AyOHIzUT09>
   * Meeting ID: 867 1044 9041
   * Passcode: 293617

## Agenda

Oniro WG SC meeting

* Approval of the meeting minutes from the 2022-07-07  Oniro WG SC Meeting - Adrian O. 5 minutes
* Approval of the Oniro Platform Release Roadmap changes - Amit K. 10 min
* Report against Program Plan goals - Oniro PgM 5 min
* Resolutions - 5 minutes 
* AoB (Open Floor) - 5 minutes

Oniro WG SC All Members meeting

* Summary of approved changes in the Oniro Platform Release Roadmap. - Amit K. 15 minutes
* Events Plan and goals - Chiara. 10 minutes
* AoB - 5 minutes

## Attendees

### Oniro WG SC Meeting

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

* Adrian O'Sullivan - Huawei alternate
* Alberto Pianon - Array alternate
* Amit Kucheria - Committers


Steering Committee Regrets:


Other Attendees:


Eclipse Foundation:

* Agustin Benito Bethencourt
* Paul Buck (excuses himeslf @ 15:31)


### Oniro WG SC All Members Meeting

Beyond the above, the participants in this section were:

* Bill Fletcher - Linaro
* Luca Tavanti - Cubit
* Luca Miotto - NOI
* Chiara Del Fabbro - Huawei
* Carlo Piana - Array


## Minutes

### Oniro WG SC meeting minutes

#### Approval of the meeting minutes from the 2022-07-07 Oniro WG SC Meeting

* Agenda and meeting minutes to be approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/agenda-minutes-to-approve/eclipse-oniro-wg-steering-committee-agenda-mom-2022wk27.md>

(Commit considered: 5b33591e)

No objections, approved.


#### Approval of the Oniro Platform Release Roadmap changes

The changes can be seen in this link: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/resources_documentation_presentations/oniro_platform_release_roadmap_alpha_beta_20220708.pdf>

No objections, approved.

#### Report against Program Plan goals

Agustin presents an example/template for quarterly reports about the Program Plan (which should include also a budget report): it should report, for each Program Objective, strategies, actions, evaluation and next quarter focus. Is should be as high level as possible, no tactic details

Adrian suggests that SC members do an offline review of the proposed document, and in the next SC meeting they will go through possible comments; others agree. Agustin will internally share the document for review.

#### Resolutions

- Unanimous approval of Oniro SC meeting minutes 2022-07-07 Oniro WG SC Meeting
- Unanimous approval of Oniro Platform Release Roadmap changes


#### AoB  

Adrian asks if the Event List has been approved by the marketing team. Agustin needs to check.

Agustin asks for the completion, within the next week, of documentation describing Priorities, High-Level Timelines, and how do we keep track of the progress on such priorities and timelines.

### Oniro WG SC All Members Meeting minutes

#### Summary of approved changes in the Oniro Platform Release Roadmap.

Amit presents the Roadmap in general. The left part is the real Roadmap, while the right part is a progress report, which is based on raw data imported from Gitlab, which may need some cleaning. Agustin suggests that only the left part should be the actual Roadmap to be approved and shared with partners, and that the right part is kept for internal use.

At the moment Oniro, Blueprints and Compliance Projects share the same roadmap, but in the future they may have separate roadmaps /release cycles.


#### Events Plan and goals

Chiara presents the Event Plan, explaining that any planned public event/activity goes through a pre-screening pipeline in a private repo hosted in OSTC gitlab server.

Link to the deck #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/resources_documentation_presentations/oniro-wg-SC_210722_events_planning_second_semester.pptx>

Agustin recalls a previous discussion at the end of June regarding internal face-to-face meetings: the idea was to have the engineering meeting at EclipseCon, and to keep the Solda meeting for WG members meeting. 

Adrian suggests that everyone reports in the event wiki which events they are going to attend.

#### AoB  (Open Floor)

None

## Links and references

### Links

This is the list of relevant links:
* Repository where you will find the agenda and meeting minutes to be approved (private repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve>
* Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc>
* Oniro WG Program Plan: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro_Program_Plan_2022.pdf>
* Oniro WG Budget: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro-2022-Working-Group-Budget-Summary.pdf>

This is the list to reference and other important notes to understand this document:
* AoB: Any Other Business
* MoM: Minutes of Meeting
* WG: Working Group
* SC: Steering Committee
* EF: Eclipse Foundation
* OH: Open Harmony
