# Oniro Working Group Steering Committee Meeting 2022wk35

MoM Status: approved on 2022-09-29 commit 897cea19

* Date: 2022-09-01
* Time: from 15:02 to 15:53 CEST
   * Oniro WG SC meeting: from 15:01 to 15:30 CEST
   * Oniro WG SC All Members meeting: from 15:31 to 15:53 CEST
* Chair: Davide Ricci
* Minutes taker: Carlo Piana
* Join via Zoom
   * #link <https://eclipse.zoom.us/j/86710449041?pwd=dmk1OW56emQvU1YvSjBUY1AyOHIzUT09>
   * Meeting ID: 867 1044 9041
   * Passcode: 293617

## Agenda

Oniro WG SC meeting

* Approval of the meeting minutes from the 2022-08-18 2022wk33 Oniro WG SC Meeting - Davide R. 5 minutes
* Approval/consensus of the Oniro Program Plan 2022Q2 report - Agustin 5 min,
* All Hands: proposal - Agustin 5 min
* Resolutions - 5 minutes 
* AoB (Open Floor) - 5 minutes

Oniro WG SC All Members meeting

* Oniro IT Infrastructure and Services Implementation and Migration Plan v1.0.1 - Agustin 10 min
* Oniro Program Plan 2022Q2 report 10 min
   * General result
   * Oniro value proposition
* Oniro at OSS EU / ELCE - Agustin 5 minutes
* AoB - 5 minutes

## Attendees

### Oniro WG SC Meeting

Steering Committee Members (Quorum =  2 of 3 for this Meeting):

Davide Ricci - Huawei
Amit Kucheria - Committers
Adrian O'Sullivan - Huawei alternate
Carlo Piana - Array. Silver Members representative
Alberto Pianon - Array alternate

Steering Committee Regrets:


Other Attendees:


Eclipse Foundation:

* Agustin Benito Bethencourt
* Paul Buck (only private part)
[comment]: # (* Sharon Corbbet)

### Oniro WG SC All Members Meeting

Beyond the above, the participants in this section were:

[comment]: # (* Bill Fletcher - Linaro)
[comment]: # (* Luca Tavanti - Cubit)
[comment]: # (* Andrea Basso - Synesthesia)
* Luca Miotto - NOI (13.30)
* Andrea Gallo - Linaro (13:32)
 

## Minutes

### Oniro WG SC meeting minutes

#### Approval of the meeting minutes from the 2022-08-18 2022wk33 Oniro WG SC Meeting

* Agenda and meeting minutes to be approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/blob/main/agenda-minutes-to-approve/eclipse-oniro-wg-steering-committee-agenda-mom-2022wk33.md>

Commit considered: 7295c096

No objections recorded, minutes are approved

#### Approval/consensus of the Oniro Program Plan 2022Q2 report

This is the lates draft available of the Program PLan 2022Q2 report and the budget report:
* Oniro Program Plan 2022Q2 Report draft in .pdf version #link: <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program_plan_2022_budget_reports/Oniro_Program_Plan_report_2022Q2.pdf>
* Oniro Program Plan 2022Q2 Report draft in .pptx version #link: <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/program_plan_2022_budget_reports/Oniro_Program_Plan_report_2022Q2.pptx>  
* Oniro 2022Q2 budget report: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro-2022-Working-Group-Budget-June-30th-Update.pdf>                                                                                                                                             |

Paul remarks we should start considering program for 2023 too, as we head to the last quarter. Everybody agree. Consensus is reached.


#### All Hands: proposal

##### Description

Schedule
* Date: 29th September
* Time: 15 hours CEST
* Length: 90 minutes
Format:
* Online
* Tool: zoom
* Recorded

The proposed agenda is posted in [our wiki](https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/Events/OniroAllHands):


##### Discussion


Agustin: besides running through the details, hopes to have Mike (Milinkovich) to give the opening or the closure.

Davide: a good idea (overall). 

Amit: concerned because it's shortly after 26th, release date. Might be doable.

Agustin: the main purpose is to have those not in engineering to have a general overview and the other way round.

Amit: concerns

Paul: any intention to speak about the program plan -- Answer yes? (cool)

All comments positive.


#### Resolutions

* Approved unanimously the 2022wk33 MoM 

#### AoB  

Carlo: We report that we have finally a PoC of a script whose logic will lead to a good Software Composition Analysis (and SBOM generator) directly in Yocto using Yocto actual process of image building.
Alberto: chimes in.
Davide: let's sneak it in the Oniro Keynote in OSS Con in Dublin.


### Oniro WG SC All Members Meeting minutes


13:30 open

#### Oniro IT Infrastructure and Services Implementation and Migration Plan v1.0.1

##### Description

This is the link to the commonly known as Oniro IT Plan: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/coordination-it-services-oniro-wg/-/blob/main/oniro-it-infrastructure-and-services-implementation-and-migration-plan.md> 


#### Oniro Program Plan 2022Q2 report

##### Description

This is the lates draft available of the Program PLan 2022Q2 report and the budget report:
* Oniro Program Plan 2022Q2 Report draft in .pdf version #link: <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/)program-plan-and-budget-oniro-wg/program_plan_2022_budget_reports/Oniro_Program_Plan_report_2022Q2.pdf>
* Oniro Program Plan 2022Q2 Report draft in .pptx version #link: <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/)program-plan-and-budget-oniro-wg/[comment]: # ( program_plan_2022_budget_reports/Oniro_Program_Plan_report_2022Q2.pptx>

Suggestions from Oniro PgM for the coming 2022Q3 report
* Participation from additional Member representatives is welcome.
* Discussions can take place at EclipseCon 2022 as part of the Program Plan 2023 discussion.
* Shorter iterations. Still 2-3 iterations seems reasonable.
* Input from Marketing, now that the Marketing Plan will be approved will be a good addition.
* We can use as input the reports from the Oniro All Hands 2022Q3


Agustin guides through. Not much feedback. Probably for the next one we will have more (at EclipseCon)




#### Oniro at OSS EU / ELCE 

* Link to the wiki page: #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/Events_CFP_participation/Open-Source-Summit>
   * Participation list: #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/Events_CFP_participation/Open-Source-Summit#attendance>
* Link to the Oniro at ELCE calendar: #link <https://calendar.google.com/calendar/embed?src=c_3e5nq3lq8h1opvq0vqs3ch8v7g%40group.calendar.google.com&ctz=Europe%2FMadrid>
   * Todo:
      * Add talks in the calendar
      * Add meetings in the calendar
* There is a specific board for the prospects that we will meet at ELCE 2022.

Agustin describes it.

Adrian will also be there, a lot of talks are scheduled. And demo. 3 attendees confirmed.


#### AoB  (Open Floor)

Agustin describes the high level document describing the migration of the IT services and migration plan, inviting everybody to provide feedback, as it will be socialized shortly through the mailing list.

Question to  Amit, need to have more interaction with Engineering WRT release. Preferably in the morning. No cadence established yet, we'll decide.


## Links and references

### Links

This is the list of relevant links:
* Repository where you will find the agenda and meeting minutes to be approved (private repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve>
* Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved (public repository) #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/2022-mom-oniro-wg-sc>
* Oniro WG Program Plan: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro_Program_Plan_2022.pdf>
* Oniro WG Budget: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/oniro-wg-committees/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/blob/main/program-plan-and-budget-oniro-wg/Oniro-2022-Working-Group-Budget-Summary.pdf>

This is the list to reference and other important notes to understand this document:
* AoB: Any Other Business
* MoM: Minutes of Meeting
* WG: Working Group
* SC: Steering Committee
* EF: Eclipse Foundation
* OH: Open Harmony

Meeting cloeses at 13:53 UTC

All times UTC unless expressly mentioned
