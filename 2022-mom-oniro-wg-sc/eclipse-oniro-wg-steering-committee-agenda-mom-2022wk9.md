# Oniro Working Group Steering Committee Meeting 2022wk9

* Date: 2022-03-03
* Time: from 15:00 to 16:00 CET
* Chair: Agustin Benito Bethencourt
* Minutes taker: Agustin
* Join via Zoom
   * #link <https://eclipse.zoom.us/j/86710449041?pwd=dmk1OW56emQvU1YvSjBUY1AyOHIzUT09> 
   * Meeting ID: 867 1044 9041
   * Passcode: 293617

## Agenda

* Approval of the meeting minutes from the 2022-01-27 Oniro WG SC Meeting - Agustin 5 minutes
* Schedule of the Oniro WG SC Meeting series - Agustin 5 minutes
* Oniro SC WG Chair election - Sharon C. 5 minutes
* Program Plan and Budget. Examples. - Paul Buck/Sharon C. 15 minutes
* Resolutions
* AoB (Open Floor)

## Attendees

Steering Committee Members (Quorum =  2 of 3 for this Meeting):
* Davide Ricci (Huawei)
* Carlo Piana (Array)
* Amit Kucheria (PMC)

Steering Committee Regrets:
* none


Other Attendees:
* Adrian O'Sullivan (Huawei alternate)

Eclipse Foundation:
* Sharon Corbett 
* Paul Buck
* Agustin Benito Bethencourt

## Minutes 

### Approval of the meeting minutes from the 2022-01-27 Oniro WG SC Meeting

* Agenda and meeting minutes from 2022-01-27 to be approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve>
   * Also available here #link <https://docs.google.com/document/d/1U8yiOYnWxudX7cUrvMQ0QyGdy46NR4SdW3QfY0cwsUo/edit#>

### Schedule of the Oniro WG SC Meeting series

* Proposed options
   * Tuesday at 15 hours CET
   * Thursday at 15 hours CET
   
* Preference for Thursday by Amit. The other members can do either way.
* EF staff will come back to the Members representatives. Adjustments in the agenda needs to be done to be available on Thursdays.

### Oniro SC WG Chair election

* Davide R. proposed as chair. Approved unanimously.
* Carlo proposes himself as minutes taker.

### Program Plan and Budget. Examples.

* Paul Buck shows the Adoptium 2022 Program Plan as example and walks the Members through it.
* Sharon C. shows the budget template including the default entries.


### Resolutions 

* Minutes from the Oniro WG SC meeting from 2022-01-27 reviewed and approved unanimously.
* Davide Ricci is unanimously approved as Oniro WG SC chair.

### AoB  (Open Floor)

* The timing for the approval of the Program Plan, as well as the budget, is part of the 30/60/90 days plan. The goal is to have them by end of March.
* Discussion about how to reflect the sponsorship coming from different companies as well as the contributions of each Member on behalf of the Oniro WG.
   * Unless they are formally agreed through an sponsorship agreement, such contributions will not be part of the budget. The group should find a different way to track them. 

## Links and references

* Repository where you will find the agenda and meeting minutes to be approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/steering-committee-private-oniro-wg/-/tree/main/agenda-minutes-to-approve>
* Repository where the Oniro WG SC Minutes of Meetings (MoM) are published once approved #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/sc-wg-oniro/meetings-steering-commitee-oniro-wg/-/tree/main/>

* AoB: Any Other Business
* MoM: Minutes of Meeting
